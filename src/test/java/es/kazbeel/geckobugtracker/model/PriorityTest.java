package es.kazbeel.geckobugtracker.model;


import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.PriorityBuilder;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class PriorityTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testNameCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Priority priority = PriorityBuilder.priority()
                                           .withName(null)
                                           .withDescription("description")
                                           .withSortValue(0)
                                           .withEnabled(true)
                                           .build();
        
        session.save(priority);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testNameCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Priority priority = PriorityBuilder.priority()
                                           .withName(RandomStringUtils.randomAlphabetic(65))
                                           .withDescription("description")
                                           .withSortValue(0)
                                           .withEnabled(true)
                                           .build();
        
        session.save(priority);
        session.flush();
        session.clear();
    }
    
    @Test(expected = org.hibernate.exception.ConstraintViolationException.class)
    @DatabaseSetup("PriorityTest.xml")
    @Transactional
    public void testNameIsUnique () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Priority prio1 = (Priority) session.get(Priority.class, 1);
        Priority prio2 = (Priority) session.get(Priority.class, 2);
        
        prio1.setName(prio2.getName());
        
        session.save(prio1);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testDescriptionCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Priority priority = PriorityBuilder.priority()
                                           .withName("name")
                                           .withDescription(RandomStringUtils.randomAlphabetic(256))
                                           .withSortValue(0)
                                           .withEnabled(true)
                                           .build();
        
        session.save(priority);
        session.flush();
        session.clear();
    }
}
