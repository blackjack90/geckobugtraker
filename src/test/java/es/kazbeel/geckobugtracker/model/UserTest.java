package es.kazbeel.geckobugtracker.model;


import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.GroupBuilder;
import es.kazbeel.geckobugtracker.builders.UserBuilder;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class UserTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testLoginNameCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        User user = UserBuilder.user()
                               .withLoginName(null)
                               .withShortcut("SCT")
                               .withPassword("password")
                               .withFirstName("first_name")
                               .withLastName("last_name")
                               .withEmail("e.mail@e-mail.com")
                               .build();
        
        session.save(user);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testLoginNameCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        User user = UserBuilder.user()
                               .withLoginName("")
                               .withShortcut("SCT")
                               .withPassword("password")
                               .withFirstName("first_name")
                               .withLastName("last_name")
                               .withEmail("e.mail@e-mail.com")
                               .build();
        user.setLoginName(RandomStringUtils.randomAscii(129));
        
        session.save(user);
        session.flush();
        session.clear();
    }
    
    @Test(expected = org.hibernate.exception.ConstraintViolationException.class)
    @Transactional
    public void testLoginNameIsUnique () {
    
        Session session = sessionFactory.getCurrentSession();
        User user = UserBuilder.user()
                               .withLoginName("login_name")
                               .withShortcut("SCT1")
                               .withPassword("password_1")
                               .withFirstName("first_name_1")
                               .withLastName("last_name_1")
                               .withEmail("e.mail_1@e-mail.com")
                               .build();
        
        session.save(user);
        session.flush();
        session.clear();
        
        user = UserBuilder.user()
                          .withLoginName("login_name")
                          .withShortcut("SCT2")
                          .withPassword("password_2")
                          .withFirstName("first_name_2")
                          .withLastName("last_name_2")
                          .withEmail("e.mail_2@e-mail.com")
                          .build();
        
        session.save(user);
        session.flush();
        session.clear();
    }
    
    @Test(expected = org.hibernate.exception.ConstraintViolationException.class)
    @Transactional
    public void testShortcutCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        User user = UserBuilder.user()
                               .withLoginName("login_name")
                               .withShortcut(null)
                               .withPassword("password")
                               .withFirstName("first_name")
                               .withLastName("last_name")
                               .withEmail("e.mail@e-mail.com")
                               .build();
        
        session.save(user);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testShortcutCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        User user = UserBuilder.user()
                               .withLoginName("login_name")
                               .withShortcut("SCT")
                               .withPassword("password")
                               .withFirstName("first_name")
                               .withLastName("last_name")
                               .withEmail("e.mail@e-mail.com")
                               .build();
        user.setShortcut(RandomStringUtils.randomAscii(6));
        
        session.save(user);
        session.flush();
        session.clear();
    }
    
    @Test(expected = org.hibernate.exception.ConstraintViolationException.class)
    @Transactional
    public void testShortcutIsUnique () {
    
        Session session = sessionFactory.getCurrentSession();
        User user = UserBuilder.user()
                               .withLoginName("login_name_1")
                               .withShortcut("SCT")
                               .withPassword("password_1")
                               .withFirstName("first_name_1")
                               .withLastName("last_name_1")
                               .withEmail("email_1@email.com")
                               .build();
        
        session.save(user);
        session.flush();
        session.clear();
        
        user = UserBuilder.user()
                          .withLoginName("login_name_2")
                          .withShortcut("SCT")
                          .withPassword("password_2")
                          .withFirstName("first_name_2")
                          .withLastName("last_name_2")
                          .withEmail("email_2@email.com")
                          .build();
        
        session.save(user);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testPasswordCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        User user = UserBuilder.user()
                               .withLoginName("login_name")
                               .withShortcut("SCT")
                               .withPassword(null)
                               .withFirstName("first_name")
                               .withLastName("last_name")
                               .withEmail("e.mail@e-mail.com")
                               .build();
        
        session.save(user);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testPasswordCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        User user = UserBuilder.user()
                               .withLoginName("login_name")
                               .withShortcut("SCT")
                               .withPassword("password")
                               .withFirstName("first_name")
                               .withLastName("last_name")
                               .withEmail("e.mail@e-mail.com")
                               .build();
        user.setPassword(RandomStringUtils.randomAscii(65));
        
        session.save(user);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testFirstNameCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        User user = UserBuilder.user()
                               .withLoginName("login_name")
                               .withShortcut("SCT")
                               .withPassword("password")
                               .withFirstName(null)
                               .withLastName("last_name")
                               .withEmail("e.mail@e-mail.com")
                               .build();
        
        session.save(user);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testFirstNameCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        User user = UserBuilder.user()
                               .withLoginName("login_name")
                               .withShortcut("SCT")
                               .withPassword("password")
                               .withFirstName("first_name")
                               .withLastName("last_name")
                               .withEmail("e.mail@e-mail.com")
                               .build();
        user.setPassword(RandomStringUtils.randomAscii(65));
        
        session.save(user);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testLastNameCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        User user = UserBuilder.user()
                               .withLoginName("login_name")
                               .withShortcut("SCT")
                               .withPassword("password")
                               .withFirstName("first_name")
                               .withLastName(null)
                               .withEmail("e.mail@e-mail.com")
                               .build();
        
        session.save(user);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testLastNameCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        User user = UserBuilder.user()
                               .withLoginName("login_name")
                               .withShortcut("SCT")
                               .withPassword("password")
                               .withFirstName("first_name")
                               .withLastName("last_name")
                               .withEmail("e.mail@e-mail.com")
                               .build();
        user.setPassword(RandomStringUtils.randomAscii(129));
        
        session.save(user);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testEmailCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        User user = UserBuilder.user()
                               .withLoginName("login_name")
                               .withShortcut("SCT")
                               .withPassword("password")
                               .withFirstName("first_name")
                               .withLastName("last_name")
                               .withEmail(null)
                               .build();
        
        session.save(user);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testEmailCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        User user = UserBuilder.user()
                               .withLoginName("login_name")
                               .withShortcut("SCT")
                               .withPassword("password")
                               .withFirstName("first_name")
                               .withLastName("last_name")
                               .withEmail("e.mail@e-mail.com")
                               .build();
        user.setEmail(RandomStringUtils.randomAscii(256));
        
        session.save(user);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testEmailMustBeValid () {
    
        Session session = sessionFactory.getCurrentSession();
        
        User user = UserBuilder.user()
                               .withLoginName("login_name")
                               .withShortcut("SCT")
                               .withPassword("password")
                               .withFirstName("first_name")
                               .withLastName("last_name")
                               .withEmail("invalid_email")
                               .build();
        
        session.save(user);
        session.flush();
        session.clear();
    }

    @Test(expected = org.hibernate.exception.ConstraintViolationException.class)
    @Transactional
    public void testEmailIsUnique () {
    
        Session session = sessionFactory.getCurrentSession();
        User user = UserBuilder.user()
                               .withLoginName("login_name_1")
                               .withShortcut("SCT1")
                               .withPassword("password_1")
                               .withFirstName("first_name_1")
                               .withLastName("last_name_1")
                               .withEmail("e.mail@e-mail.com")
                               .build();
        
        session.save(user);
        session.flush();
        session.clear();
        
        user = UserBuilder.user()
                          .withLoginName("login_name_2")
                          .withShortcut("SCT2")
                          .withPassword("password_2")
                          .withFirstName("first_name_2")
                          .withLastName("last_name_2")
                          .withEmail("e.mail@e-mail.com")
                          .build();
        
        session.save(user);
        session.flush();
        session.clear();
    }
    
    @Test
    @DatabaseSetup("UserTest.xml")
    @Transactional
    public void testManyToManyGroupsNotEmpty () {
    
        Session session = sessionFactory.getCurrentSession();
        User user = (User) session.createQuery("FROM User WHERE id=1").uniqueResult();
        
        assertThat(user, is(not(nullValue())));
        assertThat(user, is(UserBuilder.user().withId(1).withLoginName("login_name_1").build()));
        assertThat(user.getGroups(), hasSize(2));
        assertThat(user.getGroups(),
                   containsInAnyOrder(GroupBuilder.group().withId(2).withName("group_name_2").build(),
                                      GroupBuilder.group().withId(1).withName("group_name_1").build()));
    }
    
    @Test
    @DatabaseSetup("UserTest.xml")
    @Transactional
    public void testManyToManyGroupsEmpty () {
    
        Session session = sessionFactory.getCurrentSession();
        User user = (User) session.createQuery("FROM User WHERE id=4").uniqueResult();
        
        assertThat(user, is(not(nullValue())));
        assertThat(user, is(UserBuilder.user().withId(4).withLoginName("login_name_4").build()));
        assertThat(user.getGroups(), is(empty()));
    }
}
