package es.kazbeel.geckobugtracker.model;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.UserBuilder;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class ComponentTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("ComponentTest.xml")
    @Transactional
    public void testNameCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Component component = (Component) session.get(Component.class, 1L);
        component.setName(null);
        
        session.save(component);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("ComponentTest.xml")
    @Transactional
    public void testNameCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Component component = (Component) session.get(Component.class, 1L);
        component.setName(RandomStringUtils.randomAlphabetic(256));
        
        session.save(component);
        session.flush();
        session.clear();
    }
    
    @Test(expected = org.hibernate.exception.ConstraintViolationException.class)
    @DatabaseSetup("ComponentTest.xml")
    @Transactional
    public void testNameMustBeUnique () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Component component1 = (Component) session.get(Component.class, 1L);
        Component component2 = (Component) session.get(Component.class, 2L);
        component1.setName(component2.getName());
        
        session.save(component1);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("ComponentTest.xml")
    @Transactional
    public void testDescriptionCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Component component = (Component) session.get(Component.class, 1L);
        component.setDescription(RandomStringUtils.randomAlphabetic(4097));
        
        session.save(component);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("ComponentTest.xml")
    @Transactional
    public void testDevLeaderCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Component component = (Component) session.get(Component.class, 1L);
        component.setDevLeader(null);
        
        session.save(component);
        session.flush();
        session.clear();
    }
    
    @Test
    @DatabaseSetup("ComponentTest.xml")
    @Transactional
    public void testComponentWithDevLeader () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Component component = (Component) session.get(Component.class, 1L);
        
        session.save(component);
        session.flush();
        session.clear();
        
        assertThat(component, is(not(nullValue())));
        assertThat(component.getDevLeader(), is(not(nullValue())));
        assertThat(component.getDevLeader(),
                   is(UserBuilder.user()
                                 .withId(1)
                                 .withLoginName("login_name_1")
                                 .withPassword("passwd")
                                 .withFirstName("first_name")
                                 .withLastName("last_name")
                                 .withEmail("e_mail_1")
                                 .build()));
    }
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("ComponentTest.xml")
    @Transactional
    public void testQaLeaderCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Component component = (Component) session.get(Component.class, 1L);
        component.setQaLeader(null);
        
        session.save(component);
        session.flush();
        session.clear();
    }
    
    @Test
    @DatabaseSetup("ComponentTest.xml")
    @Transactional
    public void testComponentWithQaLeader () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Component component = (Component) session.get(Component.class, 1L);
        
        session.save(component);
        session.flush();
        session.clear();
        
        assertThat(component, is(not(nullValue())));
        assertThat(component.getQaLeader(), is(not(nullValue())));
        assertThat(component.getQaLeader(),
                   is(UserBuilder.user()
                                 .withId(1)
                                 .withLoginName("login_name_1")
                                 .withPassword("passwd")
                                 .withFirstName("first_name")
                                 .withLastName("last_name")
                                 .withEmail("e_mail_1")
                                 .build()));
    }
}
