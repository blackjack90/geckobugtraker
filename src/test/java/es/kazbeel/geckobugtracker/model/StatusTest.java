package es.kazbeel.geckobugtracker.model;


import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;

import es.kazbeel.geckobugtracker.builders.StatusBuilder;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class StatusTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testNameCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Status status = StatusBuilder.status()
                                     .withName(null)
                                     .withDescription("description")
                                     .withSortValue(0)
                                     .withEnabled(true)
                                     .build();
        
        session.save(status);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testNameCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Status status = StatusBuilder.status()
                                     .withName(RandomStringUtils.randomAlphanumeric(256))
                                     .withDescription("description")
                                     .withSortValue(0)
                                     .withEnabled(true)
                                     .build();
        
        session.save(status);
        session.flush();
        session.clear();
    }
    
    @Test(expected = org.hibernate.exception.ConstraintViolationException.class)
    @Transactional
    public void testNameMustBeUnique () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Status status = StatusBuilder.status()
                                     .withName("name")
                                     .withDescription("description")
                                     .withSortValue(0)
                                     .withEnabled(true)
                                     .build();
        
        session.save(status);
        session.flush();
        session.clear();
        
        status = StatusBuilder.status()
                              .withName("name")
                              .withDescription("description_2")
                              .withSortValue(1)
                              .withEnabled(false)
                              .build();
        
        session.save(status);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testDescriptionCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Status status = StatusBuilder.status()
                                     .withName("name")
                                     .withDescription(RandomStringUtils.randomAlphanumeric(256))
                                     .withSortValue(0)
                                     .withEnabled(true)
                                     .build();
        
        session.save(status);
        session.flush();
        session.clear();
    }
}
