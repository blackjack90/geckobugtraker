package es.kazbeel.geckobugtracker.model;


import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.IssueTypeBuilder;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class IssueTypeTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testNameCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        IssueType issueType = IssueTypeBuilder.issueType()
                                              .withName(null)
                                              .withDescription("description")
                                              .withSortValue(0)
                                              .build();
        
        session.save(issueType);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testNameCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        IssueType issueType = IssueTypeBuilder.issueType()
                                              .withName(RandomStringUtils.randomAlphabetic(129))
                                              .withDescription("description")
                                              .withSortValue(0)
                                              .build();
        
        session.save(issueType);
        session.flush();
        session.clear();
    }
    
    @Test(expected = org.hibernate.exception.ConstraintViolationException.class)
    @DatabaseSetup("IssueTypeTest.xml")
    @Transactional
    public void testNameMustBeUnique () {
    
        Session session = sessionFactory.getCurrentSession();
        
        IssueType it1 = (IssueType) session.get(IssueType.class, 1);
        IssueType it2 = (IssueType) session.get(IssueType.class, 2);
        
        it1.setName(it2.getName());
        
        session.save(it1);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testDescriptionCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        IssueType issueType = IssueTypeBuilder.issueType()
                                              .withName("name")
                                              .withDescription(RandomStringUtils.randomAlphabetic(256))
                                              .withSortValue(0)
                                              .build();
        
        session.save(issueType);
        session.flush();
        session.clear();
    }
}
