package es.kazbeel.geckobugtracker.model;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.TagBuilder;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class TagTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testNameCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Tag tag = TagBuilder.tag().withName(null).withEnabled(true).build();
        
        session.save(tag);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @Transactional
    public void testNameCannotBeLonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Tag tag = TagBuilder.tag().withName(RandomStringUtils.randomAlphabetic(256)).withEnabled(true).build();
        
        session.save(tag);
        session.flush();
        session.clear();
    }
    
    @Test
    @DatabaseSetup("TagTest.xml")
    @Transactional
    public void testTagWithoutIssues () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Tag tag = (Tag) session.createQuery("FROM Tag WHERE id=4").uniqueResult();
        
        assertNotNull(tag);
        assertEquals(tag.getIssues().size(), 0);
    }
    
    @Test
    @DatabaseSetup("TagTest.xml")
    @Transactional
    public void testEnvironmentWithOneIssue () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Tag tag = (Tag) session.createQuery("FROM Tag WHERE id=2").uniqueResult();
        
        assertNotNull(tag);
        assertEquals(tag.getIssues().size(), 1);
        assertEquals(tag.getIssues().iterator().next().getId(), new Integer(3));
    }
    
    @Test
    @DatabaseSetup("TagTest.xml")
    @Transactional
    public void testEnvironmentWithMultipleIssues () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Tag tag = (Tag) session.createQuery("FROM Tag WHERE id=1").uniqueResult();
        
        assertNotNull(tag);
        assertEquals(tag.getIssues().size(), 2);
    }
    
}
