package es.kazbeel.geckobugtracker.model;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

import javax.validation.ConstraintViolationException;

import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.AttachmentBuilder;
import es.kazbeel.geckobugtracker.builders.IssueBuilder;
import es.kazbeel.geckobugtracker.builders.UserBuilder;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class AttachmentTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("AttachmentTest.xml")
    @Transactional
    public void testIssueCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        User author = (User) session.get(User.class, 1);
        
        Attachment attch = AttachmentBuilder.attachment()
                                            .withIssue(null)
                                            .withAuthor(author)
                                            .withFileName("file_name")
                                            .withFileSize(new Long(0))
                                            .withFileMimeType("file_mimetype")
                                            .build();
        
        session.save(attch);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("AttachmentTest.xml")
    @Transactional
    public void testAuthorCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Issue issue = (Issue) session.get(Issue.class, 1);
        
        Attachment attch = AttachmentBuilder.attachment()
                                            .withIssue(issue)
                                            .withAuthor(null)
                                            .withFileName("file_name")
                                            .withFileSize(new Long(0))
                                            .withFileMimeType("file_mimetype")
                                            .build();
        
        session.save(attch);
        session.flush();
        session.clear();
    }
    
    @Test
    @DatabaseSetup("AttachmentTest.xml")
    @Transactional
    public void testGoodMappingForIssue () {
    
        Attachment attch = (Attachment) sessionFactory.getCurrentSession().get(Attachment.class, 1);
        
        assertThat(attch, is(not(nullValue())));
        assertThat(attch.getIssue(), is(IssueBuilder.issue().withId(1).build()));
    }
    
    @Test
    @DatabaseSetup("AttachmentTest.xml")
    @Transactional
    public void testGoodMappingForUser () {
    
        Attachment attch = (Attachment) sessionFactory.getCurrentSession().get(Attachment.class, 1);
        
        assertThat(attch, is(not(nullValue())));
        assertThat(attch.getAuthor(), is(UserBuilder.user().withId(1).withLoginName("login_name_1").build()));
    }
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("AttachmentTest.xml")
    @Transactional
    public void testFileNameCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Issue issue = (Issue) session.get(Issue.class, 1);
        User author = (User) session.get(User.class, 1);
        
        Attachment attch = AttachmentBuilder.attachment()
                                            .withIssue(issue)
                                            .withAuthor(author)
                                            .withFileName(null)
                                            .withFileSize(new Long(0))
                                            .withFileMimeType("file_mimetype")
                                            .build();
        
        session.save(attch);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("AttachmentTest.xml")
    @Transactional
    public void testFilenameCannotBelonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Issue issue = (Issue) session.get(Issue.class, 1);
        User author = (User) session.get(User.class, 1);
        
        Attachment attch = AttachmentBuilder.attachment()
                                            .withIssue(issue)
                                            .withAuthor(author)
                                            .withFileName(RandomStringUtils.randomAlphabetic(256))
                                            .withFileSize(new Long(0))
                                            .withFileMimeType("file_mimetype")
                                            .build();
        
        session.save(attch);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("AttachmentTest.xml")
    @Transactional
    public void testFileSizeCannotBeNull () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Issue issue = (Issue) session.get(Issue.class, 1);
        User author = (User) session.get(User.class, 1);
        
        Attachment attch = AttachmentBuilder.attachment()
                                            .withIssue(issue)
                                            .withAuthor(author)
                                            .withFileName("file_name")
                                            .withFileSize(null)
                                            .withFileMimeType("file_mimetype")
                                            .build();
        
        session.save(attch);
        session.flush();
        session.clear();
    }
    
    @Test(expected = ConstraintViolationException.class)
    @DatabaseSetup("AttachmentTest.xml")
    @Transactional
    public void testFileMimetypeCannotBelonger () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Issue issue = (Issue) session.get(Issue.class, 1);
        User author = (User) session.get(User.class, 1);
        
        Attachment attch = AttachmentBuilder.attachment()
                                            .withIssue(issue)
                                            .withAuthor(author)
                                            .withFileName("file_name")
                                            .withFileSize(new Long(0))
                                            .withFileMimeType(RandomStringUtils.randomAlphabetic(256))
                                            .build();
        
        session.save(attch);
        session.flush();
        session.clear();
    }
}
