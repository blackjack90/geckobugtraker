package es.kazbeel.geckobugtracker.model;


import static org.junit.Assert.*;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class WatcherTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    
    @Test
    @DatabaseSetup("WatcherTest.xml")
    @Transactional
    public void testGetWatcherWithEmptyActorType () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Watcher watcher = (Watcher) session.createQuery("FROM Watcher watcher WHERE watcher.id=1").uniqueResult();
        
        assertNull(watcher);
    }
    
    @Test
    @DatabaseSetup("WatcherTest.xml")
    @Transactional
    public void testGetWatcherWithWrongActorType () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Watcher watcher = (Watcher) session.createQuery("FROM Watcher watcher WHERE watcher.id=4").uniqueResult();
        
        assertNull(watcher);
    }
    
    @Test
    @DatabaseSetup("WatcherTest.xml")
    @Transactional
    public void testGetWatcherWithInvalidActorType () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Watcher watcher = (Watcher) session.createQuery("FROM Watcher watcher WHERE watcher.id=5").uniqueResult();
        
        assertNull(watcher);
    }
    
    @Test
    @DatabaseSetup("WatcherTest.xml")
    @Transactional
    public void testGetWatcherWithValidActorType () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Watcher watcher = (Watcher) session.createQuery("FROM Watcher watcher WHERE watcher.id=2").uniqueResult();
        
        assertNotNull(watcher);
        assertEquals(watcher.getId(), new Integer(2));
    }
    
    @Test
    @DatabaseSetup("WatcherTest.xml")
    @Transactional
    public void testCountAllWatchersWithValidActorType () {
    
        Session session = sessionFactory.getCurrentSession();
        
        Long totalWatchers = (Long) session.createQuery("select count(*) FROM Watcher watcher").uniqueResult();
        
        assertNotNull(totalWatchers);
        assertEquals(totalWatchers, new Long(2));
    }
    
}
