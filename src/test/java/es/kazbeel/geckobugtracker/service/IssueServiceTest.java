package es.kazbeel.geckobugtracker.service;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import es.kazbeel.geckobugtracker.builders.CommentBuilder;
import es.kazbeel.geckobugtracker.builders.IssueBuilder;
import es.kazbeel.geckobugtracker.builders.IssueLinkBuilder;
import es.kazbeel.geckobugtracker.builders.PageBuilder;
import es.kazbeel.geckobugtracker.builders.PriorityBuilder;
import es.kazbeel.geckobugtracker.builders.ResolutionBuilder;
import es.kazbeel.geckobugtracker.builders.UserBuilder;
import es.kazbeel.geckobugtracker.builders.VoteBuilder;
import es.kazbeel.geckobugtracker.builders.WatcherBuilder;
import es.kazbeel.geckobugtracker.model.Attachment;
import es.kazbeel.geckobugtracker.model.Category;
import es.kazbeel.geckobugtracker.model.Comment;
import es.kazbeel.geckobugtracker.model.Environment;
import es.kazbeel.geckobugtracker.model.Issue;
import es.kazbeel.geckobugtracker.model.IssueLink;
import es.kazbeel.geckobugtracker.model.IssueLinkType;
import es.kazbeel.geckobugtracker.model.IssueType;
import es.kazbeel.geckobugtracker.model.Priority;
import es.kazbeel.geckobugtracker.model.Resolution;
import es.kazbeel.geckobugtracker.model.Status;
import es.kazbeel.geckobugtracker.model.Tag;
import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.model.Vote;
import es.kazbeel.geckobugtracker.model.Watcher;
import es.kazbeel.geckobugtracker.repository.AttachmentDao;
import es.kazbeel.geckobugtracker.repository.CategoryDao;
import es.kazbeel.geckobugtracker.repository.CommentDao;
import es.kazbeel.geckobugtracker.repository.EnvironmentDao;
import es.kazbeel.geckobugtracker.repository.IssueDao;
import es.kazbeel.geckobugtracker.repository.IssueLinkDao;
import es.kazbeel.geckobugtracker.repository.IssueLinkTypeDao;
import es.kazbeel.geckobugtracker.repository.IssueTypeDao;
import es.kazbeel.geckobugtracker.repository.PriorityDao;
import es.kazbeel.geckobugtracker.repository.ResolutionDao;
import es.kazbeel.geckobugtracker.repository.StatusDao;
import es.kazbeel.geckobugtracker.repository.TagDao;
import es.kazbeel.geckobugtracker.repository.VoteDao;
import es.kazbeel.geckobugtracker.repository.WatcherDao;
import es.kazbeel.geckobugtracker.support.Page;
import es.kazbeel.geckobugtracker.support.PageRequest;


@RunWith(MockitoJUnitRunner.class)
public class IssueServiceTest {
    
    @Mock
    private IssueDao issueDao;
    
    @Mock
    private IssueTypeDao issueTypeDao;
    
    @Mock
    private StatusDao statusDao;
    
    @Mock
    private PriorityDao priorityDao;
    
    @Mock
    private EnvironmentDao environmentDao;
    
    @Mock
    private TagDao tagDao;
    
    @Mock
    private IssueLinkDao issueLinkDao;
    
    @Mock
    private VoteDao voteDao;
    
    @Mock
    private WatcherDao watcherDao;
    
    @Mock
    private CommentDao commentDao;
    
    @Mock
    private ResolutionDao resolutionDao;
    
    @Mock
    private CategoryDao categoryDao;
    
    @Mock
    private IssueLinkTypeDao issueLinkTypeDao;
    
    @Mock
    private AttachmentDao attachmentDao;
    
    @InjectMocks
    private IssueService issueService = new IssueServiceImpl(issueDao,
                                                             issueTypeDao,
                                                             issueLinkTypeDao,
                                                             statusDao,
                                                             priorityDao,
                                                             environmentDao,
                                                             tagDao,
                                                             issueLinkDao,
                                                             voteDao,
                                                             watcherDao,
                                                             commentDao,
                                                             resolutionDao,
                                                             categoryDao,
                                                             attachmentDao);
    
    
    @BeforeClass
    public static void setUpBeforeClass () throws Exception {
    
    }
    
    @Test
    public void testGetIssueById () {
    
        issueService.getIssueById(anyInt());
        
        verify(issueDao).findById(anyInt());
    }
    
    @Test
    public void testGetIssuesByReporter () {
    
        List<Issue> issues = new ArrayList<Issue>();
        
        issues.add(IssueBuilder.issue().withId(1).build());
        issues.add(IssueBuilder.issue().withId(2).build());
        issues.add(IssueBuilder.issue().withId(3).build());
        
        User reporter = UserBuilder.user().withId(1).withLoginName("login_name").build();
        
        when(issueDao.findByReporter(anyInt(), anyInt(), anyInt())).thenReturn(issues);
        when(issueDao.countByReporter(anyInt())).thenReturn(new Long(3));
        
        PageRequest pageRequest = new PageRequest(0, 5);
        Page<Issue> page = issueService.getIssuesByReporter(reporter, pageRequest);
        
        assertThat(page, is(not(nullValue())));
        assertThat(page,
                   is(PageBuilder.<Issue> page().withPageNumber(0).withMaxPageSize(5).withTotalElements(3).build()));
        verify(issueDao).findByReporter(reporter.getId(), 0, 5);
        verify(issueDao).countByReporter(reporter.getId());
    }
    
    @Test
    public void testGetIssuesVotedByUser () {
    
        List<Issue> issues = new ArrayList<Issue>();
        List<Vote> votes = new ArrayList<Vote>();
        
        issues.add(IssueBuilder.issue().withId(1).build());
        issues.add(IssueBuilder.issue().withId(2).build());
        
        votes.add(VoteBuilder.vote().withId(1).withIssue(issues.get(0)).build());
        votes.add(VoteBuilder.vote().withId(2).withIssue(issues.get(1)).build());
        
        User voter = UserBuilder.user().withId(1).withLoginName("login_name").build();
        
        when(voteDao.findByUser(anyInt(), anyInt(), anyInt())).thenReturn(votes);
        when(voteDao.countByUser(anyInt())).thenReturn(new Long(2));
        
        PageRequest pageRequest = new PageRequest(0, 5);
        Page<Issue> page = issueService.getIssuesVotedByUser(voter, pageRequest);
        
        assertThat(page, is(not(nullValue())));
        assertThat(page,
                   is(PageBuilder.<Issue> page().withPageNumber(0).withMaxPageSize(5).withTotalElements(2).build()));
        verify(voteDao).findByUser(voter.getId(), 0, 5);
        verify(voteDao).countByUser(voter.getId());
    }
    
    @Test
    public void testGetIssuesWatchedByUser () {
    
        List<Issue> issues = new ArrayList<Issue>();
        List<Watcher> watchers = new ArrayList<Watcher>();
        
        issues.add(IssueBuilder.issue().withId(1).build());
        issues.add(IssueBuilder.issue().withId(2).build());
        
        watchers.add(WatcherBuilder.watcher().withId(1).withIssue(issues.get(0)).build());
        watchers.add(WatcherBuilder.watcher().withId(2).withIssue(issues.get(1)).build());
        
        User watcher = UserBuilder.user().withId(1).withLoginName("login_name").build();
        
        when(watcherDao.findByUser(anyInt(), anyInt(), anyInt())).thenReturn(watchers);
        when(watcherDao.countByUser(anyInt())).thenReturn(new Long(2));
        
        PageRequest pageRequest = new PageRequest(0, 5);
        Page<Issue> page = issueService.getIssuesWatchedByUser(watcher, pageRequest);
        
        assertThat(page, is(not(nullValue())));
        assertThat(page,
                   is(PageBuilder.<Issue> page().withPageNumber(0).withMaxPageSize(5).withTotalElements(2).build()));
        verify(watcherDao).findByUser(watcher.getId(), 0, 5);
        verify(watcherDao).countByUser(watcher.getId());
    }
    
    @Test
    public void testGetAllIssues () {
    
        List<Issue> issues = new ArrayList<Issue>();
        
        issues.add(IssueBuilder.issue().withId(1).build());
        issues.add(IssueBuilder.issue().withId(2).build());
        issues.add(IssueBuilder.issue().withId(3).build());
        
        when(issueDao.findAll(anyInt(), anyInt())).thenReturn(issues);
        when(issueDao.countAll()).thenReturn(new Long(3));
        
        PageRequest pageRequest = new PageRequest(0, 5);
        Page<Issue> page = issueService.getAllIssues(pageRequest);
        
        assertThat(page, is(not(nullValue())));
        assertThat(page,
                   is(PageBuilder.<Issue> page().withPageNumber(0).withMaxPageSize(5).withTotalElements(3).build()));
        verify(issueDao).findAll(0, 5);
        verify(issueDao).countAll();
    }
    
    @Test
    public void testGetIssuesByAssigneeAndResolution () {
    
        List<Issue> issues = new ArrayList<Issue>();
        
        issues.add(IssueBuilder.issue().withId(1).build());
        issues.add(IssueBuilder.issue().withId(2).build());
        issues.add(IssueBuilder.issue().withId(3).build());
        
        User assignee = UserBuilder.user().withId(1).withLoginName("login_name").build();
        Resolution resolution = ResolutionBuilder.resolution().withId(1).withName("resolution_name").build();
        
        when(issueDao.findByAssigneeAndResolution(anyInt(), anyInt(), anyInt(), anyInt())).thenReturn(issues);
        when(issueDao.countByAssigneeAndResolution(anyInt(), anyInt())).thenReturn(new Long(3));
        
        PageRequest pageRequest = new PageRequest(0, 5);
        Page<Issue> page = issueService.getIssuesByAssigneeAndResolution(assignee, resolution, pageRequest);
        
        assertThat(page, is(not(nullValue())));
        assertThat(page,
                   is(PageBuilder.<Issue> page().withPageNumber(0).withMaxPageSize(5).withTotalElements(3).build()));
        verify(issueDao).findByAssigneeAndResolution(assignee.getId(), resolution.getId(), 0, 5);
        verify(issueDao).countByAssigneeAndResolution(assignee.getId(), resolution.getId());
    }
    
    @Test
    public void testGetAddedRecentlyIssuesTable () {
    
        issueService.getAddedRecentlyIssuesTable();
        
        verify(issueDao).findAddedRecentlyTable(any(Date.class));
    }
    
    @Test
    public void testSaveIssue () {
    
        issueService.saveIssue(any(Issue.class));
        
        verify(issueDao).saveOrUpdate(any(Issue.class));
    }
    
    @Test
    public void testGetNumIssuesByReporter () {
    
        issueService.getNumIssuesByReporter(UserBuilder.user().withId(1).withLoginName("login_name").build());
        
        verify(issueDao).countByReporter(1);
    }
    
    @Test
    public void testGetNumIssuesWatchedByUser () {
    
        issueService.getNumIssuesWatchedByUser(UserBuilder.user().withId(1).withLoginName("login_name").build());
        
        verify(watcherDao).countByUser(1);
    }
    
    @Test
    public void testGetNumIssuesVotedByUser () {
    
        issueService.getNumIssuesVotedByUser(UserBuilder.user().withId(1).withLoginName("login_name").build());
        
        verify(voteDao).countByUser(1);
    }
    
    @Test
    public void testDeleteIssue () {
    
        Issue issue = IssueBuilder.issue().withId(1).build();
        
        issueService.deleteIssue(issue);
        
        verify(voteDao).findByIssue(issue.getId());
        verify(watcherDao).findByIssue(issue.getId());
        verify(issueLinkDao).findAllRelatedToIssue(issue.getId());
        verify(issueDao).delete(issue);
    }
    
    @Test
    public void testAddCommentToIssue () {
    
        Issue issue = IssueBuilder.issue().withId(1).build();
        User user = UserBuilder.user().withId(1).withLoginName("login_name").build();
        Comment comment = CommentBuilder.comment().withId(1).build();
        
        issueService.addCommentToIssue(issue, user, comment);
        
        assertThat(comment.getAuthor(), is(user));
        assertThat(comment.getIssue(), is(issue));
        verify(commentDao).save(comment);
    }
    
    @Test
    public void testGetNumVotesForIssue () {
    
        Issue issue = IssueBuilder.issue().withId(1).build();
        
        issueService.getNumVotesForIssue(issue);
        
        verify(voteDao).countByIssue(issue.getId());
    }
    
    @Test
    public void testIsIssueVotedByUser () {
    
        Issue issue = IssueBuilder.issue().withId(1).build();
        User voter = UserBuilder.user().withId(1).withLoginName("login_name").build();
        
        issueService.isIssueVotedByUser(issue, voter);
        
        verify(voteDao).isIssueVotedByUser(issue.getId(), voter.getId());
    }
    
    @Test
    public void testVoteIssue () {
    
        // TODO This method has to be replaced by vote and unvote.
    }
    
    @Test
    public void testGetNumWatchersForIssue () {
    
        Issue issue = IssueBuilder.issue().withId(1).build();
        
        issueService.getNumWatchersForIssue(issue);
        
        verify(watcherDao).countByIssue(issue.getId());
    }
    
    @Test
    public void testIsIssueWatchedByUser () {
    
        Issue issue = IssueBuilder.issue().withId(1).build();
        User watcher = UserBuilder.user().withId(1).withLoginName("login_name").build();
        
        issueService.isIssueWatchedByUser(issue, watcher);
        
        verify(watcherDao).isIssueWatchedByUser(issue.getId(), watcher.getId());
    }
    
    @Test
    public void testWatchIssue () {
    
        // TODO This method has to be replaced by watch and unwatch.
    }
    
    @Test
    public void testSaveCategory () {
    
        issueService.saveCategory(any(Category.class));
        
        verify(categoryDao).saveOrUpdate(any(Category.class));
    }
    
    @Test
    public void testDeleteCategory () {
    
        issueService.deleteCategory(any(Category.class));
        
        verify(categoryDao).delete(any(Category.class));
    }
    
    @Test
    public void testGetAllCategories () {
    
        issueService.getAllCategories();
        
        verify(categoryDao).findAll();
    }
    
    @Test
    public void testGetCategoryById () {
    
        issueService.getCategoryById(anyInt());
        
        verify(categoryDao).findById(anyInt());
    }
    
    @Test
    public void testSaveIssueType () {
    
        issueService.saveIssueType(any(IssueType.class));
        
        verify(issueTypeDao).saveOrUpdate(any(IssueType.class));
    }
    
    @Test
    public void testDeleteIssueType () {
    
        issueService.deleteIssueType(any(IssueType.class));
        
        verify(issueTypeDao).delete(any(IssueType.class));
    }
    
    @Test
    public void testGetAllIssueTypes () {
    
        issueService.getAllIssueTypes();
        
        verify(issueTypeDao).findAll();
    }
    
    @Test
    public void testGetIssueTypeById () {
    
        issueService.getIssueTypeById(anyInt());
        
        verify(issueTypeDao).findById(anyInt());
    }
    
    @Test
    public void testSaveIssueLinkType () {
    
        issueService.saveIssueLinkType(any(IssueLinkType.class));
        
        verify(issueLinkTypeDao).saveOrUpdate(any(IssueLinkType.class));
    }
    
    @Test
    public void testDeleteIssueLinkType () {
    
        issueService.deleteIssueLinkType(any(IssueLinkType.class));
        
        verify(issueLinkTypeDao).delete(any(IssueLinkType.class));
    }
    
    @Test
    public void testGetAllIssueLinkTypes () {
    
        issueService.getAllIssueLinkTypes();
        
        verify(issueLinkTypeDao).findAll();
    }
    
    @Test
    public void testGetIssueLinkTypeById () {
    
        issueService.getIssueLinkTypeById(anyInt());
        
        verify(issueLinkTypeDao).findById(anyInt());
    }
    
    @Test
    public void testGetIssueLinkById () {
        
        issueService.getIssueLinkById(anyInt());
        
        verify(issueLinkDao).findById(anyInt());
    }
    
    @Test
    public void testGetAllIssueLinksWithSource () {
    
        Issue issue = IssueBuilder.issue().withId(1).build();
        
        issueService.getAllIssueLinksWithSource(issue);
        
        verify(issueLinkDao).findAllWithSource(issue.getId());
    }
    
    @Test
    public void testGetAllIssueLinksWithDestination () {
    
        Issue issue = IssueBuilder.issue().withId(1).build();
        
        issueService.getAllIssueLinksWithDestination(issue);
        
        verify(issueLinkDao).findAllWithDestination(issue.getId());
    }
    
    @Test
    public void testGetAllIssueLinksRelatedToIssue () {
    
        Issue issue = IssueBuilder.issue().withId(1).build();
        
        issueService.getAllIssueLinksRelatedToIssue(issue);
        
        verify(issueLinkDao).findAllRelatedToIssue(issue.getId());
    }
    
    @Test
    public void testSaveIssueLink () {
    
        Issue source = IssueBuilder.issue().withId(1).build();
        Issue destination = IssueBuilder.issue().withId(2).build();
        IssueLink issueLink = IssueLinkBuilder.issueLink()
                                              .withId(1)
                                              .withSource(source)
                                              .withDestination(destination)
                                              .build();
        
        issueService.saveIssueLink(issueLink);
        
        verify(issueLinkDao).saveOrUpdate(issueLink);
    }
    
    @Test
    public void testDeleteIssueLink () {
    
        issueService.deleteIssueLink(any(IssueLink.class));
        
        verify(issueLinkDao).delete(any(IssueLink.class));
    }
    
    @Test
    public void testSaveStatus () {
    
        issueService.saveStatus(any(Status.class));
        
        verify(statusDao).saveOrUpdate(any(Status.class));
    }
    
    @Test
    public void testDeleteStatus () {
    
        issueService.deleteStatus(any(Status.class));
        
        verify(statusDao).delete(any(Status.class));
    }
    
    @Test
    public void testGetAllStatuses () {
    
        issueService.getAllStatuses();
        
        verify(statusDao).findAll();
    }
    
    @Test
    public void testGetStatusById () {
    
        issueService.getStatusById(anyInt());
        
        verify(statusDao).findById(anyInt());
    }
    
    @Test
    public void testSavePriority () {
    
        issueService.savePriority(any(Priority.class));
        
        verify(priorityDao).saveOrUpdate(any(Priority.class));
    }
    
    @Test
    public void testDeletePriority () {
    
        issueService.deletePriority(any(Priority.class));
        
        verify(priorityDao).delete(any(Priority.class));
    }
    
    @Test
    public void testGetAllPriorities () {
    
        issueService.getAllPriorities();
        
        verify(priorityDao).findAll();
    }
    
    @Test
    public void testGetPriorityById () {
    
        issueService.getPriorityById(anyInt());
        
        verify(priorityDao).findById(anyInt());
    }
    
    @Test
    public void testSetHigherOrderForPriority () {
    
        Priority priority = PriorityBuilder.priority().withId(1).build();
        
        issueService.setHigherOrderForPriority(priority);
        
        verify(priorityDao).increaseOrder(priority.getId());
    }
    
    @Test
    public void testSetLowerOrderForPriority () {
    
        Priority priority = PriorityBuilder.priority().withId(1).build();
        
        issueService.setLowerOrderForPriority(priority);
        
        verify(priorityDao).decreaseOrder(priority.getId());
    }
    
    @Test
    public void testSaveResolution () {
    
        issueService.saveResolution(any(Resolution.class));
        
        verify(resolutionDao).saveOrUpdate(any(Resolution.class));
    }
    
    @Test
    public void testDeleteResolution () {
    
        issueService.deleteResolution(any(Resolution.class));
        
        verify(resolutionDao).delete(any(Resolution.class));
    }
    
    @Test
    public void testGetAllResolutions () {
    
        issueService.getAllResolutions();
        
        verify(resolutionDao).findAll();
    }
    
    @Test
    public void testGetResolutionById () {
    
        issueService.getResolutionById(anyInt());
        
        verify(resolutionDao).findById(anyInt());
    }
    
    @Test
    public void testSetHigherOrderToResolution () {
    
        Resolution resolution = ResolutionBuilder.resolution().withId(1).build();
        
        issueService.setHigherOrderToResolution(resolution);
        
        verify(resolutionDao).increaseOrder(resolution.getId());
    }
    
    @Test
    public void testSetLowerOrderToResolution () {
    
        Resolution resolution = ResolutionBuilder.resolution().withId(1).build();
        
        issueService.setLowerOrderToResolution(resolution);
        
        verify(resolutionDao).decreaseOrder(resolution.getId());
    }
    
    @Test
    public void testGetAllEnvironments () {
    
        issueService.getAllEnvironments();
        
        verify(environmentDao).findAll();
    }
    
    @Test
    public void testGetEnvironmentById () {
    
        issueService.getEnvironmentById(anyInt());
        
        verify(environmentDao).findById(anyInt());
    }
    
    @Test
    public void testSaveEnvironment () {
    
        issueService.saveEnvironment(any(Environment.class));
        
        verify(environmentDao).saveOrUpdate(any(Environment.class));
    }
    
    @Test
    public void testDeleteEnvironment () {
    
        issueService.deleteEnvironment(any(Environment.class));
        
        verify(environmentDao).delete(any(Environment.class));
    }
    
    @Test
    public void testGetAllTags () {
    
        issueService.getAllTags();
        
        verify(tagDao).findAll();
    }
    
    @Test
    public void testGetTagById () {
    
        issueService.getTagById(anyInt());
        
        verify(tagDao).findById(anyInt());
    }
    
    @Test
    public void testSaveTag () {
    
        issueService.saveTag(any(Tag.class));
        
        verify(tagDao).saveOrUpdate(any(Tag.class));
    }
    
    @Test
    public void testDeleteTag () {
    
        issueService.deleteTag(any(Tag.class));
        
        verify(tagDao).delete(any(Tag.class));
    }
    
    @Test
    public void testSaveAttachment () {
    
        issueService.saveAttachment(any(Attachment.class));
        
        verify(attachmentDao).saveOrUpdate(any(Attachment.class));
    }
    
    @Test
    public void testGetAttachmentById () {
    
        issueService.getAttachmentById(anyInt());
        
        verify(attachmentDao).findById(anyInt());
    }
    
    @Test
    public void testGetAllAttachmentsFromIssue () {
    
        Issue issue = IssueBuilder.issue().withId(1).build();
        
        issueService.getAllAttachmentsFromIssue(issue);
        
        verify(attachmentDao).findAllFromIssue(issue.getId());
    }
}
