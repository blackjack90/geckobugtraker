package es.kazbeel.geckobugtracker.service;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import es.kazbeel.geckobugtracker.builders.GroupBuilder;
import es.kazbeel.geckobugtracker.builders.PageBuilder;
import es.kazbeel.geckobugtracker.builders.UserBuilder;
import es.kazbeel.geckobugtracker.model.Group;
import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.repository.GroupDao;
import es.kazbeel.geckobugtracker.repository.UserDao;
import es.kazbeel.geckobugtracker.support.Page;
import es.kazbeel.geckobugtracker.support.PageRequest;


@RunWith(MockitoJUnitRunner.class)
public class UserGroupServiceTest {
    
    @Mock
    private UserDao userDao;
    
    @Mock
    private GroupDao groupDao;
    
    @InjectMocks
    private UserGroupService usergroupService = new UserGroupServiceImpl(userDao, groupDao);
    
    private static List<User> users = new ArrayList<User>();
    
    private static List<Group> groups = new ArrayList<Group>();
    
    
    @BeforeClass
    public static void setUpBeforeClass () throws Exception {
    
        users.add(UserBuilder.user().withId(1).withLoginName("login_name_1").build());
        users.add(UserBuilder.user().withId(2).withLoginName("login_name_2").build());
        users.add(UserBuilder.user().withId(3).withLoginName("login_name_3").build());
        
        groups.add(GroupBuilder.group().withId(1).withName("group_name_1").build());
        groups.add(GroupBuilder.group().withId(2).withName("group_name_2").build());
    }
    
    @Test
    public void testGetUserById () {
    
        usergroupService.getUserById(anyInt());
        
        verify(userDao).findById(anyInt());
    }
    
    @Test
    public void testGetAllEnabledUsers () {
    
        usergroupService.getAllEnabledUsers();
        
        verify(userDao).findAllEnabled();
    }
    
    @Test
    public void testSaveUser () {
    
        usergroupService.saveUser(UserBuilder.user().build());
        
        verify(userDao).saveOrUpdate(any(User.class));
    }
    
    @Test
    public void testGetAllUsers () {
    
        when(userDao.findAll(anyInt(), anyInt())).thenReturn(users);
        when(userDao.countAll()).thenReturn(new Long(users.size()));
        
        PageRequest pageRequest = new PageRequest(0, 5);
        Page<User> page = usergroupService.getAllUsers(pageRequest);
        
        assertThat(page, is(not(nullValue())));
        assertThat(page,
                   is(PageBuilder.<User> page().withPageNumber(0).withMaxPageSize(5).withTotalElements(3).build()));
        verify(userDao).findAll(0, 5);
        verify(userDao).countAll();
    }
    
    @Test
    public void testDeleteUser () {
    
        usergroupService.deleteUser(UserBuilder.user().build());
        
        verify(userDao).delete(any(User.class));
    }
    
    @Test
    public void testGetAllGroups () {
    
        when(groupDao.findAll(anyInt(), anyInt())).thenReturn(groups);
        when(groupDao.countAll()).thenReturn(new Long(groups.size()));
        
        PageRequest pageRequest = new PageRequest(0, 5);
        Page<Group> page = usergroupService.getAllGroups(pageRequest);
        
        assertThat(page, is(not(nullValue())));
        assertThat(page,
                   is(PageBuilder.<Group> page().withPageNumber(0).withMaxPageSize(5).withTotalElements(2).build()));
        verify(groupDao).findAll(0, 5);
        verify(groupDao).countAll();
    }
    
    @Test
    public void testGetAllEnabledGroups () {
    
        usergroupService.getAllEnabledGroups();
        
        verify(groupDao).findAllEnabled();
    }
    
    @Test
    public void testGetGroupById () {
    
        usergroupService.getGroupById(anyInt());
        
        verify(groupDao).findById(anyInt());
    }
    
    @Test
    public void testSaveGroup () {
    
        usergroupService.saveGroup(GroupBuilder.group().build());
        
        verify(groupDao).saveOrUpdate(any(Group.class));
    }
    
    @Test
    public void testDeleteGroup () {
    
        usergroupService.deleteGroup(GroupBuilder.group().build());
        
        verify(groupDao).delete(any(Group.class));
    }
}
