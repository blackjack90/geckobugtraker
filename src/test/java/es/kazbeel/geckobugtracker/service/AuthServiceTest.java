package es.kazbeel.geckobugtracker.service;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import es.kazbeel.geckobugtracker.builders.LoginInfoBuilder;
import es.kazbeel.geckobugtracker.builders.RemembermeTokenBuilder;
import es.kazbeel.geckobugtracker.model.LoginInfo;
import es.kazbeel.geckobugtracker.model.RemembermeToken;
import es.kazbeel.geckobugtracker.repository.LoginInfoDao;
import es.kazbeel.geckobugtracker.repository.RemembermeTokenDao;


@RunWith(MockitoJUnitRunner.class)
public class AuthServiceTest {
    
    @Mock
    private RemembermeTokenDao remembermeTokenDao;
    
    @Mock
    private LoginInfoDao loginInfoDao;
    
    @InjectMocks
    private AuthService authService = new AuthServiceImpl(remembermeTokenDao, loginInfoDao);
    
    private List<RemembermeToken> rmtList = new ArrayList<RemembermeToken>();
    
    
    @Before
    public void setUp () {
    
        rmtList.add(RemembermeTokenBuilder.remembermeToken().withId(1).withUsername("user_name_1").build());
        rmtList.add(RemembermeTokenBuilder.remembermeToken().withId(3).withUsername("user_name_1").build());
    }
    
    @Test
    public void testGetRemembermeTokenByUsername () {
    
        when(remembermeTokenDao.findByUsername(anyString())).thenReturn(null);
        
        authService.getRemembermeTokenByUsername("user_name");
        
        verify(remembermeTokenDao).findByUsername("user_name");
    }
    
    @Test
    public void testGetRemembermeTokenBySeries () {
    
        when(remembermeTokenDao.findBySeries(anyString())).thenReturn(null);
        
        authService.getRemembermeTokenBySeries("series");
        
        verify(remembermeTokenDao).findBySeries("series");
    }
    
    @Test
    public void testSaveTokenFromEntity () {
    
        doNothing().when(remembermeTokenDao).saveOrUpdate(any(RemembermeToken.class));
        
        authService.saveToken(new RemembermeToken());
        
        verify(remembermeTokenDao).saveOrUpdate(new RemembermeToken());
    }
    
    @Test
    public void testSaveTokenFromInfo () {
    
        when(remembermeTokenDao.findBySeries(anyString())).thenReturn(new RemembermeToken());
        doNothing().when(remembermeTokenDao).saveOrUpdate(any(RemembermeToken.class));
        
        authService.saveToken("series", "token", new Date());
        
        verify(remembermeTokenDao).saveOrUpdate(new RemembermeToken());
    }
    
    @Test
    public void testRemoveToken () {
    
        when(remembermeTokenDao.findByUsername(anyString())).thenReturn(rmtList);
        
        authService.deleteTokensFromUsername("user_name_1");
        
        verify(remembermeTokenDao, times(2)).delete(any(RemembermeToken.class));
    }
    
    @Test
    public void testGetLoginInfoByUsername () {
        
        when(loginInfoDao.findByUsername(anyString())).thenReturn(LoginInfoBuilder.loginInfo().withId(1).build());
        
        authService.getLoginInfoByUsername("user_name_1");
        
        verify(loginInfoDao).findByUsername("user_name_1");
    }

    @Test
    public void testResetFailAttemptsFromLoginInfo () {
        doNothing().when(loginInfoDao).resetFailAttempts(anyString());
        
        authService.resetFailAttemptsFromLoginInfo("user_name_1");
        
        verify(loginInfoDao).resetFailAttempts("user_name_1");
    }

    @Test
    public void testIncreaseFailAttemptsFromLoginInfoUserFoundNotLocked () {
        
        LoginInfo loginInfo = LoginInfoBuilder.loginInfo().withId(1).withUsername("user_name").withFailAttempts(1).build();
        
        when(loginInfoDao.findByUsername(anyString())).thenReturn(loginInfo);
        
        authService.increaseFailAttemptsFromLoginInfo("user_name");
        
        assertThat(loginInfo.getFailAttempts(), is(2));
        assertThat(loginInfo.isLocked(), is(false));
        verify(loginInfoDao).saveOrUpdate(any(LoginInfo.class));
    }

    @Test
    public void testIncreaseFailAttemptsFromLoginInfoUserFoundLocked () {
        
        LoginInfo loginInfo = LoginInfoBuilder.loginInfo().withId(1).withUsername("user_name").withFailAttempts(2).build();
        
        when(loginInfoDao.findByUsername(anyString())).thenReturn(loginInfo);
        
        authService.increaseFailAttemptsFromLoginInfo("user_name");
        
        assertThat(loginInfo.getFailAttempts(), is(3));
        assertThat(loginInfo.isLocked(), is(true));
        verify(loginInfoDao).saveOrUpdate(any(LoginInfo.class));
    }

    @Test
    public void testIncreaseFailAttemptsFromLoginInfoUserNotFound () {
        
        when(loginInfoDao.findByUsername(anyString())).thenReturn(null);
        
        authService.increaseFailAttemptsFromLoginInfo("user_name");
        
        verify(loginInfoDao, never()).saveOrUpdate(any(LoginInfo.class));
    }

    @Test
    public void testSaveLoginInfo () {
        
        LoginInfo loginInfo = LoginInfoBuilder.loginInfo().withId(1).build();
        
        doNothing().when(loginInfoDao).saveOrUpdate(loginInfo);
        
        authService.saveLoginInfo(loginInfo);
        
        verify(loginInfoDao).saveOrUpdate(loginInfo);
    }
    
    @Test
    public void testDeleteLoginInfoFromUsername_allOK () {
        
        when(loginInfoDao.findByUsername("user_name")).thenReturn(new LoginInfo());
        
        authService.deleteLoginInfoFromUsername("user_name");
        
        verify(loginInfoDao).findByUsername("user_name");
        verify(loginInfoDao).delete(any(LoginInfo.class));
    }

    @Test
    public void testDeleteLoginInfoFromUsername_loginInfoNotFound () {
        
        when(loginInfoDao.findByUsername("user_name")).thenReturn(null);
        
        authService.deleteLoginInfoFromUsername("user_name");
        
        verify(loginInfoDao).findByUsername("user_name");
    }
}
