package es.kazbeel.geckobugtracker.repository;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;

import java.util.List;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.IssueLinkBuilder;
import es.kazbeel.geckobugtracker.model.IssueLink;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class IssueLinkDaoTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    private IssueLinkDao   issueLinkDao;
    
    
    @Test
    @DatabaseSetup("IssueLinkDaoTest.xml")
    @Transactional
    public void testFindById () {
        
        IssueLink issueLink = issueLinkDao.findById(1);
        
        assertThat(issueLink, is(not(nullValue())));
        assertThat(issueLink, is(IssueLinkBuilder.issueLink().withId(1).build()));
    }
    
    @Test
    @DatabaseSetup("IssueLinkDaoTest.xml")
    @Transactional
    public void testFindAllWithSource () {
        List<IssueLink> issueLinks = issueLinkDao.findAllWithSource(1);
        
        assertThat(issueLinks, is(not(nullValue())));
        assertThat(issueLinks, hasSize(2));
        assertThat(issueLinks, contains(IssueLinkBuilder.issueLink().withId(2).build(),
                                        IssueLinkBuilder.issueLink().withId(1).build()));
    }

    @Test
    @DatabaseSetup("IssueLinkDaoTest.xml")
    @Transactional
    public void testFindAllWithDestination () {
        List<IssueLink> issueLinks = issueLinkDao.findAllWithDestination(1);
        
        assertThat(issueLinks, is(not(nullValue())));
        assertThat(issueLinks, hasSize(1));
        assertThat(issueLinks, contains(IssueLinkBuilder.issueLink().withId(3).build()));
    }

    @Test
    @DatabaseSetup("IssueLinkDaoTest.xml")
    @Transactional
    public void testFindAllRelatedToIssue () {
        List<IssueLink> issueLinks = issueLinkDao.findAllRelatedToIssue(1);
        
        assertThat(issueLinks, is(not(nullValue())));
        assertThat(issueLinks, hasSize(3));
        assertThat(issueLinks, contains(IssueLinkBuilder.issueLink().withId(2).build(),
                                        IssueLinkBuilder.issueLink().withId(1).build(),
                                        IssueLinkBuilder.issueLink().withId(3).build()));
    }
}
