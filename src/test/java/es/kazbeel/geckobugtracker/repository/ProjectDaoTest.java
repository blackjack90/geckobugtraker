package es.kazbeel.geckobugtracker.repository;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;

import java.util.List;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.ProjectBuilder;
import es.kazbeel.geckobugtracker.model.Project;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class ProjectDaoTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    private ProjectDao projectDao;
    
    
    @Test
    @DatabaseSetup("ProjectDaoTest.xml")
    @Transactional
    public void testFindById () {
    
        Project project = projectDao.findById(1L);
        
        assertThat(project, is(not(nullValue())));
        assertThat(project, is(ProjectBuilder.project().withId(1L).withName("project_name_1").build()));
    }
    
    @Test
    @DatabaseSetup("ProjectDaoTest.xml")
    @Transactional
    public void testFindAll () {
    
        List<Project> projects = projectDao.findAll();
        
        assertThat(projects, is(not(nullValue())));
        assertThat(projects, hasSize(5));
        assertThat(projects, contains(ProjectBuilder.project().withId(1L).withName("project_name_1").build(),
                                      ProjectBuilder.project().withId(4L).withName("project_name_4").build(),
                                      ProjectBuilder.project().withId(5L).withName("project_name_5").build(),
                                      ProjectBuilder.project().withId(2L).withName("project_name_2").build(),
                                      ProjectBuilder.project().withId(3L).withName("project_name_3").build()));
    }
    
    @Test
    @DatabaseSetup("ProjectDaoTest.xml")
    @Transactional
    public void testFindBySection () {
    
        List<Project> projects = projectDao.findBySection(1L);
        
        assertThat(projects, is(not(nullValue())));
        assertThat(projects, hasSize(3));
        assertThat(projects, contains(ProjectBuilder.project().withId(1L).withName("project_name_1").build(),
                                      ProjectBuilder.project().withId(4L).withName("project_name_4").build(),
                                      ProjectBuilder.project().withId(5L).withName("project_name_5").build()));
    }
    
    @Test
    @DatabaseSetup("ProjectDaoTest.xml")
    @Transactional
    public void testFindByName () {
    
        Project project = projectDao.findByName("project_name_1");
        
        assertThat(project, is(not(nullValue())));
        assertThat(project, is(ProjectBuilder.project().withId(1L).withName("project_name_1").build()));
    }
    
    @Test
    @DatabaseSetup("ProjectDaoTest.xml")
    @Transactional
    public void testFindByDevLeader () {
    
        List<Project> projects = projectDao.findByDevLeader(1);
        
        assertThat(projects, is(not(nullValue())));
        assertThat(projects, hasSize(2));
        assertThat(projects, contains(ProjectBuilder.project().withId(1L).withName("project_name_1").build(),
                                      ProjectBuilder.project().withId(5L).withName("project_name_5").build()));
    }
    
    @Test
    @DatabaseSetup("ProjectDaoTest.xml")
    @Transactional
    public void testFindByQALeader () {
    
        List<Project> projects = projectDao.findByQALeader(1);
        
        assertThat(projects, is(not(nullValue())));
        assertThat(projects, hasSize(3));
        assertThat(projects, contains(ProjectBuilder.project().withId(2L).withName("project_name_2").build(),
                                      ProjectBuilder.project().withId(3L).withName("project_name_3").build(),
                                      ProjectBuilder.project().withId(4L).withName("project_name_4").build()));
    }
    
    @Test
    @DatabaseSetup("ProjectDaoTest.xml")
    @Transactional
    public void testFindShareableProjectsForComponent () {
    
        List<Project> projects = projectDao.findShareableProjectsForComponent(1L);
        
        assertThat(projects, is(not(nullValue())));
        assertThat(projects, hasSize(3));
        assertThat(projects, contains(ProjectBuilder.project().withId(3L).withName("project_name_3").build(),
                                      ProjectBuilder.project().withId(4L).withName("project_name_4").build(),
                                      ProjectBuilder.project().withId(5L).withName("project_name_5").build()));
    }
}
