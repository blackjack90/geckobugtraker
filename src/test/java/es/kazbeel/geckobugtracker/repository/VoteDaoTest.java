package es.kazbeel.geckobugtracker.repository;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;

import java.util.List;

import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.VoteBuilder;
import es.kazbeel.geckobugtracker.model.Vote;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class VoteDaoTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    private VoteDao voteDao;
    
    
    @Test
    @DatabaseSetup("VoteDaoTest.xml")
    @Transactional
    public void testFindByUser () {
    
        List<Vote> votes = voteDao.findByUser(1, 0, 10);
        
        assertThat(votes, is(not(nullValue())));
        assertThat(votes, hasSize(2));
        assertThat(votes, contains(VoteBuilder.vote().withId(3).build(), VoteBuilder.vote().withId(5).build()));
    }
    
    @Test
    @DatabaseSetup("VoteDaoTest.xml")
    @Transactional
    public void testFindByIssue () {
    
        List<Vote> votes = voteDao.findByIssue(1);
        
        assertThat(votes, is(not(nullValue())));
        assertThat(votes, hasSize(2));
        assertThat(votes, contains(VoteBuilder.vote().withId(2).build(), VoteBuilder.vote().withId(3).build()));
    }
    
    @Test
    @DatabaseSetup("VoteDaoTest.xml")
    @Transactional
    public void testCountByUser () {
    
        Long count = voteDao.countByUser(2);
        
        assertThat(count, is(not(nullValue())));
        assertThat(count, is(new Long(1)));
    }
    
    @Test
    @DatabaseSetup("VoteDaoTest.xml")
    @Transactional
    public void testCountByIssue () {
    
        Long count = voteDao.countByIssue(1);
        
        assertThat(count, is(not(nullValue())));
        assertThat(count, is(new Long(2)));
    }
    
    @Test
    @DatabaseSetup("VoteDaoTest.xml")
    @Transactional
    public void testIsIssueVotedByUser () {
    
        Boolean result = voteDao.isIssueVotedByUser(1, 2);
        
        assertThat(result, is(not(nullValue())));
        assertThat(result, is(true));
        
        result = voteDao.isIssueVotedByUser(2, 2);
        
        assertThat(result, is(not(nullValue())));
        assertThat(result, is(false));
    }
}
