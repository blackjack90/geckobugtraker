package es.kazbeel.geckobugtracker.repository;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

import java.util.List;

import org.hibernate.LazyInitializationException;
import org.hibernate.SessionFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

import es.kazbeel.geckobugtracker.builders.SectionBuilder;
import es.kazbeel.geckobugtracker.model.Section;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:META-INF/spring/app-context.xml"})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
                         TransactionalTestExecutionListener.class,
                         DbUnitTestExecutionListener.class})
public class SectionDaoTest {
    
    @Autowired
    private SessionFactory sessionFactory;
    
    @Autowired
    private SectionDao sectionDao;
    
    
    @Test
    @DatabaseSetup("SectionDaoTest.xml")
    @Transactional
    public void testFindAll () {
    
        List<Section> sections = sectionDao.findAll();
        
        assertThat(sections, is(not(nullValue())));
        assertThat(sections, hasSize(3));
        assertThat(sections, contains(SectionBuilder.section().withId(1L).withName("section_name_1").build(),
                                      SectionBuilder.section().withId(2L).withName("section_name_2").build(),
                                      SectionBuilder.section().withId(3L).withName("section_name_3").build()));
    }
    
    @Test
    @DatabaseSetup("SectionDaoTest.xml")
    @Transactional
    public void testFindById_found () {
    
        Section section = sectionDao.findById(1L);
        
        assertThat(section, is(not(nullValue())));
        assertThat(section, is(SectionBuilder.section().withId(1L).withName("section_name_1").build()));
    }
    
    @Test
    @DatabaseSetup("SectionDaoTest.xml")
    @Transactional
    public void testFindById_notFound () {
    
        Section section = sectionDao.findById(0L);
        
        assertThat(section, is(nullValue()));
    }
    
    @Test(expected = LazyInitializationException.class)
    @DatabaseSetup("SectionDaoTest.xml")
    @Transactional
    public void testFindById_doesNotLoadProjects () {
    
        Section section = sectionDao.findById(1L);
        
        assertThat(section, is(not(nullValue())));
        assertThat(section, is(SectionBuilder.section().withId(1L).withName("section_name_1").build()));
        
        sessionFactory.getCurrentSession().clear();
        
        assertThat(section.getProjects(), is(not(nullValue())));
        
        section.getProjects().size();
    }
    
    @Test
    @DatabaseSetup("SectionDaoTest.xml")
    @Transactional
    public void testFindByName_found () {
    
        Section section = sectionDao.findByName("section_name_1");
        
        assertThat(section, is(not(nullValue())));
        assertThat(section, is(SectionBuilder.section().withId(1L).withName("section_name_1").build()));
    }
    
    @Test
    @DatabaseSetup("SectionDaoTest.xml")
    @Transactional
    public void testFindByName_notFound () {
    
        Section section = sectionDao.findByName("unknown_section_name");
        
        assertThat(section, is(nullValue()));
    }
    
    @Test
    @DatabaseSetup("SectionDaoTest.xml")
    @Transactional
    public void testIncreaseSortValue () {
    
        Section section = (Section) sessionFactory.getCurrentSession().get(Section.class, 1L);
        
        assertThat(section, is(not(nullValue())));
        assertThat(section.getSortValue(), is(0));
        
        sessionFactory.getCurrentSession().clear();
        
        sectionDao.increaseSortValue(1L);
        
        section = (Section) sessionFactory.getCurrentSession().get(Section.class, 1L);
        
        assertThat(section, is(not(nullValue())));
        assertThat(section.getSortValue(), is(1));
    }
    
    @Test
    @DatabaseSetup("SectionDaoTest.xml")
    @Transactional
    public void testDecreaseSortValue () {
    
        Section section = (Section) sessionFactory.getCurrentSession().get(Section.class, 3L);
        
        assertThat(section, is(not(nullValue())));
        assertThat(section.getSortValue(), is(1));
        
        sessionFactory.getCurrentSession().clear();
        
        sectionDao.decreaseSortValue(3L);
        
        section = (Section) sessionFactory.getCurrentSession().get(Section.class, 3L);
        
        assertThat(section, is(not(nullValue())));
        assertThat(section.getSortValue(), is(0));
    }
    
    @Test
    @DatabaseSetup("SectionDaoTest.xml")
    @Transactional
    public void testDecreaseSortValue_valueIsAlwaysGreaterThanZero () {
    
        Section section = (Section) sessionFactory.getCurrentSession().get(Section.class, 1L);
        
        assertThat(section, is(not(nullValue())));
        assertThat(section.getSortValue(), is(0));
        
        sessionFactory.getCurrentSession().clear();
        
        sectionDao.decreaseSortValue(1L);
        
        section = (Section) sessionFactory.getCurrentSession().get(Section.class, 1L);
        
        assertThat(section, is(not(nullValue())));
        assertThat(section.getSortValue(), is(0));
    }
}
