package es.kazbeel.geckobugtracker.web.secure;


import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import es.kazbeel.geckobugtracker.builders.LoginInfoBuilder;
import es.kazbeel.geckobugtracker.builders.UserBuilder;
import es.kazbeel.geckobugtracker.model.LoginInfo;
import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.repository.LoginInfoDao;
import es.kazbeel.geckobugtracker.repository.UserDao;


@RunWith(MockitoJUnitRunner.class)
public class CustomUserDetailsServiceTest {
    
    @Mock
    private UserDao userDao;
    
    @Mock
    private LoginInfoDao loginInfoDao;
    
    @InjectMocks
    private CustomUserDetailsService customUserDetailsService;
    
    
    @Test
    public void testLoadByUserName_firstLogIn () {
    
        User user = UserBuilder.user().withLoginName("login_name").withPassword("password").withEnabled(true).build();
        
        when(userDao.findByLoginName(anyString())).thenReturn(user);
        when(loginInfoDao.findByUsername(anyString())).thenReturn(null);
        
        CurrentUserDetails userDetails = (CurrentUserDetails) customUserDetailsService.loadUserByUsername(user.getLoginName());
        
        verify(userDao).findByLoginName(anyString());
        verify(loginInfoDao).findByUsername(anyString());
        verify(loginInfoDao).save(any(LoginInfo.class));
        
        assertThat(userDetails.getCurrentUser(), is(user));
        assertThat(userDetails.getAuthorities(), hasSize(1));
        assertThat(userDetails.getAuthorities(), contains((GrantedAuthority) new SimpleGrantedAuthority("ROLE_USER")));
    }
    
    @Test
    public void testLoadByUserName_nonFirstLogIn () {
    
        User user = UserBuilder.user().withLoginName("login_name").withPassword("password").withEnabled(true).build();
        LoginInfo loginInfo = LoginInfoBuilder.loginInfo().withUsername("login_name").withLocked(false).build();
        
        when(userDao.findByLoginName(anyString())).thenReturn(user);
        when(loginInfoDao.findByUsername(anyString())).thenReturn(loginInfo);
        
        CurrentUserDetails userDetails = (CurrentUserDetails) customUserDetailsService.loadUserByUsername(user.getLoginName());
        
        verify(userDao).findByLoginName(anyString());
        verify(loginInfoDao).findByUsername(anyString());
        verify(loginInfoDao, never()).save(any(LoginInfo.class));
        
        assertThat(userDetails.getCurrentUser(), is(user));
        assertThat(userDetails.getAuthorities(), hasSize(1));
        assertThat(userDetails.getAuthorities(), contains((GrantedAuthority) new SimpleGrantedAuthority("ROLE_USER")));
    }
    
    @Test
    public void testLoadByUserName_isAdministrator () {
    
        User user = UserBuilder.user().withLoginName("admin").withPassword("password").withEnabled(true).build();
        
        when(userDao.findByLoginName(anyString())).thenReturn(user);
        when(loginInfoDao.findByUsername(anyString())).thenReturn(null);
        
        CurrentUserDetails userDetails = (CurrentUserDetails) customUserDetailsService.loadUserByUsername(user.getLoginName());
        
        verify(userDao).findByLoginName(anyString());
        verify(loginInfoDao).findByUsername(anyString());
        verify(loginInfoDao).save(any(LoginInfo.class));
        
        assertThat(userDetails.getCurrentUser(), is(user));
        assertThat(userDetails.getAuthorities(), hasSize(2));
        assertThat(userDetails.getAuthorities(),
                   contains((GrantedAuthority) new SimpleGrantedAuthority("ROLE_ADMIN"),
                            (GrantedAuthority) new SimpleGrantedAuthority("ROLE_USER")));
    }
    
    @Test(expected = UsernameNotFoundException.class)
    public void testLoadByUserName_userNotFound () {
    
        when(userDao.findByLoginName(anyString())).thenReturn(null);
        
        customUserDetailsService.loadUserByUsername(anyString());
        
        verify(userDao).findByLoginName(anyString());
    }
}
