package es.kazbeel.geckobugtracker.web.controller.admin;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import es.kazbeel.geckobugtracker.model.Section;
import es.kazbeel.geckobugtracker.service.ProjectService;


@RunWith(MockitoJUnitRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class SectionControllerTest {
    
    @Mock
    private ProjectService projectService;
    
    @InjectMocks
    private SectionController sectionController;
    
    private MockMvc mockMvc;
    
    
    @Before
    public void setup () {
    
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        
        mockMvc = MockMvcBuilders.standaloneSetup(sectionController).setViewResolvers(viewResolver).build();
    }
    
    @Test
    public void testViewSections () throws Exception {
    
        mockMvc.perform(get("/admin/sections/ViewSections"))
               .andExpect(view().name("/admin/sections/ViewSections"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("sections"));
        
        verify(projectService).getAllSections();
    }
    
    @Test
    public void testCreateSectionGet () throws Exception {
    
        mockMvc.perform(get("/admin/sections/CreateSection"))
               .andExpect(view().name("/admin/sections/CreateSection"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("section"));
    }
    
    @Test
    public void testCreateSectionPost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/sections/CreateSection").param("name", "section_name"))
               .andExpect(redirectedUrl("/admin/sections/ViewSections"))
               .andExpect(model().hasNoErrors())
               .andExpect(model().size(0));
        
        verify(projectService).saveSection(any(Section.class));
    }
    
    @Test
    public void testCreateSectionPost_throwsDataIntegrityViolation () throws Exception {
    
        doThrow(DataIntegrityViolationException.class).when(projectService).saveSection(any(Section.class));
        
        mockMvc.perform(post("/admin/sections/CreateSection").param("name", "section_name"))
               .andExpect(view().name("/admin/sections/CreateSection"))
               .andExpect(model().hasNoErrors())
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("section"));
        
        verify(projectService).saveSection(any(Section.class));
    }
    
    @Test
    public void testCreateSectionPost_withBindingResultError () throws Exception {
    
        mockMvc.perform(post("/admin/sections/CreateSection"))
               .andExpect(view().name("/admin/sections/CreateSection"))
               .andExpect(model().hasErrors())
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("section"));
        
        verify(projectService, never()).saveSection(any(Section.class));
    }
    
    @Test
    public void testEditSectionGet () throws Exception {
    
        when(projectService.getSectionById(anyLong())).thenReturn(new Section());
        
        mockMvc.perform(get("/admin/sections/EditSection").param("id", "0"))
               .andExpect(view().name("/admin/sections/EditSection"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("section"));
        
        verify(projectService).getSectionById(0L);
    }
    
    @Test
    public void testEditSectionPost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/sections/EditSection").param("id", "0").param("name", "section_name"))
               .andExpect(redirectedUrl("/admin/sections/ViewSections"))
               .andExpect(model().hasNoErrors())
               .andExpect(model().size(0));
        
        verify(projectService).saveSection(any(Section.class));
    }
    
    @Test
    public void testEditSectionPost_throwsDataIntegrityViolation () throws Exception {
    
        doThrow(DataIntegrityViolationException.class).when(projectService).saveSection(any(Section.class));
        
        mockMvc.perform(post("/admin/sections/EditSection").param("id", "0").param("name", "section_name"))
               .andExpect(view().name("/admin/sections/EditSection"))
               .andExpect(model().hasNoErrors())
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("section"));
        
        verify(projectService).saveSection(any(Section.class));
    }
    
    @Test
    public void testEditSectionPost_withBindingResultError () throws Exception {
    
        mockMvc.perform(post("/admin/sections/EditSection").param("id", "0"))
               .andExpect(view().name("/admin/sections/EditSection"))
               .andExpect(model().hasErrors())
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("section"));
        
        verify(projectService, never()).saveSection(any(Section.class));
    }
    
    @Test
    public void testDeleteSection_allOK () throws Exception {
    
        mockMvc.perform(get("/admin/sections/DeleteSection").param("id", "0"))
               .andExpect(redirectedUrl("/admin/sections/ViewSections"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgSuccess"));
        
        verify(projectService).getSectionById(anyLong());
        verify(projectService).deleteSection(any(Section.class));
    }
    
    @Test
    public void testDeleteSection_entityDoesNotExist () throws Exception {
    
        doThrow(new IllegalArgumentException()).when(projectService).deleteSection(any(Section.class));
        
        mockMvc.perform(get("/admin/sections/DeleteSection").param("id", "0"))
               .andExpect(redirectedUrl("/admin/sections/ViewSections"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(projectService).getSectionById(anyLong());
        verify(projectService).deleteSection(any(Section.class));
    }
    
    @Test
    public void testDeleteSection_throwsDataIntegrityViolation () throws Exception {
    
        doThrow(new DataIntegrityViolationException("")).when(projectService).deleteSection(any(Section.class));
        
        mockMvc.perform(get("/admin/sections/DeleteSection").param("id", "0"))
               .andExpect(redirectedUrl("/admin/sections/ViewSections"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(projectService).getSectionById(anyLong());
        verify(projectService).deleteSection(any(Section.class));
    }
    
    @Test
    public void testSetSectionOrder_moveUp () throws Exception {
    
        when(projectService.getSectionById(anyLong())).thenReturn(new Section());
        
        mockMvc.perform(get("/admin/sections/SortSection").param("id", "0").param("move", "up"))
               .andExpect(redirectedUrl("/admin/sections/ViewSections"))
               .andExpect(flash().attributeCount(0));
        
        verify(projectService).getSectionById(anyLong());
        verify(projectService).setHigherOrderToSection(any(Section.class));
        verify(projectService, never()).setLowerOrderToSection(any(Section.class));
    }
    
    @Test
    public void testSetSectionOrder_moveDown () throws Exception {
    
        when(projectService.getSectionById(anyLong())).thenReturn(new Section());
        
        mockMvc.perform(get("/admin/sections/SortSection").param("id", "0").param("move", "down"))
               .andExpect(redirectedUrl("/admin/sections/ViewSections"))
               .andExpect(flash().attributeCount(0));
        
        verify(projectService).getSectionById(anyLong());
        verify(projectService).setLowerOrderToSection(any(Section.class));
        verify(projectService, never()).setHigherOrderToSection(any(Section.class));
    }
    
    @Test
    public void testSetSectionOrder_invalidId () throws Exception {
    
        doThrow(new IllegalArgumentException()).when(projectService).setHigherOrderToSection(any(Section.class));
        
        mockMvc.perform(get("/admin/sections/SortSection").param("id", "0").param("move", "up"))
               .andExpect(redirectedUrl("/admin/sections/ViewSections"))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(projectService).getSectionById(anyLong());
        verify(projectService, never()).setLowerOrderToSection(any(Section.class));
        verify(projectService, never()).setHigherOrderToSection(any(Section.class));
    }
    
    @Test
    public void testSetSectionOrder_invalidMoveAction () throws Exception {
    
        when(projectService.getSectionById(anyLong())).thenReturn(new Section());
        
        mockMvc.perform(get("/admin/sections/SortSection").param("id", "0").param("move", "invalid"))
               .andExpect(redirectedUrl("/admin/sections/ViewSections"))
               .andExpect(flash().attributeCount(0));
        
        verify(projectService).getSectionById(anyLong());
        verify(projectService, never()).setLowerOrderToSection(any(Section.class));
        verify(projectService, never()).setHigherOrderToSection(any(Section.class));
    }
    
    @Test
    public void testDetailsSection_allOK () throws Exception {
    
        when(projectService.getSectionByIdWithProjects(anyLong())).thenReturn(new Section());
        
        mockMvc.perform(get("/admin/sections/DetailsSection").param("id", "0"))
               .andExpect(view().name("/admin/sections/DetailsSection"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("section"));
        
        verify(projectService).getSectionByIdWithProjects(anyLong());
    }
    
    @Test
    public void testDetailsSection_throwsResourceNotFound () throws Exception {
    
        mockMvc.perform(get("/admin/sections/DetailsSection").param("id", "0"))
               .andExpect(status().isNotFound());
        
        verify(projectService).getSectionByIdWithProjects(anyLong());
    }
}
