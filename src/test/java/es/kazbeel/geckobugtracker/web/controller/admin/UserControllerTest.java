package es.kazbeel.geckobugtracker.web.controller.admin;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import es.kazbeel.geckobugtracker.builders.PageBuilder;
import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.service.AuthService;
import es.kazbeel.geckobugtracker.service.UserGroupService;
import es.kazbeel.geckobugtracker.support.Page;
import es.kazbeel.geckobugtracker.support.PageRequest;


@RunWith(MockitoJUnitRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class UserControllerTest {
    
    @Mock
    private UserGroupService userGroupService;
    
    @Mock
    private AuthService authService;
    
    @Mock
    private BCryptPasswordEncoder passwordEncoder;
    
    @InjectMocks
    private UserController userController;
    
    private MockMvc mockMvc;
    
    
    @Before
    public void setup () {
    
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        
        mockMvc = MockMvcBuilders.standaloneSetup(userController).setViewResolvers(viewResolver).build();
    }
    
    @Test
    public void testViewUsers_allOK () throws Exception {
    
        Page<User> dummyUserPage = PageBuilder.<User> page().withMaxPageSize(1).withTotalElements(1).build();
        
        when(userGroupService.getAllUsers(any(PageRequest.class))).thenReturn(dummyUserPage);
        
        mockMvc.perform(get("/admin/users/ViewUsers").param("page", "0"))
               .andExpect(view().name("/admin/users/ViewUsers"))
               .andExpect(model().size(5))
               .andExpect(model().attributeExists("groupList"))
               .andExpect(model().attributeExists("users"))
               .andExpect(model().attributeExists("totalPages"))
               .andExpect(model().attributeExists("isFirst"))
               .andExpect(model().attributeExists("isLast"));
        
        verify(userGroupService).getAllUsers(any(PageRequest.class));
    }
    
    @Test
    public void testViewUsers_invalidPage () throws Exception {
    
        Page<User> dummyUserPage = PageBuilder.<User> page().withMaxPageSize(1).withTotalElements(1).build();
        
        when(userGroupService.getAllUsers(any(PageRequest.class))).thenReturn(dummyUserPage);
        
        mockMvc.perform(get("/admin/users/ViewUsers").param("page", "2"))
               .andExpect(redirectedUrl("/admin/users/ViewUsers"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("groupList"));
        
        verify(userGroupService).getAllUsers(any(PageRequest.class));
    }
    
    @Test
    public void testCreateUserGet () throws Exception {
    
        mockMvc.perform(get("/admin/users/CreateUser"))
               .andExpect(view().name("/admin/users/CreateUser"))
               .andExpect(model().size(2))
               .andExpect(model().attributeExists("user"))
               .andExpect(model().attributeExists("groupList"));
    }
    
    @Test
    public void testCreateUserPost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/users/CreateUser").param("loginName", "login_name")
                                                       .param("password", "password")
                                                       .param("firstName", "first_name")
                                                       .param("lastName", "last_name")
                                                       .param("email", "email@email.com"))
               .andExpect(redirectedUrl("/admin/users/ViewUsers"))
               .andExpect(model().hasNoErrors())
               .andExpect(model().size(2))
               .andExpect(model().attributeExists("user"))
               .andExpect(model().attributeExists("groupList"));
        
        verify(passwordEncoder).encode(any(CharSequence.class));
        verify(userGroupService).saveUser(any(User.class));
    }
    
    @Test
    public void testCreateUserPost_withBindingResultErrors () throws Exception {
    
        mockMvc.perform(post("/admin/users/CreateUser").param("firstName", "")
                                                       .param("lastName", "last_name")
                                                       .param("email", "email@email.com"))
               .andExpect(view().name("/admin/users/CreateUser"))
               .andExpect(model().hasErrors())
               .andExpect(model().size(3))
               .andExpect(model().attributeExists("user"))
               .andExpect(model().attributeExists("groupList"))
               .andExpect(model().attributeExists("msgError"));
        
        verify(passwordEncoder, never()).encode(any(CharSequence.class));
        verify(userGroupService, never()).saveUser(any(User.class));
    }
    
    @Test
    public void testCreateUserPost_throwsDataIntegrityViolationException () throws Exception {
    
        doThrow(new DataIntegrityViolationException("Duplicate entry")).when(userGroupService)
                                                                       .saveUser(any(User.class));
        
        mockMvc.perform(post("/admin/users/CreateUser").param("loginName", "login_name")
                                                       .param("password", "password")
                                                       .param("firstName", "first_name")
                                                       .param("lastName", "last_name")
                                                       .param("email", "email@email.com"))
               .andExpect(view().name("/admin/users/CreateUser"))
               .andExpect(model().size(2))
               .andExpect(model().attributeExists("user"))
               .andExpect(model().attributeExists("groupList"));
        
        verify(passwordEncoder).encode(any(CharSequence.class));
        verify(userGroupService).saveUser(any(User.class));
    }
    
    @Test
    public void testEditUserGet_allOK () throws Exception {
    
        when(userGroupService.getUserById(anyInt())).thenReturn(new User());
        
        mockMvc.perform(get("/admin/users/EditUser").param("id", "0"))
               .andExpect(view().name("/admin/users/EditUser"))
               .andExpect(model().size(2))
               .andExpect(model().attributeExists("user"))
               .andExpect(model().attributeExists("groupList"));
        
        verify(userGroupService).getUserById(0);
    }
    
    @Test
    public void testEditUserGet_userDoesNotExist () throws Exception {
    
        when(userGroupService.getUserById(anyInt())).thenReturn(null);
        
        mockMvc.perform(get("/admin/users/EditUser").param("id", "0"))
               .andExpect(redirectedUrl("/admin/users/ViewUsers"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("groupList"));
        
        verify(userGroupService).getUserById(0);
    }
    
    @Test
    public void testEditUserPost_allOK () throws Exception {
    
        when(userGroupService.getUserById(anyInt())).thenReturn(new User());
        
        mockMvc.perform(post("/admin/users/EditUser").param("id", "0")
                                                     .param("loginName", "login_name")
                                                     .param("password", "password")
                                                     .param("firstName", "first_name")
                                                     .param("lastName", "last_name")
                                                     .param("email", "email@email.com"))
               .andExpect(model().hasNoErrors())
               .andExpect(redirectedUrl("/admin/users/ViewUsers"))
               .andExpect(model().size(2))
               .andExpect(model().attributeExists("user"))
               .andExpect(model().attributeExists("groupList"));
        
        verify(userGroupService).getUserById(0);
        verify(userGroupService).saveUser(any(User.class));
    }
    
    @Test
    public void testEditUserPost_withBindingResultErrors () throws Exception {
    
        mockMvc.perform(post("/admin/users/EditUser").param("id", "0")
                                                     .param("firstName", "")
                                                     .param("lastName", "last_name")
                                                     .param("email", "email@email.com"))
               .andExpect(view().name("/admin/users/EditUser"))
               .andExpect(model().hasErrors())
               .andExpect(model().size(3))
               .andExpect(model().attributeExists("user"))
               .andExpect(model().attributeExists("groupList"))
               .andExpect(model().attributeExists("msgError"));
        
        verify(userGroupService, never()).getUserById(0);
        verify(userGroupService, never()).saveUser(any(User.class));
    }
    
    @Test
    public void testEditUserPost_userDoesNotExist () throws Exception {
    
        when(userGroupService.getUserById(anyInt())).thenReturn(null);
        
        mockMvc.perform(post("/admin/users/EditUser").param("id", "0")
                                                     .param("loginName", "login_name")
                                                     .param("password", "password")
                                                     .param("firstName", "first_name")
                                                     .param("lastName", "last_name")
                                                     .param("email", "email@email.com"))
               .andExpect(redirectedUrl("/admin/users/ViewUsers"))
               .andExpect(model().hasNoErrors())
               .andExpect(model().size(2))
               .andExpect(model().attributeExists("user"))
               .andExpect(model().attributeExists("groupList"));
        
        verify(userGroupService).getUserById(0);
        verify(userGroupService, never()).saveUser(any(User.class));
    }
    
    @Test
    public void testEditUserPost_throwsException () throws Exception {
    
        when(userGroupService.getUserById(anyInt())).thenReturn(new User());
        
        doThrow(DataIntegrityViolationException.class).when(userGroupService)
                                                                       .saveUser(any(User.class));
        
        mockMvc.perform(post("/admin/users/EditUser").param("id", "0")
                                                     .param("loginName", "login_name")
                                                     .param("password", "password")
                                                     .param("firstName", "first_name")
                                                     .param("lastName", "last_name")
                                                     .param("email", "email@email.com"))
               .andExpect(model().hasNoErrors())
               .andExpect(view().name("/admin/users/EditUser"))
               .andExpect(model().size(2))
               .andExpect(model().attributeExists("user"))
               .andExpect(model().attributeExists("groupList"));
        
        verify(userGroupService).getUserById(0);
        verify(userGroupService).saveUser(any(User.class));
    }
    
    @Test
    public void testDeleteUser_allOK () throws Exception {
    
        when(userGroupService.getUserById(anyInt())).thenReturn(new User());
        
        mockMvc.perform(get("/admin/users/DeleteUser").param("id", "0"))
               .andExpect(redirectedUrl("/admin/users/ViewUsers"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgSuccess"));
        
        verify(userGroupService).getUserById(0);
        verify(userGroupService).deleteUser(any(User.class));
        verify(authService).deleteLoginInfoFromUsername(anyString());
        verify(authService).deleteTokensFromUsername(anyString());
    }
    
    @Test
    public void testDeleteUser_userDoesNotExist () throws Exception {
    
        when(userGroupService.getUserById(anyInt())).thenReturn(null);
        
        mockMvc.perform(get("/admin/users/DeleteUser").param("id", "0"))
               .andExpect(redirectedUrl("/admin/users/ViewUsers"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(userGroupService).getUserById(0);
        verify(userGroupService, never()).deleteUser(any(User.class));
        verify(authService, never()).deleteLoginInfoFromUsername(anyString());
        verify(authService, never()).deleteTokensFromUsername(anyString());
    }
    
    @Test
    public void testDeleteUser_throwsDataIntegrityViolationException () throws Exception {
    
        when(userGroupService.getUserById(anyInt())).thenReturn(new User());
        doThrow(DataIntegrityViolationException.class).when(userGroupService).deleteUser(any(User.class));
        
        mockMvc.perform(get("/admin/users/DeleteUser").param("id", "0"))
               .andExpect(redirectedUrl("/admin/users/ViewUsers"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(userGroupService).getUserById(0);
        verify(userGroupService).deleteUser(any(User.class));
        verify(authService, never()).deleteLoginInfoFromUsername(anyString());
        verify(authService, never()).deleteTokensFromUsername(anyString());
    }
}
