package es.kazbeel.geckobugtracker.web.controller.admin;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import es.kazbeel.geckobugtracker.builders.PageBuilder;
import es.kazbeel.geckobugtracker.model.Group;
import es.kazbeel.geckobugtracker.service.UserGroupService;
import es.kazbeel.geckobugtracker.support.Page;
import es.kazbeel.geckobugtracker.support.PageRequest;


@RunWith(MockitoJUnitRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class GroupControllerTest {
    
    @Mock
    private UserGroupService userGroupService;
    
    @InjectMocks
    private GroupController groupController;
    
    private MockMvc mockMvc;
    
    
    @Before
    public void setup () {
    
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        
        mockMvc = MockMvcBuilders.standaloneSetup(groupController).setViewResolvers(viewResolver).build();
    }
    
    @Test
    public void testViewGroups_allOK () throws Exception {
    
        Page<Group> dummyGroupPage = PageBuilder.<Group> page().withMaxPageSize(1).withTotalElements(1).build();
        
        when(userGroupService.getAllGroups(any(PageRequest.class))).thenReturn(dummyGroupPage);
        
        mockMvc.perform(get("/admin/groups/ViewGroups").param("page", "0"))
               .andExpect(view().name("/admin/groups/ViewGroups"))
               .andExpect(model().size(5))
               .andExpect(model().attributeExists("userList"))
               .andExpect(model().attributeExists("groups"))
               .andExpect(model().attributeExists("totalPages"))
               .andExpect(model().attributeExists("isFirst"))
               .andExpect(model().attributeExists("isLast"));
        
        verify(userGroupService).getAllGroups(any(PageRequest.class));
    }
    
    @Test
    public void testViewGroups_invalidPage () throws Exception {
    
        Page<Group> dummyGroupPage = PageBuilder.<Group> page().withMaxPageSize(1).withTotalElements(1).build();
        
        when(userGroupService.getAllGroups(any(PageRequest.class))).thenReturn(dummyGroupPage);
        
        mockMvc.perform(get("/admin/groups/ViewGroups").param("page", "100"))
               .andExpect(redirectedUrl("/admin/groups/ViewGroups"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("userList"));
        
        verify(userGroupService).getAllGroups(any(PageRequest.class));
    }
    
    @Test
    public void testCreateGroupGet () throws Exception {
    
        mockMvc.perform(get("/admin/groups/CreateGroup"))
               .andExpect(view().name("/admin/groups/CreateGroup"))
               .andExpect(model().size(2))
               .andExpect(model().attributeExists("group"))
               .andExpect(model().attributeExists("userList"));
    }
    
    @Test
    public void testCreateGroupPost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/groups/CreateGroup").param("name", "group_name"))
               .andExpect(redirectedUrl("/admin/groups/ViewGroups"))
               .andExpect(model().size(2))
               .andExpect(model().attributeExists("group"))
               .andExpect(model().attributeExists("userList"));
        
        verify(userGroupService).saveGroup(any(Group.class));
    }
    
    @Test
    public void testCreateGroupPost_withBindingResultErrors () throws Exception {
    
        mockMvc.perform(post("/admin/groups/CreateGroup").param("name", ""))
               .andExpect(view().name("/admin/groups/CreateGroup"))
               .andExpect(model().size(2))
               .andExpect(model().attributeExists("group"))
               .andExpect(model().attributeExists("userList"));
        
        verify(userGroupService, never()).saveGroup(any(Group.class));
    }
    
    @Test
    public void testCreateGroupPost_throwsException () throws Exception {
    
        doThrow(new DataIntegrityViolationException("Duplicate entry")).when(userGroupService)
                                                                       .saveGroup(any(Group.class));
        
        mockMvc.perform(post("/admin/groups/CreateGroup").param("name", "duplicate_name"))
               .andExpect(view().name("/admin/groups/CreateGroup"))
               .andExpect(model().size(2))
               .andExpect(model().attributeExists("group"))
               .andExpect(model().attributeExists("userList"));
        
        verify(userGroupService).saveGroup(any(Group.class));
    }
    
    @Test
    public void testEditGroupGet_allOK () throws Exception {
    
        when(userGroupService.getGroupById(anyInt())).thenReturn(new Group());
        
        mockMvc.perform(get("/admin/groups/EditGroup").param("id", "0"))
               .andExpect(view().name("/admin/groups/EditGroup"))
               .andExpect(model().size(2))
               .andExpect(model().attributeExists("group"))
               .andExpect(model().attributeExists("userList"));
        
        verify(userGroupService).getGroupById(anyInt());
    }
    
    @Test
    public void testEditGroupPost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/groups/EditGroup").param("id", "0").param("name", "not_empty"))
               .andExpect(redirectedUrl("/admin/groups/ViewGroups"))
               .andExpect(model().size(2))
               .andExpect(model().attributeExists("group"))
               .andExpect(model().attributeExists("userList"));
        
        verify(userGroupService).saveGroup(any(Group.class));
    }
    
    @Test
    public void testEditGroupPost_withBindingResultErrors () throws Exception {
    
        mockMvc.perform(post("/admin/groups/EditGroup").param("id", "0").param("name", ""))
               .andExpect(view().name("/admin/groups/EditGroup"))
               .andExpect(model().size(2))
               .andExpect(model().attributeExists("group"))
               .andExpect(model().attributeExists("userList"));
        
        verify(userGroupService, never()).saveGroup(any(Group.class));
    }
    
    @Test
    public void testEditGroupPost_throwsException () throws Exception {
    
        doThrow(new DataIntegrityViolationException("Duplicate entry")).when(userGroupService)
                                                                       .saveGroup(any(Group.class));
        
        mockMvc.perform(post("/admin/groups/EditGroup").param("id", "0").param("name", "duplicate_name"))
               .andExpect(view().name("/admin/groups/EditGroup"))
               .andExpect(model().size(2))
               .andExpect(model().attributeExists("group"))
               .andExpect(model().attributeExists("userList"));
        
        verify(userGroupService).saveGroup(any(Group.class));
    }
    
    @Test
    public void testDeleteGroup_allOK () throws Exception {
    
        mockMvc.perform(get("/admin/groups/DeleteGroup").param("id", "0"))
               .andExpect(redirectedUrl("/admin/groups/ViewGroups"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgSuccess"));
        
        verify(userGroupService).getGroupById(anyInt());
        verify(userGroupService).deleteGroup(any(Group.class));
    }
    
    @Test
    public void testDeleteGroup_entityDoesNotExist () throws Exception {
    
        doThrow(new IllegalArgumentException()).when(userGroupService).deleteGroup(any(Group.class));
        
        mockMvc.perform(get("/admin/groups/DeleteGroup").param("id", "0"))
               .andExpect(redirectedUrl("/admin/groups/ViewGroups"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(userGroupService).getGroupById(anyInt());
        verify(userGroupService).deleteGroup(any(Group.class));
    }
    
    @Test
    public void testDeleteGroup_throwsDataIntegrityViolation () throws Exception {
    
        doThrow(new DataIntegrityViolationException("")).when(userGroupService).deleteGroup(any(Group.class));
        
        mockMvc.perform(get("/admin/groups/DeleteGroup").param("id", "0"))
               .andExpect(redirectedUrl("/admin/groups/ViewGroups"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(userGroupService).getGroupById(anyInt());
        verify(userGroupService).deleteGroup(any(Group.class));
    }
}
