package es.kazbeel.geckobugtracker.web.controller.admin;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import es.kazbeel.geckobugtracker.model.Tag;
import es.kazbeel.geckobugtracker.service.IssueService;


@RunWith(MockitoJUnitRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class TagControllerTest {
    
    @Mock
    private IssueService issueService;
    
    @InjectMocks
    private TagController tagController;
    
    private MockMvc mockMvc;
    
    
    @Before
    public void setup () {
    
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        
        mockMvc = MockMvcBuilders.standaloneSetup(tagController).setViewResolvers(viewResolver).build();
    }
    
    @Test
    public void testViewTags () throws Exception {
    
        mockMvc.perform(get("/admin/tags/ViewTags"))
               .andExpect(view().name("/admin/tags/ViewTags"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("tags"));
        
        verify(issueService).getAllTags();
    }
    
    @Test
    public void testCreateTagGet () throws Exception {
    
        mockMvc.perform(get("/admin/tags/CreateTag"))
               .andExpect(view().name("/admin/tags/CreateTag"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("tag"));
    }
    
    @Test
    public void testCreateTagPost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/tags/CreateTag").param("name", "not_empty"))
               .andExpect(redirectedUrl("/admin/tags/ViewTags"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("tag"));
        
        verify(issueService).saveTag(any(Tag.class));
    }
    
    @Test
    public void testCreateTagPost_withBindingResultErrors () throws Exception {
    
        mockMvc.perform(post("/admin/tags/CreateTag").param("name", ""))
               .andExpect(view().name("/admin/tags/CreateTag"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("tag"));
        
        verify(issueService, never()).saveTag(any(Tag.class));
    }
    
    @Test
    public void testCreateTagPost_throwsException () throws Exception {
    
        doThrow(new DataIntegrityViolationException("Duplicate entry")).when(issueService)
                                                                       .saveTag(any(Tag.class));
        
        mockMvc.perform(post("/admin/tags/CreateTag").param("name", "duplicate_name"))
               .andExpect(view().name("/admin/tags/CreateTag"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("tag"));
        
        verify(issueService).saveTag(any(Tag.class));
    }
    
    @Test
    public void testEditTagGet_allOK () throws Exception {
    
        when(issueService.getTagById(anyInt())).thenReturn(new Tag());
        
        mockMvc.perform(get("/admin/tags/EditTag").param("id", "0"))
               .andExpect(view().name("/admin/tags/EditTag"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("tag"));
        
        verify(issueService).getTagById(anyInt());
    }
    
    @Test
    public void testEditTagPost_allOK () throws Exception {
    
        mockMvc.perform(post("/admin/tags/EditTag").param("id", "0").param("name", "not_empty"))
               .andExpect(redirectedUrl("/admin/tags/ViewTags"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("tag"));
        
        verify(issueService).saveTag(any(Tag.class));
    }
    
    @Test
    public void testEditTagPost_withBindingResultErrors () throws Exception {
    
        mockMvc.perform(post("/admin/tags/EditTag").param("id", "0").param("name", ""))
               .andExpect(view().name("/admin/tags/EditTag"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("tag"));
        
        verify(issueService, never()).saveTag(any(Tag.class));
    }
    
    @Test
    public void testEditTagPost_throwsException () throws Exception {
    
        doThrow(new DataIntegrityViolationException("Duplicate entry")).when(issueService)
                                                                       .saveTag(any(Tag.class));
        
        mockMvc.perform(post("/admin/tags/EditTag").param("id", "0").param("name", "duplicate_name"))
               .andExpect(view().name("/admin/tags/EditTag"))
               .andExpect(model().size(1))
               .andExpect(model().attributeExists("tag"));
        
        verify(issueService).saveTag(any(Tag.class));
    }
    
    @Test
    public void testDeleteTag_allOK () throws Exception {
    
        mockMvc.perform(get("/admin/tags/DeleteTag").param("id", "0"))
               .andExpect(redirectedUrl("/admin/tags/ViewTags"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgSuccess"));
        
        verify(issueService).getTagById(anyInt());
        verify(issueService).deleteTag(any(Tag.class));
    }
    
    @Test
    public void testDeleteTag_entityDoesNotExist () throws Exception {
    
        doThrow(new IllegalArgumentException()).when(issueService).deleteTag(any(Tag.class));
        
        mockMvc.perform(get("/admin/tags/DeleteTag").param("id", "0"))
               .andExpect(redirectedUrl("/admin/tags/ViewTags"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(issueService).getTagById(anyInt());
        verify(issueService).deleteTag(any(Tag.class));
    }
    
    @Test
    public void testDeleteTag_throwsDataIntegrityViolation () throws Exception {
    
        doThrow(new DataIntegrityViolationException("")).when(issueService).deleteTag(any(Tag.class));
        
        mockMvc.perform(get("/admin/tags/DeleteTag").param("id", "0"))
               .andExpect(redirectedUrl("/admin/tags/ViewTags"))
               .andExpect(model().size(0))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attributeExists("msgError"));
        
        verify(issueService).getTagById(anyInt());
        verify(issueService).deleteTag(any(Tag.class));
    }
}
