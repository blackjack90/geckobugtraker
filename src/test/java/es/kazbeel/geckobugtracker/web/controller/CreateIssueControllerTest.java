package es.kazbeel.geckobugtracker.web.controller;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.service.IssueService;
import es.kazbeel.geckobugtracker.service.UserGroupService;
import es.kazbeel.geckobugtracker.web.controller.CreateIssueController;
import es.kazbeel.geckobugtracker.web.secure.CurrentUserDetails;


@RunWith(MockitoJUnitRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class CreateIssueControllerTest {
    
    @Mock
    private IssueService issueService;
    
    @Mock
    private UserGroupService userGroupService;
    
    @InjectMocks
    private CreateIssueController createIssueController;
    
    private MockMvc mockMvc;
    
    private User admin = new User();
    
    
    @Before
    public void setup () {
    
        admin.setLoginName("admin");
        admin.setPassword("admin");
        
        CurrentUserDetails loggedUser = new CurrentUserDetails(admin.getLoginName(),
                                                               admin.getPassword(),
                                                               AuthorityUtils.createAuthorityList("ROLE_ADMIN"),
                                                               admin);
        Authentication auth = new UsernamePasswordAuthenticationToken(loggedUser, null);
        SecurityContextHolder.getContext().setAuthentication(auth);
        
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        
        mockMvc = MockMvcBuilders.standaloneSetup(createIssueController).setViewResolvers(viewResolver).build();
    }
    
    @Test
    public void testCreateIssueGet () throws Exception {
    
        mockMvc.perform(get("/issues/CreateIssue"))
               .andExpect(view().name("/issues/CreateIssue"))
               .andExpect(model().size(10))
               .andExpect(model().attributeExists("environments"))
               .andExpect(model().attributeExists("tags"))
               .andExpect(model().attributeExists("loggedUser"))
               .andExpect(model().attributeExists("types"))
               .andExpect(model().attributeExists("statuses"))
               .andExpect(model().attributeExists("priorities"))
               .andExpect(model().attributeExists("resolutions"))
               .andExpect(model().attributeExists("users"))
               .andExpect(model().attributeExists("categories"))
               .andExpect(model().attributeExists("issuePackage"));
    }
    
    @Test
    public void testCreateIssuePost () throws Exception {
    
        // TODO Improve this test. Do not really know waht to test and/or how.
        
        mockMvc.perform(post("/issues/CreateIssue"))
               .andExpect(redirectedUrl("/issues/null"));
    }
}
