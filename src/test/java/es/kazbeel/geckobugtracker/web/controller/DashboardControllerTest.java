package es.kazbeel.geckobugtracker.web.controller;


import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import es.kazbeel.geckobugtracker.builders.IssueBuilder;
import es.kazbeel.geckobugtracker.model.Issue;
import es.kazbeel.geckobugtracker.model.Resolution;
import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.service.IssueService;
import es.kazbeel.geckobugtracker.support.Page;
import es.kazbeel.geckobugtracker.support.PageRequest;
import es.kazbeel.geckobugtracker.web.controller.DashboardController;
import es.kazbeel.geckobugtracker.web.secure.CurrentUserDetails;


@RunWith(MockitoJUnitRunner.class)
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class})
public class DashboardControllerTest {
    
    @Mock
    private IssueService issueService;
    
    @InjectMocks
    private DashboardController dashboardController;
    
    private MockMvc mockMvc;
    
    private User admin = new User();
    
    
    @Before
    public void setup () {
    
        CurrentUserDetails loggedUser = new CurrentUserDetails("admin",
                                                               "admin",
                                                               AuthorityUtils.createAuthorityList("ROLE_ADMIN"),
                                                               admin);
        Authentication auth = new UsernamePasswordAuthenticationToken(loggedUser, null);
        SecurityContextHolder.getContext().setAuthentication(auth);
        
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        
        mockMvc = MockMvcBuilders.standaloneSetup(dashboardController).setViewResolvers(viewResolver).build();
    }
    
    @Test
    public void testSearch_issueFound () throws Exception {
    
        Issue issue = IssueBuilder.issue().withId(123).build();
        
        when(issueService.getIssueById(anyInt())).thenReturn(issue);
        
        mockMvc.perform(post("/SearchIssue").param("searchIssue", "123"))
               .andExpect(redirectedUrl("/issues/123"));
    }
    
    @Test
    public void testSearch_issueEmptyId () throws Exception {
    
        when(issueService.getIssueById(anyInt())).thenReturn(null);
        
        mockMvc.perform(post("/SearchIssue").param("searchIssue", ""))
               .andExpect(redirectedUrl("/Dashboard"))
               .andExpect(model().size(0))
               .andExpect(model().attributeDoesNotExist("searchIssue"))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attribute("formatError", is("Issue ID is malformed.")));
    }
    
    @Test
    public void testSearch_issueNotFound () throws Exception {
    
        when(issueService.getIssueById(anyInt())).thenReturn(null);
        
        mockMvc.perform(post("/SearchIssue").param("searchIssue", RandomStringUtils.randomNumeric(5)))
               .andExpect(redirectedUrl("/Dashboard"))
               .andExpect(model().size(0))
               .andExpect(model().attributeDoesNotExist("searchIssue"))
               .andExpect(flash().attributeCount(1))
               .andExpect(flash().attribute("notFound", is("Issue ID not found.")));
    }
    
    @Test
    public void testDashboardGet () throws Exception {
    
        Page<Issue> dummyIssuePage = new Page<Issue>();
        
        when(issueService.getIssuesByAssigneeAndResolution(any(User.class),
                                                           any(Resolution.class),
                                                           any(PageRequest.class))).thenReturn(dummyIssuePage);
        when(issueService.getIssuesByReporter(any(User.class), any(PageRequest.class))).thenReturn(dummyIssuePage);
        when(issueService.getIssuesVotedByUser(any(User.class), any(PageRequest.class))).thenReturn(dummyIssuePage);
        when(issueService.getIssuesWatchedByUser(any(User.class), any(PageRequest.class))).thenReturn(dummyIssuePage);
        
        mockMvc.perform(get("/Dashboard"))
               .andExpect(view().name("Dashboard"))
               .andExpect(model().attributeExists("loggedUser"))
               .andExpect(model().attributeExists("issuesAssignedToMe"))
               .andExpect(model().attributeExists("totalAssignedToMe"))
               .andExpect(model().attributeExists("issuesReportedByMe"))
               .andExpect(model().attributeExists("totalReportedByMe"))
               .andExpect(model().attributeExists("issuesVotedByMe"))
               .andExpect(model().attributeExists("totalVoted"))
               .andExpect(model().attributeExists("issuesWatchedByMe"))
               .andExpect(model().attributeExists("totalWatched"));
    }
    
    @Test
    public void testDashboardPostThrowsError () throws Exception {
    
        mockMvc.perform(post("/Dashboard"))
               .andExpect(status().isMethodNotAllowed());
    }
    
    @Test
    public void testRoot () throws Exception {
    
        mockMvc.perform(get("/")).andExpect(redirectedUrl("/Dashboard"));
    }
}
