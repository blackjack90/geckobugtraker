<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="/WEB-INF/functions.tld" prefix="myf" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Gecko - Issue #${issue.id}</title>

    <link rel="stylesheet" href="<c:url value="/resources/uikit/css/uikit.almost-flat.css" />" type="text/css" />
    <script src="<c:url value="/resources/js/jquery.js" />"></script>
    <script src="<c:url value="/resources/uikit/js/uikit.min.js" />"></script>
</head>
<body>

<jsp:include page="../TopNavBar.jsp" />

<div style="margin-left: 10px; margin-top: 10px; margin-bottom: 10px;" class="uk-flex">
	<h3>@ Project - Component @</h3>
	<strong>${issue.summary}</strong>
</div>

<hr>

<div class="uk-container uk-container-center">
    <div class="uk-grid">
        <div class="uk-width-1-1">
	        <div class="uk-panel">
                <a class="uk-button" href="<c:url value="${issue.id}/edit" />">Edit</a>
                <a class="uk-button" href="<c:url value="${issue.id}/delete" />">Delete</a>
	        </div>
        </div>
    </div>
    
    <div class="uk-grid">
        <div class="uk-width-2-3">
            <div class="uk-panel uk-panel-box uk-panel-header">
                <h3 class="uk-panel-title"><i class="uk-icon-gears"></i> Details</h3>
                <table style="width:100%">
                   <tr>
                       <td style="width:50%">Type:</td>
                       <td style="width:50%">${issue.type.name}</td>
                   </tr>
                   <tr>
                       <td style="width:50%">Status:</td>
                       <td style="width:50%">${issue.status.name}</td>
                   </tr>
                   <tr>
                       <td style="width:50%">Priority:</td>
                       <td style="width:50%">${issue.priority.name}</td>
                   </tr>
                   <tr>
                       <td style="width:50%">Resolution:</td>
                       <td style="width:50%">
                           <span data-uk-tooltip="{pos:'right'}" title="${issue.resolution.description}">
                           <c:out value="${issue.resolution.name}" />
                           </span>
                       </td>
                   </tr>
                   <tr>
                       <td style="width:50%; vertical-align:top;">Environment:</td>
                       <td style="width:50%">
                       <c:choose>
                           <c:when test="${not empty issue.environments}">
	                           <c:forEach items="${issue.environments}" var="env">
	                               <div class="uk-badge uk-badge-notification"><c:out value="${env.name}" /></div>
	                           </c:forEach>
                           </c:when>
                           <c:otherwise>
                                  <div class="uk-badge uk-badge-notification uk-badge-warning">None</div>
                           </c:otherwise>
                       </c:choose>
                       </td>
                   </tr>
                      <tr>
                          <td style="width:50%; vertical-align:top;">Tags:</td>
                          <td style="width:50%">
                          <c:choose>
                              <c:when test="${not empty issue.tags}">
	                           <c:forEach items="${issue.tags}" var="tag">
	                               <div class="uk-badge uk-badge-notification"><c:out value="${tag.name}" /></div>
	                           </c:forEach>
                              </c:when>
                              <c:otherwise>
                               <div class="uk-badge uk-badge-notification uk-badge-warning">None</div>
                              </c:otherwise>
                          </c:choose>
                          </td>
                      </tr>
                </table>
            </div>
            
               <div class="uk-panel uk-panel-box uk-panel-header">
                   <h3 class="uk-panel-title"><i class="uk-icon-inbox"></i> Description</h3>
                   <c:out value="${issue.description}" />
               </div>

               <div class="uk-panel uk-panel-box uk-panel-header">
                   <form action="<c:url value="/issues/${issue.id}/UploadAttachment" />" enctype="multipart/form-data" method="post">
                   <h3 class="uk-panel-title"> 
                       <i class="uk-icon-file"></i> Attachments
                       <a class="uk-link-reset" href="" onclick="document.getElementById('upload').click(); return false"><i class="uk-icon-plus uk-float-right"></i></a>
                   </h3>
                   <input id="upload" type="file" name="file" style="visibility: hidden; width: 1px; height: 1px" onchange="submit()">
                   </form>
                   <c:forEach items="${attachments}" var="attachment">
                    <a href="<c:url value="/issues/${issue.id}/DownloadAttachment/${attachment.id}" />">
                    <c:out value="${attachment.fileName} (${attachment.fileSize} bytes)" /> - <fmt:formatDate pattern="dd-MM-yyyy HH:mm" value="${attachment.createdOn}" /><br />
                    </a>
                   </c:forEach>
               </div>

               <div class="uk-panel uk-panel-box uk-panel-header">
                   <c:url value="/issues/${issue.id}/DeleteIssueLink" var="deleteIssueLinkUrl" />
                   <h3 class="uk-panel-title">
                       <i class="uk-icon-external-link"></i> Issue Links</h3>
                   <table>
                   <c:forEach items="${outIssueLinks}" var="issueLink">
                        <tr>
                            <td class="uk-width-2-10">
                                <c:out value="${issueLink.linkType.outward}" />
                            </td>
                            <td class="uk-width-7-10">
                                <c:url value="/issues/${issueLink.destination.id}" var="issueUrl"/>
                                <a class="uk-link-muted" href="${issueUrl}">
                                <c:out value="${issueLink.destination.id} - ${issueLink.destination.summary}" />
                                </a>
                            </td>
                            <td class="uk-width-1-10">
                               <form action="${deleteIssueLinkUrl}" method="post">
                                    <input type="hidden" name="issueLinkId" value="${issueLink.id}" />
                                    <button type="submit"><i class="uk-icon-close"></i></button>
                               </form>
                            </td>
                        </tr>
                   </c:forEach>
                   <c:forEach items="${inIssueLinks}" var="issueLink">
                        <tr>
                            <td class="uk-width-2-10"><c:out value="${issueLink.linkType.inward}" /></td>
                            <td class="uk-width-7-10">
                                <c:url value="/issues/${issueLink.source.id}" var="issueUrl"/>
                                <a class="uk-link-muted" href="${issueUrl}">
                                <c:out value="${issueLink.source.id} - ${issueLink.source.summary}" />
                                </a>
                            </td>
                            <td class="uk-width-1-10">
                               <form action="${deleteIssueLinkUrl}" method="post">
                                    <input type="hidden" name="issueLinkId" value="${issueLink.id}" />
                                    <button type="submit"><i class="uk-icon-close"></i></button>
                               </form>
                            </td>
                        </tr>
                   </c:forEach>
                   </table>
                   <hr>
                   <c:url value="/issues/${issue.id}/CreateIssueLink" var="createLinkUrl" />
                   <form:form class="uk-form" modelAttribute="newIssueLink" action="${createLinkUrl}" method="post">
                        <fieldset data-uk-margin>
                            <form:select path="linkType" items="${allIssueLinkTypes}" itemLabel="outward" itemValue="id" />
                            <form:input type="text" placeholder="Issue #" path="destination"/>
                            <form:input type="hidden" path="source" value="${issue.id}" />
                            <button type="submit" class="uk-button">Add Link</button>
                        </fieldset>
                   </form:form>
               </div>
        </div>
        
        <div class="uk-width-1-3">
            <div class="uk-panel uk-panel-box uk-panel-header">
               <h3 class="uk-panel-title"><i class="uk-icon-user"></i> People</h3>
               
                  <h5>Reporter:</h5>
                  <!-- TODO Make below image and name linkable to user's profile. -->
                  <img class="uk-thumbnail" 
                       width="30" height="30"
                       src="<c:url value="/resources/images/unknown_avatar.jpg" />"
                       alt="loggedUser.shortcut">
                  <c:out value="${issue.reporter.firstName} ${issue.reporter.lastName}" />
               
               <h4>Assignee:</h4>
                  <!-- TODO Make below image and name linkable to user's profile. -->
               <c:choose>
                   <c:when test="${issue.assignee != null}">
                       <c:out value="${issue.assignee.firstName} ${issue.assignee.lastName}" />
                   </c:when>
                   <c:otherwise>
                       <div class="uk-badge uk-badge-notification uk-badge-warning"><c:out value="None yet" /></div>
                   </c:otherwise>
               </c:choose>
               
               <h4>Votes:</h4>
               <div class="uk-badge uk-badge-notification"><c:out value="${numVotes}" /></div>
                  <c:choose>
                      <c:when test="${isVoter}">
                       <spring:url value="/issues/{issueId}/vote?action=unvote" var="issueVoteUrl">
                           <spring:param name="issueId" value="${issue.id}" />
                       </spring:url>
                          <a href="${fn:escapeXml(issueVoteUrl)}">Remove vote for this issue</a>
                      </c:when>
                      <c:otherwise>
                          <spring:url value="/issues/{issueId}/vote?action=vote" var="issueVoteUrl">
                              <spring:param name="issueId" value="${issue.id}" />
                          </spring:url>
                          <a href="${fn:escapeXml(issueVoteUrl)}">Vote for this issue</a>
                      </c:otherwise>
                  </c:choose>

                  <h4>Watchers:</h4>
                  <div class="uk-badge uk-badge-notification"><c:out value="${numWatchers}" /></div>
                  <c:choose>
                      <c:when test="${isWatcher}">
                          <spring:url value="/issues/{issueId}/watch?action=unwatch" var="issueWatchUrl">
                              <spring:param name="issueId" value="${issue.id}" />
                          </spring:url>
                          <a href="${fn:escapeXml(issueWatchUrl)}">Stop watching this issue</a>
                      </c:when>
                      <c:otherwise>
                          <spring:url value="/issues/{issueId}/watch?action=watch" var="issueWatchUrl">
                              <spring:param name="issueId" value="${issue.id}" />
                          </spring:url>
                          <a href="${fn:escapeXml(issueWatchUrl)}">Start watching this issue</a>
                      </c:otherwise>
                  </c:choose>
            </div>
            
            <div class="uk-panel uk-panel-box uk-panel-header">
                   <h3 class="uk-panel-title"><i class="uk-icon-calendar"></i> Dates</h3>
                   
                   <table style="width:100%">
                       <tr>
                           <td style="width:50%">Created:</td>
                           <td style="width:50%">
                               <fmt:formatDate pattern="dd-MM-yyyy" value="${issue.createdOn}" />
                               <br>
                               <small><c:out value="(${myf:getDiff(issue.createdOn)})" /></small>
                           </td>
                       </tr>
                       <tr>
                           <td style="width:50%">Updated:</td>
                           <td style="width:50%">
                               <fmt:formatDate pattern="dd-MM-yyyy" value="${issue.lastUpdateOn}" />
                               <br>
                               <small><c:out value="(${myf:getDiff(issue.lastUpdateOn)})" /></small>
                           </td>
                       </tr>
                   </table>
            </div>
        </div>
    </div>
    
    <p />
    
    <ul class="uk-tab uk-tab-flip" data-uk-tab="{connect:'#tabs-content'}">
          <li class="uk-active"><a href="#comments"><i class="uk-icon-comments"></i> Comments</a></li>
          <li class="uk-active"><a href="#history"><i class="uk-icon-paw"></i> History</a></li>
    </ul>
    <!-- 
    <ul id="tabs-content" class="uk-switcher uk-margin">
        <li>
               <div class="uk-panel uk-panel-box">
                   <c:if test="${not empty issue.comments}">
                       <c:forEach items="${issue.comments}" var="comment" varStatus="status">
                           <article class="uk-comment">
                               <header class="uk-comment-header">
                                   <img class="uk-comment-avatar"
                                        src="<c:url value="/resources/images/unknown_avatar.jpg" />"
                                        alt="User shortcut"
                                        width="50" height="50">
                                   <h4 class="uk-comment-title">${comment.author.shortcut}</h4>
                                   <div class="uk-comment-meta">${comment.createdOn}</div>
                               </header>
                               <div class="uk-comment-body"><c:out value="${comment.body}" /></div>
                               <p>
                           </article>
                   </c:forEach>
                   </c:if>
                   
                   <hr>
                   
                   <form class="uk-form uk-form-stacked" method="post">
                       <div class="uk-form-row">
                           <div class="uk-form-controls">
                               <textarea id="comment-body" name="body" cols="50" rows="5" placeholder="Comment..." ></textarea>
                           </div>
                       </div>
                       <p />
                       <button class="uk-button uk-button-primary uk-button-large">Add comment</button>
                   </form>
               </div>
        </li>
    </ul>
    -->
</div>

</body>
</html>