<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Gecko - Create New Issue</title>

    <link rel="stylesheet" href="<c:url value="/resources/uikit/css/uikit.almost-flat.css" />" type="text/css" />
    <script src="<c:url value="/resources/js/jquery.js" />"></script>
    <script src="<c:url value="/resources/uikit/js/uikit.min.js" />"></script>
    <script type="text/javascript">
    function goBack() {
    	window.history.back();
    }
    function onChangeProject() {
    	var project = document.getElementById("projects");
    	var selectedValue = project.options[project.selectedIndex].value;
    	
    	alert(selectedValue);
    }
    </script>
</head>
<body>

    <jsp:include page="../TopNavBar.jsp" />

<%-- 
<c:url var="selectCurrentProjectUrl" value="/issues/SelectCurrentProject" />
<form:form id="select-current-project" class="uk-form uk-form-horizontal" modelAttribute="issuePackage" action="${selectCurrentProjectUrl}" method="post">
    <div class="uk-form-row">
        <label class="uk-form-label">Project:</label>
        <div class="uk-form-controls uk-form-controls-text">
            <form:select path="project">
                <form:options items="${allProjects}" itemValue="id" itemLabel="name" />
            </form:select>
            <button type="submit" class="uk-button">Select</button>
            <hr />
        </div>
    </div>
</form:form>
 --%>
<c:url var="createIssueUrl" value="/issues/CreateIssue" />
<form:form id="create-issue" class="uk-form uk-form-horizontal" modelAttribute="issuePackage" action="${createIssueUrl}" method="post">
    <div class="uk-form-row">
        <label class="uk-form-label" for="component">Component:</label>
        <div class="uk-form-controls uk-form-controls-text">
            <form:select path="issue.component">
                <form:options items="${components}" itemLabel="name" itemValue="id" />
            </form:select>
        </div>
    </div>

    <div class="uk-form-row">
        <label class="uk-form-label" for="form-type">Type:</label>
        <div class="uk-form-controls uk-form-controls-text">
            <form:select path="issue.type">
                <form:options items="${types}" itemLabel="name" itemValue="id" />
            </form:select>
        </div>
    </div>

    <div class="uk-form-row">
        <label class="uk-form-label">Summary:</label>
        <div class="uk-form-controls uk-form-controls-text">
            <form:input path="issue.summary" type="text" class="uk-form-width-large" />
        </div>
    </div>

    <div class="uk-form-row">
        <label class="uk-form-label" for="form-priority">Priority:</label>
        <div class="uk-form-controls uk-form-controls-text">
            <form:select path="issue.priority">
                <form:options items="${priorities}" itemLabel="name" itemValue="id" />
            </form:select>
        </div>
    </div>

    <div class="uk-form-row">
        <label class="uk-form-label" for="form-assignee">Assignee:</label>
        <div class="uk-form-controls uk-form-controls-text">
            <form:select path="issue.assignee">
                <form:options items="${users}" itemLabel="loginName" itemValue="id" />
            </form:select>
        </div>
    </div>

    <div class="uk-form-row">
        <label class="uk-form-label" for="form-environment">Environments:</label>
        <div class="uk-form-controls uk-form-controls-text">
            <form:select path="issue.environments" multiple="true">
                <form:options items="${environments}" itemLabel="name" itemValue="id" />
            </form:select>
            <br />
            <form:input type="text" path="environmentStr" />
        </div>
    </div>

    <div class="uk-form-row">
        <label class="uk-form-label" for="description">Description:</label>
        <div id="description" class="uk-form-controls uk-form-controls-text">
            <form:textarea path="issue.description" rows="10" class="uk-form-width-large" />
        </div>
    </div>

    <div class="uk-form-row">
        <label class="uk-form-label" for="form-tag">Tags:</label>
        <div class="uk-form-controls uk-form-controls-text">
            <form:select path="issue.tags" multiple="true">
                <form:options items="${tags}" itemLabel="name" itemValue="id" />
            </form:select>
            <br />
            <form:input type="text" path="tagStr" />
        </div>
    </div>

    <p></p>
    
    <button type="submit" class="uk-button">Create</button>
    <button type="button" class="uk-button" onclick="goBack()">Cancel</button>
</form:form>

</body>
</html>