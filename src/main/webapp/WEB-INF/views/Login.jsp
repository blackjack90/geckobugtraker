<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Gecko - Login</title>

    <link rel="stylesheet" href="<c:url value="/resources/uikit/css/uikit.almost-flat.css" />" type="text/css" />
    <script src="<c:url value="/resources/js/jquery.js" />"></script>
    <script src="<c:url value="/resources/uikit/js/uikit.min.js" />"></script>
</head>

<body class="uk-height-1-1">
    <div class="uk-vertical-align uk-text-center uk-height-1-1">
	    <div class="uk-vertical-align-middle" style="width: 400px;">
	        <form class="uk-form uk-panel uk-panel-box" name="login" action="<c:url value="/login_check" />" method="POST">
                <div class="uk-form-row">
                    <input class="uk-width-1-1 uk-form-large" type="text" name="username" placeholder="Username" />
                </div>
                <div class="uk-form-row">
                    <input class="uk-width-1-1 uk-form-large" type="password" name="password" placeholder="Password" />
                </div>
                <div class="uk-form-row">
                    <button class="uk-width-1-1 uk-button uk-button-primary uk-button-large" type="submit">Login</button>
                </div>
                <div class="uk-form-row uk-text-small">
                    <label class="uk-float-left"><input type="checkbox" name="remember_me"> Remember Me</label>
                    <a class="uk-float-right uk-link uk-link-muted" href="#">Forgot Password?</a>
               </div>
	        </form>
	    </div>
    </div>
</body>

</html>
