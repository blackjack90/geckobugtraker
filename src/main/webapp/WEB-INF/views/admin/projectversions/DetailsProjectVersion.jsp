<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Project Version Details</title>
    
    <link rel="stylesheet" href="<c:url value="/resources/uikit/css/uikit.almost-flat.css" />" type="text/css" />
    <script src="<c:url value="/resources/js/jquery.js" />"></script>
    <script src="<c:url value="/resources/uikit/js/uikit.min.js" />"></script>
    <link rel="stylesheet" href="<c:url value="/resources/css/docs.css" />" type="text/css" />
</head>

<body>

<jsp:include page="../../TopNavBar.jsp" />


<div class="tm-middle">
	<div class="uk-container uk-container-center">
	
	    <div class="uk-grid uk-grid-divider" data-uk-grid-margin data-uk-grid-match>
            <div class="tm-sidebar uk-width-medium-1-4">
                <ul class="tm-nav uk-nav uk-nav-side" data-uk-nav>
                    <li><a href="<c:url value="/admin/users/ViewUsers" />">Users</a></li>
                    <li><a href="<c:url value="/admin/GroupsList" />">Groups</a></li>
                    <li><a href="<c:url value="/admin/IssueTypeList" />">Issue Types</a></li>
                    <li><a href="<c:url value="/admin/PriorityList" />">Priorities</a></li>
                    <li class="uk-active"><a href="<c:url value="/admin/resolutions/ViewResolutions" />">Resolutions</a></li>
                </ul>
            </div>
	        
	        <div class="tm-main uk-width-medium-3-4">
                <div class="uk-grid uk-grid-divider" data-uk-grid-margin data-uk-grid-match>
                    <div class="uk-width-1-1">

                    <div class="uk-panel">
                        <spring:url value="/admin/projectversions/EditProjectVersion" var="editProjectVersionUrl" htmlEscape="true">
                            <spring:param name="id" value="${projectVersion.id}" />
                        </spring:url>
                        <a class="uk-button" href="${editProjectVersionUrl}">Edit</a>

                        <spring:url value="/admin/projectversions/DeleteProjectVersion" var="deleteProjectVersionUrl" htmlEscape="true">
                            <spring:param name="id" value="${projectVersion.id}" />
                        </spring:url>
                        <a class="uk-button" href="${deleteProjectVersionUrl}">Delete</a>
                    </div>
                        
                    <div class="uk-panel">
                        <spring:url value="/admin/projects/DetailsProject" var="projectUrl" htmlEscape="true">
                            <spring:param name="id" value="${projectVersion.project.id}" />
                        </spring:url>
                        Project: <a href="${projectUrl}"><c:out value="${projectVersion.project.name}" /></a><p />

                        Name: <c:out value="${projectVersion.name}" /><p />
                        Description: <c:out value="${projectVersion.description}" /><p />
                        Released: <input type="checkbox" ${projectVersion.released == 'true' ? 'checked' : '' } disabled="disabled" />
                        <c:if test="${projectVersion.released == true}">
                            On Date: <fmt:formatDate value="${projectVersion.releasedDate}" pattern="dd-MM-yyyy HH:mm" />
                        </c:if>
                        <p />
                        Archived: <input type="checkbox" ${projectVersion.archived == 'true' ? 'checked' : '' } disabled="disabled" /><p />
		            </div>
                    </div>
                </div>
	        </div>
	        
	    </div>
	    
	</div>
</div>

</body>
</html>