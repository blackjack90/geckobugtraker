<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Priority List</title>
    
    <link rel="stylesheet" href="<c:url value="/resources/uikit/css/uikit.almost-flat.css" />" type="text/css" />
    <script src="<c:url value="/resources/js/jquery.js" />"></script>
    <script src="<c:url value="/resources/uikit/js/uikit.min.js" />"></script>
    <link rel="stylesheet" href="<c:url value="/resources/css/docs.css" />" type="text/css" />
</head>

<body>

<jsp:include page="../../TopNavBar.jsp" />


<div class="tm-middle">
	<div class="uk-container uk-container-center">
	
	    <div class="uk-grid uk-grid-divider" data-uk-grid-margin data-uk-grid-match>
	        <div class="tm-sidebar uk-width-medium-1-4">
	            <ul class="tm-nav uk-nav uk-nav-side" data-uk-nav>
                    <li class="uk-active"><a href="<c:url value="/admin/users/ViewUsers" />">Users</a></li>
                    <li><a href="<c:url value="/admin/GroupList" />">Groups</a></li>
                    <li><a href="<c:url value="/admin/IssueTypeList" />">Issue Types</a></li>
                    <li><a href="<c:url value="/admin/IssueLinkTypeList" />">Issue Link Types</a></li>
                    <li><a href="<c:url value="/admin/PriorityList" />">Priorities</a></li>
                    <li><a href="<c:url value="/admin/ResolutionList" />">Resolutions</a></li>
                    <li><a href="<c:url value="/admin/StatusList" />">Statuses</a></li>
                    <li><a href="<c:url value="/admin/EnvironmentList" />">Environments</a></li>
                    <li><a href="<c:url value="/admin/TagList" />">Tags</a></li>
	            </ul>
	        </div>
	        
	        <div class="tm-main uk-width-medium-3-4">
                <div class="uk-grid uk-grid-divider" data-uk-grid-margin data-uk-grid-match>
                    <div class="uk-panel uk-width-1-1">
                        <c:if test="${not empty msgSuccess}">
                            <div class="uk-alert uk-alert-success">
                               <a href="" class="uk-alert-close uk-close"></a>
                               <p>${msgSuccess}</p>
                            </div>
                        </c:if>
                        <c:if test="${not empty msgError}">
                            <div class="uk-alert uk-alert-danger">
                               <a href="" class="uk-alert-close uk-close"></a>
                               <p>${msgError}</p>
                            </div>
                        </c:if>

                        <div class="uk-panel">
                            <a class="uk-button uk-align-right" href="<c:url value="/admin/priorities/CreatePriority" />"><i class="uk-icon-plus"></i> Add Priority</a>
                        </div>
                        
                        <div class="uk-panel">
				            <ul class="uk-pagination uk-pagination-right">
				                <c:choose>
				                    <c:when test="${isFirst == false && not empty param.page}">
				                        <spring:url value="/admin/priorities/ViewPriorities" var="prevPage" htmlEscape="true">
				                           <spring:param name="page" value="${param.page - 1}" />
				                        </spring:url>
				                        <li><a href="${prevPage}"><i class="uk-icon-angle-left"></i></a></li>
				                    </c:when>
				                    <c:otherwise>
				                        <li class="uk-disabled"><span><i class="uk-icon-angle-left"></i></span></li>
				                    </c:otherwise>
				                </c:choose>
				                
				                <c:choose>
				                    <c:when test="${empty param.page}">
				                        <li>0 of ${totalPages}</li>
				                    </c:when>
				                    <c:otherwise>
				                        <li>${param.page} of ${totalPages}</li>
				                    </c:otherwise>
				                </c:choose>
				                
				                <c:choose>
				                    <c:when test="${isLast == false}">
				                        <spring:url value="/admin/priorities/ViewPriorities" var="nextPage" htmlEscape="true">
				                           <spring:param name="page" value="${param.page + 1}" />
				                        </spring:url>
				                        <li><a href="${nextPage}"><i class="uk-icon-angle-right"></i></a></li>
				                    </c:when>
				                    <c:otherwise>
				                        <li class="uk-disabled"><span><i class="uk-icon-angle-right"></i></span></li>
				                    </c:otherwise>
				                </c:choose>
				            </ul>
				            
	                        <table class="uk-table uk-table-hover uk-table-condensed">
	                            <thead>
	                                <tr>
	                                    <th>Name</th>
	                                    <th>Description</th>
	                                    <th>Sort Value</th>
	                                    <th>Enabled</th>
	                                    <th>Order</th>
	                                    <th>Options</th>
	                                </tr>
	                            </thead>
	                            <tbody>
	                                <c:forEach items="${priorities}" var="priority">
                                       <spring:url value="/admin/priorities/EditPriority" var="editPriorityUrl" htmlEscape="true">
                                           <spring:param name="id" value="${priority.id}" />
                                       </spring:url>
                                       <spring:url value="/admin/priorities/DeletePriority" var="delPriorityUrl" htmlEscape="true">
                                           <spring:param name="id" value="${priority.id}" />
                                       </spring:url>
                                       <spring:url value="/admin/priorities/SortPriority" var="moveUpPriorityUrl" htmlEscape="true">
                                           <spring:param name="id" value="${priority.id}" />
                                           <spring:param name="move" value="up" />
                                       </spring:url>
                                       <spring:url value="/admin/priorities/SortPriority" var="moveDownPriorityUrl" htmlEscape="true">
                                           <spring:param name="id" value="${priority.id}" />
                                           <spring:param name="move" value="down" />
                                       </spring:url>
		                                <tr>
		                                    <td><a href="${editPriorityUrl}">${priority.name}</a></td>
		                                    <td>${priority.description}</td>
		                                    <td>${priority.sortValue}</td>
		                                    <td>${priority.enabled}</td>
		                                    <td>
		                                      <a href="${moveUpPriorityUrl}">Up</a>
		                                      <a href="${moveDownPriorityUrl}">Down</a>
		                                    </td>
		                                    <td><a href="${delPriorityUrl}">Delete</a></td>
		                                </tr>
	                                </c:forEach>
	                            </tbody>
	                        </table>
                        </div>
                    </div>
                </div>

	        </div>
	        
	    </div>
	    
	</div>
</div>

</body>
</html>