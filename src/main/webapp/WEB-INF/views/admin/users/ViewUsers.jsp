<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>User Profile</title>
    
    <link rel="stylesheet" href="<c:url value="/resources/uikit/css/uikit.almost-flat.css" />" type="text/css" />
    <link rel="stylesheet" href="<c:url value="/resources/css/docs.css" />" type="text/css" />
    <script src="<c:url value="/resources/js/jquery.js" />"></script>
    <script src="<c:url value="/resources/uikit/js/uikit.min.js" />"></script>
</head>

<body>

<jsp:include page="../../TopNavBar.jsp" />


<div class="tm-middle">
	<div class="uk-container uk-container-center">
	
	    <div class="uk-grid uk-grid-divider" data-uk-grid-margin data-uk-grid-match>
	        <div class="tm-sidebar uk-width-medium-1-4">
	            <ul class="tm-nav uk-nav uk-nav-side" data-uk-nav>
                    <li class="uk-active"><a href="<c:url value="/admin/users/ViewUsers" />">Users</a></li>
                    <li><a href="<c:url value="/admin/groups/ViewGroups" />">Groups</a></li>
                    <li><a href="<c:url value="/admin/issuelinktypes/ViewIssueLinkTypes" />">Issue Link Types</a></li>
                    <li><a href="<c:url value="/admin/issuetypes/ViewIssueTypes" />">Issue Types</a></li>
                    <li><a href="<c:url value="/admin/priorities/ViewPriorities" />">Priorities</a></li>
                    <li><a href="<c:url value="/admin/resolutions/ViewResolutions" />">Resolutions</a></li>
                    <li><a href="<c:url value="/admin/statuses/ViewStatuses" />">Statuses</a></li>
                    <li><a href="<c:url value="/admin/environments/ViewEnvironments" />">Environments</a></li>
                    <li><a href="<c:url value="/admin/tags/ViewTags" />">Tags</a></li>
                    <li><a href="<c:url value="/admin/sections/ViewSections" />">Sections</a></li>
                    <li><a href="<c:url value="/admin/projects/ViewProjects" />">Projects</a></li>
	            </ul>
	        </div>
	        
	        <div class="tm-main uk-width-medium-3-4">
                <div class="uk-grid uk-grid-divider" data-uk-grid-margin data-uk-grid-match>
                    <div class="uk-panel uk-width-1-1">
                        <c:if test="${not empty msgSuccess}">
                            <div class="uk-alert uk-alert-success">
                               <a href="" class="uk-alert-close uk-close"></a>
                               <p>${msgSuccess}</p>
                            </div>
                        </c:if>
                        <c:if test="${not empty msgError}">
                            <div class="uk-alert uk-alert-danger">
                               <a href="" class="uk-alert-close uk-close"></a>
                               <p>${msgError}</p>
                            </div>
                        </c:if>

                        <div class="uk-panel">
                            <a class="uk-button uk-align-right" href="<c:url value="/admin/users/CreateUser" />"><i class="uk-icon-plus"></i> Add User</a>
                        </div>
                        
                        <p>
                        TODO - Add filters here
                        </p>
                        
                        <div class="uk-panel">
				            <ul class="uk-pagination uk-pagination-right">
				                <c:choose>
				                    <c:when test="${isFirst == false && not empty param.page}">
				                        <spring:url value="/admin/users/ViewUsers" var="prevPage" htmlEscape="true">
				                           <spring:param name="page" value="${param.page - 1}" />
				                        </spring:url>
				                        <li><a href="${prevPage}"><i class="uk-icon-angle-left"></i></a></li>
				                    </c:when>
				                    <c:otherwise>
				                        <li class="uk-disabled"><span><i class="uk-icon-angle-left"></i></span></li>
				                    </c:otherwise>
				                </c:choose>
				                
				                <c:choose>
				                    <c:when test="${empty param.page}">
				                        <li>0 of ${totalPages}</li>
				                    </c:when>
				                    <c:otherwise>
				                        <li>${param.page} of ${totalPages}</li>
				                    </c:otherwise>
				                </c:choose>
				                
				                <c:choose>
				                    <c:when test="${isLast == false}">
				                        <spring:url value="/admin/users/ViewUsers" var="nextPage" htmlEscape="true">
				                           <spring:param name="page" value="${param.page + 1}" />
				                        </spring:url>
				                        <li><a href="${nextPage}"><i class="uk-icon-angle-right"></i></a></li>
				                    </c:when>
				                    <c:otherwise>
				                        <li class="uk-disabled"><span><i class="uk-icon-angle-right"></i></span></li>
				                    </c:otherwise>
				                </c:choose>
				            </ul>
				            
	                        <table class="uk-table uk-table-hover uk-table-condensed">
	                            <thead>
	                                <tr>
	                                    <th>Shortcut</th>
	                                    <th>Full Name</th>
	                                    <th>Groups</th>
	                                    <th>Enabled</th>
	                                    <th>Operations</th>
	                                </tr>
	                            </thead>
	                            <tbody>
	                                <c:forEach items="${users}" var="user">
                                       <spring:url value="/admin/users/EditUser" var="editUserUrl" htmlEscape="true">
                                           <spring:param name="id" value="${user.id}" />
                                       </spring:url>
                                       <spring:url value="/admin/users/DeleteUser" var="deleteUserUrl" htmlEscape="true">
                                           <spring:param name="id" value="${user.id}" />
                                       </spring:url>
		                                <tr>
		                                    <td><a href="${editUserUrl}">${user.shortcut}</a></td>
		                                    <td>${user.firstName} ${user.lastName}</td>
		                                    <td>TODO</td>
		                                    <td>${user.enabled}</td>
		                                    <td><a href="${deleteUserUrl}">Delete</a></td>
		                                </tr>
	                                </c:forEach>
	                            </tbody>
	                        </table>
                        </div>
                    </div>
                </div>

	        </div>
	        
	    </div>
	    
	</div>
</div>

</body>
</html>