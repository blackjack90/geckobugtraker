<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<spring:url value="/SearchIssue" var="searchIssue" />

<nav class="uk-navbar uk-navbar-attached">
    <a class="uk-navbar-brand" href="<c:url value="/Dashboard" />"><i class="uk-icon-home" ></i></a>
    
    <ul class="uk-navbar-nav">
        <li class="uk-parent" data-uk-dropdown>
            <a>Issues</a>
            <div class="uk-dropdown uk-dropdown-navbar">
                <ul class="uk-nav uk-nav-dropdown">
                    <li><a href="<c:url value="/issues/AllIssues" />">All</a></li>
                    <li><a href="<c:url value="/issues/AssignedToMe" />">Assigned To Me</a></li>
                    <li><a href="<c:url value="/issues/ReportedByMe" />">Reported By Me</a></li>
                    <li><a href="<c:url value="/issues/VotedByMe" />">Voted By Me</a></li>
                    <li><a href="<c:url value="/issues/WatchedByMe" />">Watched By Me</a></li>
                </ul>
            </div>
        </li>
    </ul>

    <ul class="uk-navbar-nav">
        <li class="uk-parent" data-uk-dropdown>
            <a>Projects</a>
            <div class="uk-dropdown uk-dropdown-navbar">
                <ul class="uk-nav uk-nav-dropdown">
                <c:forEach items="${sections}" var="section">
                    <li class="uk-parent" data-uk-dropdown>
                        <a><c:out value="${section.name}" /></a>
                        <div class="uk-dropdown">
                            <ul class="uk-nav uk-nav-dropdown">
                                <li></li>
                            </ul>
                        </div>
                    </li>
                </c:forEach>
                </ul>
            </div>
        </li>
    </ul>
    
	<div class="uk-navbar-content">
	    <a class="uk-button uk-button-primary" href="<c:url value="/issues/CreateIssue"/>">Create</a>
	</div>
    
    <div style="margin-right: 10px;" class="uk-navbar-flip">
	    <div style="margin-right: 40px;" class="uk-navbar-content">
	        <form class="uk-form uk-margin-remove uk-display-inline-block" action="${searchIssue}" method="post">
	            <div class="uk-form-icon">
	                <input class="uk-form-width-small" type="text" placeholder="Issue #" name="searchIssue" />
	                <i class="uk-icon-search"></i>
	            </div>
	        </form>
	    </div>

        <ul class="uk-navbar-nav">
            <li class="uk-parent" data-uk-dropdown>
                <span data-uk-tooltip="{pos:'left'}" title="${loggedUser.loginName}">
                <img class="uk-thumbnail" 
                   width="42" height="42"
                    src="<c:url value="/resources/images/unknown_avatar.jpg"/>">
                </span>
                
                <div class="uk-dropdown uk-dropdown-navbar">
                    <ul class="uk-nav uk-nav-navbar">
                        <li><a href="<c:url value="/profile/ViewProfile" />">Profile</a></li>
                        <li class="uk-nav-header">Direct Links</li>
                        <li><a href="<c:url value="/Dashboard" />">My Dashboard</a></li>
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <li><a href="<c:url value="/admin/users/ViewUsers" />">Administration</a></li>
                        </sec:authorize>
                        <li class="uk-nav-divider"></li>
                        <li><a href="<c:url value="/logout" />">Log Out</a></li>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</nav>
