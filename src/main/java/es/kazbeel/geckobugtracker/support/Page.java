package es.kazbeel.geckobugtracker.support;


import java.util.ArrayList;
import java.util.List;


public class Page<T> {
    
    private int page;
    
    private int pageSize;
    
    private int totalElements;
    
    private List<T> content = new ArrayList<T>();
    
    
    public Page () {
    
    }
    
    public Page (List<T> content) {
    
        this.content = content;
    }
    
    public void setPageNumber (int number) {
    
        this.page = number;
    }
    
    public int getPageNumber () {
    
        return this.page;
    }
    
    public void setContent (List<T> content) {
    
        this.content = content;
    }
    
    public List<T> getContent () {
    
        return this.content;
    }
    
    public void setMaxPageSize (int size) {
    
        this.pageSize = size;
    }
    
    public int getMaxPageSize () {
    
        return this.pageSize;
    }
    
    public int getPageSize () {
    
        return this.content.size();
    }
    
    public void setTotalElements (int totalElements) {
    
        this.totalElements = totalElements;
    }
    
    public long getTotalElements () {
    
        return this.totalElements;
    }
    
    public int getTotalPages () {
    
        int value = this.totalElements / getMaxPageSize();
        
        return value;
    }
    
    public boolean isFirst () {
    
        return (page == 0);
    }
    
    public boolean isLast () {
    
        return (page == getTotalPages());
    }

    @Override
    public int hashCode () {
    
        final int prime = 31;
        int result = 1;
        result = prime * result + page;
        result = prime * result + pageSize;
        result = prime * result + totalElements;
        return result;
    }

    @Override
    public boolean equals (Object obj) {
    
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Page))
            return false;
        Page<?> other = (Page<?>) obj;
        if (page != other.page)
            return false;
        if (pageSize != other.pageSize)
            return false;
        if (totalElements != other.totalElements)
            return false;
        return true;
    }
}
