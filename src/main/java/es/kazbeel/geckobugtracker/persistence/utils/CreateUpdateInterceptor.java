package es.kazbeel.geckobugtracker.persistence.utils;


import java.io.Serializable;
import java.util.Date;

import org.apache.log4j.Logger;
import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;


public class CreateUpdateInterceptor extends EmptyInterceptor {
    
    private static final long   serialVersionUID = 1L;
    
    private static final String CREATED_DB_FIELD = "createdOn";
    private static final String UPDATED_DB_FIELD = "lastUpdateOn";
    
    private static final Logger log              = Logger.getLogger(CreateUpdateInterceptor.class);
    
    
    @Override
    public boolean onFlushDirty (Object entity, Serializable id, Object[] currentState, Object[] previousState, String[] propertyNames, Type[] types) {
        for (int i = 0; i < propertyNames.length; i++) {
            if (UPDATED_DB_FIELD.equals(propertyNames[i])) {
                currentState[i] = new Date();
                log.debug("Set date to " + propertyNames[i] + " column.");
            }
        }
        
        return true;
    }
    
    @Override
    public boolean onSave (Object entity, Serializable id, Object[] state, String[] propertyNames, Type[] types) {
        for (int i = 0; i < propertyNames.length; i++) {
            if ((CREATED_DB_FIELD.equals(propertyNames[i])) || (UPDATED_DB_FIELD.equals(propertyNames[i]))) {
                state[i] = new Date();
                log.debug("Set date to " + propertyNames[i] + " column.");
            }
        }
        
        return true;
    }
    
}
