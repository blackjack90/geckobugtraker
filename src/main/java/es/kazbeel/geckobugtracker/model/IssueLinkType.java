package es.kazbeel.geckobugtracker.model;


import java.io.Serializable;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class IssueLinkType implements Serializable {
    
    private static final long serialVersionUID = 1L;
    
    private Integer id;
    
    @NotNull
    @Size(min = 1, max = 255)
    private String name;
    
    @NotNull
    @Size(min = 1, max = 255)
    private String inward;
    
    @NotNull
    @Size(min = 1, max = 255)
    private String outward;
    
    
    public IssueLinkType () {
    
    }
    
    public Integer getId () {
    
        return id;
    }
    
    public void setId (Integer id) {
    
        this.id = id;
    }
    
    public String getName () {
    
        return name;
    }
    
    public void setName (String name) {
    
        this.name = name;
    }
    
    public String getInward () {
    
        return inward;
    }
    
    public void setInward (String inward) {
    
        this.inward = inward;
    }
    
    public String getOutward () {
    
        return outward;
    }
    
    public void setOutward (String outward) {
    
        this.outward = outward;
    }
    
    @Override
    public int hashCode () {
    
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }
    
    @Override
    public boolean equals (Object obj) {
    
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        IssueLinkType other = (IssueLinkType) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }
}
