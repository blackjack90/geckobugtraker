package es.kazbeel.geckobugtracker.model;


import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class Environment implements Serializable {
    
    private static final long serialVersionUID = -1;
    
    private Integer id;
    
    @NotNull
    @Size(min = 1, max = 255)
    private String name;
    
    @NotNull
    private boolean enabled = true;
    
    private Set<Issue> issues = new HashSet<Issue>(0);
    
    
    public Environment () {
    
    }
    
    public Integer getId () {
    
        return id;
    }
    
    public void setId (Integer id) {
    
        this.id = id;
    }
    
    public String getName () {
    
        return name;
    }
    
    public void setName (String name) {
    
        this.name = name;
    }
    
    public boolean isEnabled () {
    
        return enabled;
    }
    
    public void setEnabled (boolean enabled) {
    
        this.enabled = enabled;
    }
    
    public Set<Issue> getIssues () {
    
        return issues;
    }
    
    public void setIssues (Set<Issue> issues) {
    
        this.issues = issues;
    }
    
    @Override
    public int hashCode () {
    
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }
    
    @Override
    public boolean equals (Object obj) {
    
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Environment other = (Environment) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }
}
