package es.kazbeel.geckobugtracker.model.validator;


import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import es.kazbeel.geckobugtracker.model.IssueLink;


public class IssueLinkValidator implements Validator {
    
    @Override
    public boolean supports (Class<?> clazz) {
    
        return IssueLink.class.equals(clazz);
    }
    
    @Override
    public void validate (Object object, Errors errors) {
    
        IssueLink issueLink = (IssueLink) object;
        
        if (issueLink.getSource() == null) {
            errors.rejectValue("source", "may not be null");
        }
        
        if (issueLink.getDestination() == null) {
            errors.rejectValue("destination", "may not be null");
        }
        
        if (issueLink.getLinkType() == null) {
            errors.rejectValue("linkType", "may not be null");
        }
        
        if ((issueLink.getSource() != null) &&
            (issueLink.getDestination() != null) &&
            (issueLink.getSource().equals(issueLink.getDestination()))) {
            errors.rejectValue("destination", "cannot be linked to itself");
        }
    }
}
