package es.kazbeel.geckobugtracker.model;


import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class LoginInfo implements Serializable {
    
    private static final long serialVersionUID = -1;
    
    private Integer id;
    
    @NotNull
    @Size(min = 1, max = 255)
    private String username;
    
    @NotNull
    private int failAttempts;
    
    @NotNull
    private boolean locked;
    
    private Date lastUpdateOn;
    
    
    public LoginInfo () {
    
    }
    
    public LoginInfo (String username) {
    
        this.username = username;
    }
    
    public Integer getId () {
    
        return id;
    }
    
    public void setId (Integer id) {
    
        this.id = id;
    }
    
    public String getUsername () {
    
        return username;
    }
    
    public void setUsername (String username) {
    
        this.username = username;
    }
    
    public int getFailAttempts () {
    
        return failAttempts;
    }
    
    public void setFailAttempts (int failAttempts) {
    
        this.failAttempts = failAttempts;
    }
    
    public Boolean isLocked () {
    
        return locked;
    }
    
    public void setLocked (boolean locked) {
    
        this.locked = locked;
    }
    
    public Date getLastUpdateOn () {
    
        return lastUpdateOn;
    }
    
    public void setLastUpdateOn (Date lastUpdateOn) {
    
        this.lastUpdateOn = lastUpdateOn;
    }
    
    @Override
    public int hashCode () {
    
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        result = prime * result + ((username == null) ? 0 : username.hashCode());
        return result;
    }
    
    @Override
    public boolean equals (Object obj) {
    
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        LoginInfo other = (LoginInfo) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        if (username == null) {
            if (other.username != null)
                return false;
        } else if (!username.equals(other.username))
            return false;
        return true;
    }
}
