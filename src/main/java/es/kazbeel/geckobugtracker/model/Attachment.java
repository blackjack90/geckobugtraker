package es.kazbeel.geckobugtracker.model;


import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class Attachment implements Serializable {
    
    private static final long serialVersionUID = -1;
    
    private Integer id;
    
    @NotNull
    private Issue issue;
    
    @NotNull
    private User author;
    
    @NotNull
    @Size(min = 1, max = 255)
    private String fileName;
    
    @NotNull
    @Min(value = 1)
    private Long fileSize;
    
    @NotNull
    @Size(min = 1, max = 255)
    private String fileMimeType;
    
    private Date createdOn;
    
    
    public Attachment () {
    
    }
    
    public Integer getId () {
    
        return id;
    }
    
    public void setId (Integer id) {
    
        this.id = id;
    }
    
    public Issue getIssue () {
    
        return issue;
    }
    
    public void setIssue (Issue issue) {
    
        this.issue = issue;
    }
    
    public User getAuthor () {
    
        return author;
    }
    
    public void setAuthor (User author) {
    
        this.author = author;
    }
    
    public String getFileName () {
    
        return fileName;
    }
    
    public void setFileName (String fileName) {
    
        this.fileName = fileName;
    }
    
    public Long getFileSize () {
    
        return fileSize;
    }
    
    public void setFileSize (Long fileSize) {
    
        this.fileSize = fileSize;
    }
    
    public String getFileMimeType () {
    
        return fileMimeType;
    }
    
    public void setFileMimeType (String fileMimeType) {
    
        this.fileMimeType = fileMimeType;
    }
    
    public Date getCreatedOn () {
    
        return createdOn;
    }
    
    public void setCreatedOn (Date createdOn) {
    
        this.createdOn = createdOn;
    }
    
    @Override
    public int hashCode () {
    
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }
    
    @Override
    public boolean equals (Object obj) {
    
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Attachment other = (Attachment) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }
}
