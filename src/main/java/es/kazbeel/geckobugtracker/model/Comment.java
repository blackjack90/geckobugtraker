package es.kazbeel.geckobugtracker.model;


import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


//@Entity
//@Table(name = "COMMENTS")
public class Comment implements Serializable {
    
    private static final long serialVersionUID = -1;
    
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    @Column(name = "ID", unique = true, length = 11)
    private Integer id;
    
//    @ManyToOne
//    @JoinColumn(name = "PARENT_ID")
    private Comment parent;
    
//    @ManyToOne(optional = false)
    @NotNull
    private Issue issue;
    
//    @ManyToOne(optional = false)
    @NotNull
    private User author;
    
//    @Column(name = "BODY", nullable = false, length = 1000)
    @NotNull
    @Size(min = 1, max = 100000)
    private String body;
    
//    @Column(name = "CREATED_ON")
    private Date createdOn;
    
//    @Column(name = "LAST_UPDATE_ON")
    private Date lastUpdateOn;
    
//    @OneToMany(orphanRemoval = true, mappedBy = "parent", fetch = FetchType.LAZY)
    private Set<Comment> children = new HashSet<Comment>(0);
    
    
    public Comment () {
    
    }
    
    public Integer getId () {
    
        return id;
    }
    
    public void setId (Integer id) {
    
        this.id = id;
    }
    
    public Issue getIssue () {
    
        return issue;
    }
    
    public void setIssue (Issue issue) {
    
        this.issue = issue;
    }
    
    public User getAuthor () {
    
        return author;
    }
    
    public void setAuthor (User author) {
    
        this.author = author;
    }
    
    public String getBody () {
    
        return body;
    }
    
    public void setBody (String body) {
    
        this.body = body;
    }
    
    public Date getCreatedOn () {
    
        return createdOn;
    }
    
    public void setCreatedOn (Date createdOn) {
    
        this.createdOn = createdOn;
    }
    
    public Date getLastUpdateOn () {
    
        return lastUpdateOn;
    }
    
    public void setLastUpdateOn (Date lastUpdateOn) {
    
        this.lastUpdateOn = lastUpdateOn;
    }
    
    public Comment getParent () {
    
        return parent;
    }
    
    public void setParent (Comment parent) {
    
        this.parent = parent;
    }
    
    public Set<Comment> getChildren () {
    
        return children;
    }
    
    public void setChildren (Set<Comment> children) {
    
        this.children = children;
    }
    
    @Override
    public int hashCode () {
    
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }
    
    @Override
    public boolean equals (Object obj) {
    
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Comment))
            return false;
        Comment other = (Comment) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }
}
