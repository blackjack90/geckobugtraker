package es.kazbeel.geckobugtracker.model;

import java.io.Serializable;


public class Watcher extends IssueActor implements Serializable {
    
    private static final long serialVersionUID = -1;
    
    public Watcher () {
        super();
    }
    
    public Watcher (User user, Issue issue) {
        super(user, issue);
    }

}
