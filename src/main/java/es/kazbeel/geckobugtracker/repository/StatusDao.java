package es.kazbeel.geckobugtracker.repository;


import java.util.List;

import es.kazbeel.geckobugtracker.model.Status;


public interface StatusDao extends BaseDao<Status> {
    
    public List<Status> findAll ();
    
    public List<Status> findAllEnabled ();

    public Status findById (Integer id);
}
