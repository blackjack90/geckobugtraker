package es.kazbeel.geckobugtracker.repository.hibernate;


import java.util.List;

import org.springframework.stereotype.Repository;

import es.kazbeel.geckobugtracker.model.Priority;
import es.kazbeel.geckobugtracker.repository.PriorityDao;


@Repository
public class PriorityDaoImpl extends BaseDaoImpl<Priority> implements PriorityDao {
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Priority> findAll () {
    
        return getSession().getNamedQuery("Priority_findAll").list();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Priority> findAllEnabled () {
    
        return getSession().getNamedQuery("Priority_findAllEnabled").list();
    }
    
    @Override
    public Priority findById (Integer id) {
    
        return (Priority) getSession().get(Priority.class, id);
    }
    
    @Override
    public void increaseOrder (Integer id) {
    
        getSession().getNamedQuery("Priority_decreaseSortValue").setInteger("priorityId", id).executeUpdate();
    }

    @Override
    public void decreaseOrder (Integer id) {
    
        getSession().getNamedQuery("Priority_increaseSortValue").setInteger("priorityId", id).executeUpdate();
    }
}
