package es.kazbeel.geckobugtracker.repository.hibernate;


import java.util.List;

import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import es.kazbeel.geckobugtracker.model.Section;
import es.kazbeel.geckobugtracker.repository.SectionDao;


@Repository
public class SectionDaoImpl extends BaseDaoImpl<Section> implements SectionDao {
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Section> findAll () {
    
        return getSession().createCriteria(Section.class).addOrder(Order.asc("sortValue")).list();
    }
    
    @Override
    public Section findById (Long id) {
    
        return (Section) getSession().get(Section.class, id);
    }
    
    @Override
    public Section findByName (String name) {
    
        return (Section) getSession().getNamedQuery("Section.findByName").setString("name", name).uniqueResult();
    }
    
    @Override
    public void increaseSortValue (Long id) {
    
        getSession().getNamedQuery("Section.increaseSortValue").setLong("id", id).executeUpdate();
    }
    
    @Override
    public void decreaseSortValue (Long id) {
    
        getSession().getNamedQuery("Section.decreaseSortValue").setLong("id", id).executeUpdate();
    }
}
