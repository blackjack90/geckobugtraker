package es.kazbeel.geckobugtracker.repository.hibernate;


import java.util.List;

import org.springframework.stereotype.Repository;

import es.kazbeel.geckobugtracker.model.Category;
import es.kazbeel.geckobugtracker.repository.CategoryDao;


@Repository
public class CategoryDaoImpl extends BaseDaoImpl<Category> implements CategoryDao {
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Category> findAll () {
    
        return getSession().getNamedQuery("Category_findAll").list();
    }
    
    @Override
    public Category findById (Integer id) {
    
        return (Category) getSession().getNamedQuery("Category_findById").setInteger("id", id).uniqueResult();
    }
}
