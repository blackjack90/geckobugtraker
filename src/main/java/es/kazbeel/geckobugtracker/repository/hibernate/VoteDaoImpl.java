package es.kazbeel.geckobugtracker.repository.hibernate;


import java.util.List;

import org.springframework.stereotype.Repository;

import es.kazbeel.geckobugtracker.model.Vote;
import es.kazbeel.geckobugtracker.repository.VoteDao;


@Repository
public class VoteDaoImpl extends BaseDaoImpl<Vote> implements VoteDao {
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Vote> findByUser (Integer userId, int offset, int size) {
    
        return getSession().getNamedQuery("Vote_findByUser")
                           .setInteger("userId", userId)
                           .setFirstResult(offset)
                           .setMaxResults(size)
                           .list();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Vote> findByIssue (Integer issueId) {
    
        return getSession().getNamedQuery("Vote_findByIssue").setInteger("issueId", issueId).list();
    }
    
    @Override
    public Long countByUser (Integer userId) {
    
        return (Long) getSession().getNamedQuery("Vote_countByUser").setInteger("userId", userId).uniqueResult();
    }
    
    @Override
    public Long countByIssue (Integer issueId) {
    
        return (Long) getSession().getNamedQuery("Vote_countByIssue").setInteger("issueId", issueId).uniqueResult();
    }
    
    @Override
    public Boolean isIssueVotedByUser (Integer issueId, Integer userId) {
    
        return (Boolean) getSession().getNamedQuery("Vote_isIssueVotedByUser")
                                     .setInteger("issueId", issueId)
                                     .setInteger("userId", userId)
                                     .uniqueResult();
    }
}
