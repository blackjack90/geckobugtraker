package es.kazbeel.geckobugtracker.repository.hibernate;


import java.util.List;

import org.springframework.stereotype.Repository;

import es.kazbeel.geckobugtracker.model.Status;
import es.kazbeel.geckobugtracker.repository.StatusDao;


@Repository
public class StatusDaoImpl extends BaseDaoImpl<Status> implements StatusDao {
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Status> findAll () {
    
        return getSession().getNamedQuery("Status_findAll").list();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Status> findAllEnabled () {
    
        return getSession().getNamedQuery("Status_findAllEnabled").list();
    }
    
    @Override
    public Status findById (Integer id) {
    
        return (Status) getSession().getNamedQuery("Status_findById").setInteger("statusId", id).uniqueResult();
    }
}
