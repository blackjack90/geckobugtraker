package es.kazbeel.geckobugtracker.repository.hibernate;


import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import es.kazbeel.geckobugtracker.model.Issue;
import es.kazbeel.geckobugtracker.repository.IssueDao;


@Repository
public class IssueDaoImpl extends BaseDaoImpl<Issue> implements IssueDao {
    
    private static final Logger LOG = LoggerFactory.getLogger(IssueDaoImpl.class);
    
    
    @Override
    public Issue findById (Integer id) {
    
        return (Issue) getSession().get(Issue.class, id);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Issue> findByAssigneeAndResolution (Integer assigneeId, Integer resolutionId, int offset, int size) {
    
        return getSession().getNamedQuery("Issue_findByAssigneeAndResolution")
                           .setInteger("assigneeId", assigneeId)
                           .setInteger("resolutionId", resolutionId)
                           .setFirstResult(offset)
                           .setMaxResults(size)
                           .list();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Issue> findAddedRecentlyTable (Date deadline) {
    
        LOG.debug("Fetching issues newer than " + deadline);
        
        return getSession().getNamedQuery("Issue.findAddedRecentlyTable").setTimestamp("deadline", deadline).list();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Issue> findAll (int offset, int size) {
    
        return getSession().getNamedQuery("Issue_findAll").setFirstResult(offset).setMaxResults(size).list();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<Issue> findByReporter (Integer reporterId, int offset, int size) {
    
        return getSession().getNamedQuery("Issue_findByReporter")
                           .setFirstResult(offset)
                           .setMaxResults(size)
                           .setInteger("reporterId", reporterId)
                           .list();
    }
    
    @Override
    public Long countByReporter (Integer reporter) {
    
        return (Long) getSession().getNamedQuery("Issue_countByReporter")
                                  .setInteger("reporterId", reporter)
                                  .uniqueResult();
    }
    
    @Override
    public Long countByAssigneeAndResolution (Integer assignee, Integer resolution) {
    
        return (Long) getSession().getNamedQuery("Issue_countByAssigneeAndResolution")
                                  .setInteger("assigneeId", assignee)
                                  .setInteger("resolutionId", resolution)
                                  .uniqueResult();
    }
    
    @Override
    public Long countAll () {
    
        return (Long) getSession().getNamedQuery("Issue_countAll").uniqueResult();
    }
}
