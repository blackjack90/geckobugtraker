package es.kazbeel.geckobugtracker.repository.hibernate;


import java.util.List;

import org.springframework.stereotype.Repository;

import es.kazbeel.geckobugtracker.model.IssueLink;
import es.kazbeel.geckobugtracker.repository.IssueLinkDao;


@Repository
public class IssueLinkDaoImpl extends BaseDaoImpl<IssueLink> implements IssueLinkDao {
    
    @Override
    public IssueLink findById (Integer id) {
        
        return (IssueLink) getSession().get(IssueLink.class, id);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<IssueLink> findAllWithSource (Integer sourceId) {
    
        return getSession().getNamedQuery("IssueLink_findAllWithSource").setInteger("sourceId", sourceId).list();
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<IssueLink> findAllWithDestination (Integer destinationId) {
    
        return getSession().getNamedQuery("IssueLink_findAllWithDestination").setInteger("destinationId", destinationId).list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<IssueLink> findAllRelatedToIssue (Integer issueId) {
        
        return getSession().getNamedQuery("IssueLink_findAllRelatedToIssue").setInteger("issueId", issueId).list();
    }
}
