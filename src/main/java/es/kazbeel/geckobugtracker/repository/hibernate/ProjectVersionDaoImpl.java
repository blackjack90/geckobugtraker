package es.kazbeel.geckobugtracker.repository.hibernate;

import java.util.List;

import org.springframework.stereotype.Repository;

import es.kazbeel.geckobugtracker.model.ProjectVersion;
import es.kazbeel.geckobugtracker.repository.ProjectVersionDao;


@Repository
public class ProjectVersionDaoImpl extends BaseDaoImpl<ProjectVersion> implements ProjectVersionDao {

    @Override
    public ProjectVersion findById (Long projectVersionId) {
        
        return (ProjectVersion) getSession().get(ProjectVersion.class, projectVersionId);
    }
    
    @Override
    @SuppressWarnings("unchecked")
    public List<ProjectVersion> findByProject (Long id) {
    
        return getSession().getNamedQuery("ProjectVersion.findByProjectId").setLong("id", id).list();
    }

    @Override
    public void increaseSortValue (Long id) {
    
        getSession().getNamedQuery("ProjectVersion.increaseSortValue").setLong("id", id).executeUpdate();
    }
    
    @Override
    public void decreaseSortValue (Long id) {
    
        getSession().getNamedQuery("ProjectVersion.decreaseSortValue").setLong("id", id).executeUpdate();
    }
}
