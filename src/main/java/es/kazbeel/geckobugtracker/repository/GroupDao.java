package es.kazbeel.geckobugtracker.repository;


import java.util.List;

import es.kazbeel.geckobugtracker.model.Group;


public interface GroupDao extends BaseDao<Group> {
    
    public List<Group> findAll (int offset, int size);
    
    public List<Group> findAllEnabled ();
    
    public Group findById (Integer id);
    
    public Long countAll ();
}
