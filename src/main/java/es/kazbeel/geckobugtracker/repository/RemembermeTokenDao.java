package es.kazbeel.geckobugtracker.repository;


import java.util.List;

import es.kazbeel.geckobugtracker.model.RemembermeToken;


public interface RemembermeTokenDao extends BaseDao<RemembermeToken> {
    
    public List<RemembermeToken> findByUsername (String username);
    
    public RemembermeToken findBySeries (String series);
}
