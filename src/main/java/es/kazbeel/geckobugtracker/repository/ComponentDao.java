package es.kazbeel.geckobugtracker.repository;


import java.util.List;

import es.kazbeel.geckobugtracker.model.Component;


public interface ComponentDao extends BaseDao<Component> {
    
    public Component findById (Long componentId);
    
    public Component findByName (String name);
    
    public List<Component> findByProject (Long projectId);
}
