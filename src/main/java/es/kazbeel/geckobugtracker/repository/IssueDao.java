package es.kazbeel.geckobugtracker.repository;


import java.util.Date;
import java.util.List;

import es.kazbeel.geckobugtracker.model.Issue;


public interface IssueDao extends BaseDao<Issue> {
    
    public Issue findById (Integer id);

    public List<Issue> findByAssigneeAndResolution (Integer assigneeId, Integer resolutionId, int offset, int size);
    
    public List<Issue> findAddedRecentlyTable (Date deadline);
    
    public List<Issue> findAll (int offset, int size);
    
    public List<Issue> findByReporter (Integer reporterId, int offset, int size);
    
    public Long countByReporter (Integer reporter);
    
    public Long countByAssigneeAndResolution (Integer assignee, Integer resolution);
    
    public Long countAll ();
}
