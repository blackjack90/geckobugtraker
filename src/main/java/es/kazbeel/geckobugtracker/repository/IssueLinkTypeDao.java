package es.kazbeel.geckobugtracker.repository;


import java.util.List;

import es.kazbeel.geckobugtracker.model.IssueLinkType;


public interface IssueLinkTypeDao extends BaseDao<IssueLinkType> {
    
    public List<IssueLinkType> findAll ();
    
    public IssueLinkType findById (Integer id);
}
