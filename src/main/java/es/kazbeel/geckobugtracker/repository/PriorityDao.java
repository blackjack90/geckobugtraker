package es.kazbeel.geckobugtracker.repository;


import java.util.List;

import es.kazbeel.geckobugtracker.model.Priority;


public interface PriorityDao extends BaseDao<Priority> {
    
    public List<Priority> findAll ();
    
    public List<Priority> findAllEnabled ();
    
    public Priority findById (Integer id);

    public void increaseOrder (Integer id);
    
    public void decreaseOrder (Integer id);
}
