package es.kazbeel.geckobugtracker.repository;

import java.util.List;

import es.kazbeel.geckobugtracker.model.ProjectVersion;


public interface ProjectVersionDao extends BaseDao<ProjectVersion> {

    public ProjectVersion findById (Long projectVersionId);

    public List<ProjectVersion> findByProject (Long id);

    public void increaseSortValue (Long id);
    
    public void decreaseSortValue (Long id);
}
