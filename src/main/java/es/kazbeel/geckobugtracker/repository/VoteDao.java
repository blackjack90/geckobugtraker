package es.kazbeel.geckobugtracker.repository;


import java.util.List;

import es.kazbeel.geckobugtracker.model.Vote;


public interface VoteDao extends BaseDao<Vote> {
    
    public List<Vote> findByUser (Integer userId, int offset, int size);
    
    public List<Vote> findByIssue (Integer issueId);
    
    public Long countByUser (Integer userId);
    
    public Long countByIssue (Integer issueId);

    public Boolean isIssueVotedByUser (Integer issueId, Integer userId);
}
