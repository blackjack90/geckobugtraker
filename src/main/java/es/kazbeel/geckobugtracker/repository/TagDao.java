package es.kazbeel.geckobugtracker.repository;


import java.util.List;

import es.kazbeel.geckobugtracker.model.Tag;


public interface TagDao extends BaseDao<Tag> {
    
    public List<Tag> findAll ();
    
    public List<Tag> findAllEnabled ();
    
    public Tag findById (Integer id);
}
