package es.kazbeel.geckobugtracker.web.controller.admin;


import es.kazbeel.geckobugtracker.model.Project;


public class ShareComponentForm {
    
    private Long componentId;
    
    private Project project;
    
    
    public ShareComponentForm () {
    
    }
    
    public Long getComponentId () {
    
        return componentId;
    }
    
    public void setComponentId (Long componentId) {
    
        this.componentId = componentId;
    }
    
    public Project getProject () {
    
        return project;
    }
    
    public void setProject (Project project) {
    
        this.project = project;
    }
}
