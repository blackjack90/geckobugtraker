package es.kazbeel.geckobugtracker.web.controller.admin;


import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.kazbeel.geckobugtracker.model.Section;
import es.kazbeel.geckobugtracker.service.ProjectService;
import es.kazbeel.geckobugtracker.web.controller.exception.ResourceNotFoundException;


@Controller
public class SectionController {
    
    private static final Logger LOG = LoggerFactory.getLogger(SectionController.class);
    
    @Autowired
    private ProjectService projectService;
    
    
    @RequestMapping(value = "/admin/sections/ViewSections", method = RequestMethod.GET)
    public ModelAndView viewSections () {
    
        LOG.debug("Showing List of Sections");
        
        ModelAndView mav = new ModelAndView("/admin/sections/ViewSections");
        
        mav.addObject("sections", projectService.getAllSections());
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/sections/CreateSection", method = RequestMethod.GET)
    public ModelAndView createSectionGet () {
    
        LOG.debug("Showing Create Section view");
        
        ModelAndView mav = new ModelAndView("/admin/sections/CreateSection");
        mav.addObject("section", new Section());
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/sections/CreateSection", method = RequestMethod.POST)
    public String createSectionPost (@Valid @ModelAttribute("section") Section section,
                                     BindingResult result,
                                     ModelMap model) {
    
        LOG.debug("Creating new Section");
        
        if (result.hasErrors() == true) {
            LOG.debug("The {} is not valid: {}", result.getFieldError().getField(), result.getFieldError()
                                                                                          .getDefaultMessage());
            
            return "/admin/sections/CreateSection";
        }
        
        try {
            projectService.saveSection(section);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return "/admin/sections/CreateSection";
        }
        
        model.clear();
        
        return "redirect:/admin/sections/ViewSections";
    }
    
    @RequestMapping(value = "/admin/sections/EditSection", params = {"id"}, method = RequestMethod.GET)
    public ModelAndView editSectionGet (@RequestParam("id") Long sectionId) {
    
        LOG.debug("Showing Edit Section view (ID = {})", sectionId);
        
        ModelAndView mav = new ModelAndView("/admin/sections/EditSection");
        mav.addObject("section", projectService.getSectionById(sectionId));
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/sections/EditSection", params = {"id"}, method = RequestMethod.POST)
    public String editSectionPost (@RequestParam("id") Long sectionId,
                                   @Valid @ModelAttribute("section") Section section,
                                   BindingResult result,
                                   ModelMap model) {
    
        LOG.debug("Updating Section (ID = {})", sectionId);
        
        if (result.hasErrors() == true) {
            LOG.debug("The {} is not valid: {}", result.getFieldError().getField(), result.getFieldError()
                                                                                          .getDefaultMessage());
            
            return "/admin/sections/EditSection";
        }
        
        try {
            projectService.saveSection(section);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return "/admin/sections/EditSection";
        }
        
        model.clear();
        
        return "redirect:/admin/sections/ViewSections";
    }
    
    @RequestMapping(value = "/admin/sections/DeleteSection", params = {"id"}, method = RequestMethod.GET)
    public String deleteSection (@RequestParam("id") Long sectionId, RedirectAttributes redirectAttributes) {
    
        LOG.debug("Deleting Section (ID = {})", sectionId);
        
        try {
            projectService.deleteSection(projectService.getSectionById(sectionId));
            redirectAttributes.addFlashAttribute("msgSuccess", "Section deleted successfully.");
            
            LOG.debug("Section deleted successfully");
        } catch (DataIntegrityViolationException ex) {
            redirectAttributes.addFlashAttribute("msgError",
                                                 "Section cannot be deleted because it interacts with some projects.");
            LOG.debug("Section cannot be deleted because it interacts with some issues");
        } catch (IllegalArgumentException ex) {
            redirectAttributes.addFlashAttribute("msgError", "Section does not exist.");
            LOG.debug("Section cannot be deleted because it interacts with some projects");
        }
        
        return "redirect:/admin/sections/ViewSections";
    }
    
    @RequestMapping(value = "/admin/sections/SortSection", params = {"id", "move"}, method = RequestMethod.GET)
    public String setSectionOrder (@RequestParam("id") Long sectionId,
                                   @RequestParam("move") String move,
                                   RedirectAttributes redirectAttributes) {
    
        LOG.debug("Moving Section {} (ID = {})", move, sectionId);
        
        Section section = projectService.getSectionById(sectionId);
        
        if (section == null) {
            redirectAttributes.addFlashAttribute("msgError", "The section does not exist.");
            
            LOG.debug("The section (ID = {}) does not exist", sectionId);
        } else {
            if (move.equalsIgnoreCase("up")) {
                projectService.setHigherOrderToSection(section);
            } else if (move.equalsIgnoreCase("down")) {
                projectService.setLowerOrderToSection(section);
            }
        }
        
        return "redirect:/admin/sections/ViewSections";
    }
    
    @RequestMapping(value = "/admin/sections/DetailsSection", params = {"id"}, method = RequestMethod.GET)
    public ModelAndView detailsSection (@RequestParam("id") Long sectionId) {
    
        LOG.debug("Showing Details of Section (ID = {})", sectionId);
        
        ModelAndView mav = new ModelAndView("/admin/sections/DetailsSection");
        
        Section section = projectService.getSectionByIdWithProjects(sectionId);
        
        if (section == null) {
            LOG.debug("Requested Section does not exist");
            
            throw new ResourceNotFoundException();
        }
        
        mav.addObject("section", section);
        
        return mav;
    }
}
