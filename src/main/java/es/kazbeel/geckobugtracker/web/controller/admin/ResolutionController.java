package es.kazbeel.geckobugtracker.web.controller.admin;


import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.kazbeel.geckobugtracker.model.Resolution;
import es.kazbeel.geckobugtracker.service.IssueService;


@Controller
public class ResolutionController {
    
    private static final Logger LOG = LoggerFactory.getLogger(ResolutionController.class);
    
    @Autowired
    IssueService issueService;
    
    
    @RequestMapping(value = "/admin/resolutions/ViewResolutions", method = RequestMethod.GET)
    public ModelAndView viewResolutions () {
    
        LOG.debug("Showing List of Resolutions");
        
        ModelAndView mav = new ModelAndView("/admin/resolutions/ViewResolutions");
        mav.addObject("resolutions", issueService.getAllResolutions());
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/resolutions/CreateResolution", method = RequestMethod.GET)
    public ModelAndView createResolutionGet () {
    
        LOG.debug("Showing Create Resolution page");
        
        ModelAndView mav = new ModelAndView("/admin/resolutions/CreateResolution");
        mav.addObject("resolution", new Resolution());
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/resolutions/CreateResolution", method = RequestMethod.POST)
    public String createResolutionPost (@Valid @ModelAttribute("resolution") Resolution resolution,
                                        BindingResult result) {
    
        LOG.debug("Creating new Resolution");
        
        if (result.hasErrors() == true) {
            LOG.debug("The resolution is not valid: {}", result.getFieldError().getDefaultMessage());
            
            return "/admin/resolutions/CreateResolution";
        }
        
        try {
            issueService.saveResolution(resolution);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return "/admin/resolutions/CreateResolution";
        }
        
        return "redirect:/admin/resolutions/ViewResolutions";
    }
    
    @RequestMapping(value = "/admin/resolutions/EditResolution", params = {"id"}, method = RequestMethod.GET)
    public ModelAndView editResolutionGet (@RequestParam("id") Integer resolutionId) {
    
        LOG.debug("Showing Edit Resolution page (ID = {})", resolutionId);
        
        ModelAndView mav = new ModelAndView("/admin/resolutions/EditResolution");
        mav.addObject("resolution", issueService.getResolutionById(resolutionId));
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/resolutions/EditResolution", params = {"id"}, method = RequestMethod.POST)
    public String editResolutionPost (@RequestParam("id") Integer resolutionId,
                                      @Valid @ModelAttribute("resolution") Resolution resolution,
                                      BindingResult result) {
    
        LOG.debug("Updating Resolution (ID = {})", resolutionId);
        
        if (result.hasErrors() == true) {
            LOG.debug("The resolution is not valid: {}", result.getFieldError().getDefaultMessage());
            
            return "/admin/resolutions/EditResolution";
        }
        
        try {
            issueService.saveResolution(resolution);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return "/admin/resolutions/EditResolution";
        }
        
        return "redirect:/admin/resolutions/ViewResolutions";
    }
    
    @RequestMapping(value = "/admin/resolutions/DeleteResolution", params = {"id"}, method = RequestMethod.GET)
    public String deleteResolution (@RequestParam("id") Integer resolutionId,
                                    RedirectAttributes redirectAttributes) {
    
        LOG.debug("Deleting Resolution (ID = {})", resolutionId);
        
        try {
            issueService.deleteResolution(issueService.getResolutionById(resolutionId));
            redirectAttributes.addFlashAttribute("msgSuccess", "Resolution deleted successfully.");
            
            LOG.debug("Resolution deleted successfully");
        } catch (DataIntegrityViolationException ex) {
            redirectAttributes.addFlashAttribute("msgError",
                                                 "Resolution cannot be deleted because it interacts with some issues.");
            LOG.debug("Resolution cannot be deleted because it interacts with some issues");
        } catch (IllegalArgumentException ex) {
            redirectAttributes.addFlashAttribute("msgError", "Resolution does not exist.");
            LOG.debug("Resolution cannot be deleted because it interacts with some issues");
        }
        
        return "redirect:/admin/resolutions/ViewResolutions";
    }
    
    @RequestMapping(value = "/admin/resolutions/SortResolution", params = {"id", "move"}, method = RequestMethod.GET)
    public String setResolutionOrder (@RequestParam("id") Integer resolutionId,
                                      @RequestParam("move") String move,
                                      RedirectAttributes redirectAttributes) {
    
        LOG.debug("Moving Resolution {} (ID = {})", move, resolutionId);
        
        Resolution resolution = issueService.getResolutionById(resolutionId);
        
        if (resolution == null) {
            redirectAttributes.addFlashAttribute("msgError", "The resolution does not exist.");
            
            LOG.debug("The resolution (ID = {}) does not exist", resolutionId);
        } else {
            if (move.equalsIgnoreCase("up")) {
                issueService.setHigherOrderToResolution(resolution);
            } else if (move.equalsIgnoreCase("down")) {
                issueService.setLowerOrderToResolution(resolution);
            }
        }
        
        return "redirect:/admin/resolutions/ViewResolutions";
    }
}
