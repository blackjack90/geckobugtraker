package es.kazbeel.geckobugtracker.web.controller.admin;


import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.service.AuthService;
import es.kazbeel.geckobugtracker.service.UserGroupService;
import es.kazbeel.geckobugtracker.support.Page;
import es.kazbeel.geckobugtracker.support.PageRequest;
import es.kazbeel.geckobugtracker.web.entityeditors.GroupListEditor;


@Controller
@SessionAttributes("groupList")
public class UserController {
    
    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);
    
    @Autowired
    private UserGroupService userGroupService;
    
    @Autowired
    private AuthService authService;
    
    @Autowired
    private BCryptPasswordEncoder passwordEncoder;
    
    
    @ModelAttribute("groupList")
    public void populate (Model model) {
    
        model.addAttribute("groupList", userGroupService.getAllEnabledGroups());
    }
    
    @InitBinder
    public void initBinder (WebDataBinder binder) {
    
        binder.registerCustomEditor(Set.class, new GroupListEditor(userGroupService));
    }
    
    @RequestMapping(value = "/admin/users/ViewUsers", method = RequestMethod.GET)
    public ModelAndView viewUsers (@RequestParam(value = "page", required = false, defaultValue = "0") Integer page) {
    
        LOG.debug("Showing List of Users (page = {})", page);
        
        PageRequest pageRequest = new PageRequest(page, 5);
        Page<User> usersPage = userGroupService.getAllUsers(pageRequest);
        
        if (usersPage.getTotalPages() < page) {
            LOG.debug("Trying to show page {} out of {}", page, usersPage.getTotalPages());
            
            return new ModelAndView("redirect:/admin/users/ViewUsers");
        }
        
        ModelAndView mav = new ModelAndView("/admin/users/ViewUsers");
        mav.addObject("users", usersPage.getContent());
        mav.addObject("totalPages", usersPage.getTotalPages());
        mav.addObject("isFirst", usersPage.isFirst());
        mav.addObject("isLast", usersPage.isLast());
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/users/CreateUser", method = RequestMethod.GET)
    public ModelAndView createUsetGet () {
    
        LOG.debug("Showing Create Issue View");
        
        ModelAndView mav = new ModelAndView("/admin/users/CreateUser");
        mav.addObject("user", new User());
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/users/CreateUser", method = RequestMethod.POST)
    public String createUserPost (@Valid @ModelAttribute("user") User user,
                                  BindingResult result,
                                  SessionStatus status,
                                  ModelMap model) {
    
        LOG.debug("Creating new User");
        
        if (result.hasErrors() == true) {
            StringBuilder msgError = new StringBuilder();
            
            for (FieldError fieldError : result.getFieldErrors()) {
                msgError.append(fieldError.getField() + " " + fieldError.getDefaultMessage() + System.getProperty("line.separator"));
            }
            
            LOG.debug("The User is not valid: {}", msgError);
            
            model.addAttribute("msgError", msgError);
            
            return "/admin/users/CreateUser";
        }
        
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        
        try {
            userGroupService.saveUser(user);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMostSpecificCause().getMessage());
            
            user.setPassword(null);

            // TODO Add {msgError} message explaining the problem (i.e. Username already exists)
            
            return "/admin/users/CreateUser";
        }
        
        status.setComplete();
        
        return "redirect:/admin/users/ViewUsers";
    }
    
    @RequestMapping(value = "/admin/users/EditUser", params = {"id"}, method = RequestMethod.GET)
    public ModelAndView editUserGet (@RequestParam(value = "id") Integer userId) {
    
        LOG.debug("Showing Edit User View (ID = {})", userId);
        
        ModelAndView mav = new ModelAndView();
        User user = userGroupService.getUserById(userId);
        
        if (user == null) {
            LOG.debug("The user (ID = {}) does not exist", userId);
            
            mav.setViewName("redirect:/admin/users/ViewUsers");
        } else {
            mav.setViewName("/admin/users/EditUser");
            mav.addObject("user", user);
        }
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/users/EditUser", params = {"id"}, method = RequestMethod.POST)
    public String editUserPost (@RequestParam(value = "id") Integer userId,
                                @Valid @ModelAttribute("user") User user,
                                BindingResult result,
                                SessionStatus status,
                                ModelMap model) {
    
        LOG.debug("Updating User (ID = {})", userId);
        
        if (result.hasErrors() == true) {
            if ((result.getErrorCount() > 1) ||
                (!result.getFieldError().getField().equalsIgnoreCase("password"))) {
                
                StringBuilder msgError = new StringBuilder();
                
                for (FieldError fieldError : result.getFieldErrors()) {
                    if (fieldError.getField().equalsIgnoreCase("password") == false) {
                        msgError.append(fieldError.getField() + " " + fieldError.getDefaultMessage() + System.getProperty("line.separator"));
                    }
                }
                
                LOG.debug("The User is not valid: {}", msgError);
                
                model.addAttribute("msgError", msgError);
                
                return "/admin/users/EditUser";
            }
        }
        
        User dbUser = userGroupService.getUserById(userId);
        
        if (dbUser == null) {
            LOG.debug("The user (ID = {}) does not exist", userId);
            
            status.setComplete();
            return "redirect:/admin/users/ViewUsers";
        }
        
        user.setPassword(dbUser.getPassword());
        user.setCreatedOn(dbUser.getCreatedOn());
        
        try {
            userGroupService.saveUser(user);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return "/admin/users/EditUser";
        }
        
        status.setComplete();
        
        return "redirect:/admin/users/ViewUsers";
    }
    
    @RequestMapping(value = "/admin/users/DeleteUser", params = {"id"}, method = RequestMethod.GET)
    public String deleteUser (@RequestParam(value = "id") Integer userId, RedirectAttributes redirectAttributes) {
    
        LOG.debug("Deleting User (ID = {})", userId);
        
        try {
            User user = userGroupService.getUserById(userId);
            
            if (user == null) {
                throw new IllegalArgumentException();
            }
            
            userGroupService.deleteUser(user);
            authService.deleteTokensFromUsername(user.getLoginName());
            authService.deleteLoginInfoFromUsername(user.getLoginName());
            
            LOG.debug("User deleted successfully");
            
            redirectAttributes.addFlashAttribute("msgSuccess", "User deleted successfully.");
        } catch (DataIntegrityViolationException ex) {
            LOG.debug("User cannot be deleted because it interacts with some issues");
            
            redirectAttributes.addFlashAttribute("msgError",
                                                 "User cannot be deleted because it interacts with some issues.");
        } catch (IllegalArgumentException ex) {
            LOG.debug("User does not exist");
            
            redirectAttributes.addFlashAttribute("msgError", "User does not exist.");
        }
        
        return "redirect:/admin/users/ViewUsers";
    }
}
