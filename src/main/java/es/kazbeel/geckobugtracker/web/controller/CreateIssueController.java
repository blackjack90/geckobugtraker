package es.kazbeel.geckobugtracker.web.controller;


import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import es.kazbeel.geckobugtracker.model.Category;
import es.kazbeel.geckobugtracker.model.Environment;
import es.kazbeel.geckobugtracker.model.IssueType;
import es.kazbeel.geckobugtracker.model.Priority;
import es.kazbeel.geckobugtracker.model.Project;
import es.kazbeel.geckobugtracker.model.Resolution;
import es.kazbeel.geckobugtracker.model.Status;
import es.kazbeel.geckobugtracker.model.Tag;
import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.service.IssueService;
import es.kazbeel.geckobugtracker.service.ProjectService;
import es.kazbeel.geckobugtracker.service.UserGroupService;
import es.kazbeel.geckobugtracker.web.entityeditors.CategoryEditor;
import es.kazbeel.geckobugtracker.web.entityeditors.EnvironmentsEditor;
import es.kazbeel.geckobugtracker.web.entityeditors.IssueStatusEditor;
import es.kazbeel.geckobugtracker.web.entityeditors.IssueTypeEditor;
import es.kazbeel.geckobugtracker.web.entityeditors.PriorityEditor;
import es.kazbeel.geckobugtracker.web.entityeditors.ProjectEditor;
import es.kazbeel.geckobugtracker.web.entityeditors.ResolutionEditor;
import es.kazbeel.geckobugtracker.web.entityeditors.TagsEditor;
import es.kazbeel.geckobugtracker.web.entityeditors.UserEditor;
import es.kazbeel.geckobugtracker.web.utils.SecurityUtils;


@Controller
public class CreateIssueController {
    
    private static final Logger LOG = LoggerFactory.getLogger(CreateIssueController.class);
    
    @Autowired
    private IssueService issueService;
    
    @Autowired
    private UserGroupService userGroupService;
    
    @Autowired
    private ProjectService projectService;
    
    
    @ModelAttribute("environments")
    public Collection<Environment> populateAllEnvironments () {
    
        return issueService.getAllEnvironments();
    }
    
    @ModelAttribute("tags")
    public Collection<Tag> populateAllTags () {
    
        return issueService.getAllTags();
    }
    
    @InitBinder
    public void initBinder (WebDataBinder binder) {
    
        binder.registerCustomEditor(IssueType.class, new IssueTypeEditor(issueService));
        binder.registerCustomEditor(Status.class, new IssueStatusEditor(issueService));
        binder.registerCustomEditor(Priority.class, new PriorityEditor(issueService));
        binder.registerCustomEditor(Resolution.class, new ResolutionEditor(issueService));
        binder.registerCustomEditor(Set.class, "issue.environments", new EnvironmentsEditor(Set.class, issueService));
        binder.registerCustomEditor(Set.class, "issue.tags", new TagsEditor(Set.class, issueService));
        binder.registerCustomEditor(User.class, new UserEditor(userGroupService));
        binder.registerCustomEditor(Category.class, new CategoryEditor(issueService));
        binder.registerCustomEditor(Project.class, new ProjectEditor(projectService));
    }
    
//    @RequestMapping(value = "/issues/SelectCurrentProject", method = RequestMethod.POST)
//    public ModelAndView selectCurrentProject (@ModelAttribute("issuePackage") IssueTagEnvironment issuePackage) {
//    
//        ModelAndView mav = new ModelAndView("/issues/CreateIssue");
//        
//        LOG.debug("Showing formulary to Create an Issue");
//        
//        Collection<IssueType> types = issueService.getAllIssueTypes();
//        Collection<Status> statuses = issueService.getAllStatuses();
//        Collection<Priority> priorities = issueService.getAllPriorities();
//        Collection<Resolution> resolutions = issueService.getAllResolutions();
//        Collection<User> users = userGroupService.getAllEnabledUsers();
//        Collection<Category> categories = issueService.getAllCategories();
//        
//        mav.addObject("types", types);
//        mav.addObject("statuses", statuses);
//        mav.addObject("priorities", priorities);
//        mav.addObject("resolutions", resolutions);
//        mav.addObject("users", users);
//        mav.addObject("categories", categories);
//        mav.addObject("allProjects", projectService.getAllProjects());
//        mav.addObject("components", projectService.getComponentsByProject(issuePackage.getProject()));
//        
//        return mav;
//    }
    
    @RequestMapping(value = "/issues/CreateIssue", method = RequestMethod.GET)
    public ModelAndView createIssueGet () {
    
        ModelAndView mav = new ModelAndView("/issues/CreateIssue");
        
        LOG.debug("Showing formulary to Create an Issue");
        
        Collection<IssueType> types = issueService.getAllIssueTypes();
        Collection<Status> statuses = issueService.getAllStatuses();
        Collection<Priority> priorities = issueService.getAllPriorities();
        Collection<Resolution> resolutions = issueService.getAllResolutions();
        Collection<User> users = userGroupService.getAllEnabledUsers();
        Collection<Category> categories = issueService.getAllCategories();
        
        mav.addObject("loggedUser", SecurityUtils.getCurrentUser());
        mav.addObject("types", types);
        mav.addObject("statuses", statuses);
        mav.addObject("priorities", priorities);
        mav.addObject("resolutions", resolutions);
        mav.addObject("users", users);
        mav.addObject("categories", categories);
        mav.addObject("issuePackage", new IssueTagEnvironment());
        mav.addObject("allProjects", projectService.getAllProjects());
        
        return mav;
    }
    
    @RequestMapping(value = "/issues/CreateIssue", method = RequestMethod.POST)
    public String createIssuePost (IssueTagEnvironment issuePackage,
                                   @ModelAttribute("environments") List<Environment> environments,
                                   @ModelAttribute("tags") List<Tag> tags,
                                   BindingResult result,
                                   SessionStatus status) {
    
        LOG.debug("Creating new Issue");
        
        if (result.hasErrors() == true) {
            return "redirect:/issues/CreateIssue";
        }
        
        // TODO Optimize/Refactor
        List<String> commaList = Arrays.asList(issuePackage.getEnvironmentStr().split(","));
        
        for (String envStr : commaList) {
            if (!envStr.equals("")) {
                Environment environment = new Environment();
                environment.setName(envStr.trim());
                
                if (environments.contains(environment)) {
                    environment = issueService.getEnvironmentById(environments.get(environments.indexOf(environment))
                                                                              .getId());
                } else {
                    issueService.saveEnvironment(environment);
                }
                
                issuePackage.getIssue().addEnvironment(environment);
            }
        }
        
        commaList = Arrays.asList(issuePackage.getTagStr().split(","));
        
        for (String tagStr : commaList) {
            if (!tagStr.equals("")) {
                Tag tag = new Tag();
                tag.setName(tagStr.trim());
                
                if (tags.contains(tag)) {
                    tag = issueService.getTagById(environments.get(tags.indexOf(tag)).getId());
                } else {
                    issueService.saveTag(tag);
                }
                
                issuePackage.getIssue().addTag(tag);
            }
        }
        
        issuePackage.getIssue().setReporter(SecurityUtils.getCurrentUser());
        issuePackage.getIssue().setResolution(issueService.getResolutionById(1));
        issuePackage.getIssue().setStatus(issueService.getStatusById(1));
        
        issueService.saveIssue(issuePackage.getIssue());
        
        status.setComplete();
        
        return "redirect:/issues/" + issuePackage.getIssue().getId();
    }
}
