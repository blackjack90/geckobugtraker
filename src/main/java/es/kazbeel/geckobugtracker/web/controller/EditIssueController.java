package es.kazbeel.geckobugtracker.web.controller;


import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import es.kazbeel.geckobugtracker.model.Category;
import es.kazbeel.geckobugtracker.model.Comment;
import es.kazbeel.geckobugtracker.model.Environment;
import es.kazbeel.geckobugtracker.model.IssueType;
import es.kazbeel.geckobugtracker.model.Priority;
import es.kazbeel.geckobugtracker.model.Resolution;
import es.kazbeel.geckobugtracker.model.Status;
import es.kazbeel.geckobugtracker.model.Tag;
import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.service.IssueService;
import es.kazbeel.geckobugtracker.service.UserGroupService;
import es.kazbeel.geckobugtracker.web.entityeditors.CategoryEditor;
import es.kazbeel.geckobugtracker.web.entityeditors.EnvironmentsEditor;
import es.kazbeel.geckobugtracker.web.entityeditors.IssueStatusEditor;
import es.kazbeel.geckobugtracker.web.entityeditors.IssueTypeEditor;
import es.kazbeel.geckobugtracker.web.entityeditors.PriorityEditor;
import es.kazbeel.geckobugtracker.web.entityeditors.ResolutionEditor;
import es.kazbeel.geckobugtracker.web.entityeditors.TagsEditor;
import es.kazbeel.geckobugtracker.web.entityeditors.UserEditor;
import es.kazbeel.geckobugtracker.web.utils.SecurityUtils;


@Controller
@RequestMapping(value = "/issues/{issueId}")
@SessionAttributes("issuePackage")
public class EditIssueController {
    
    private static final Logger LOG = LoggerFactory.getLogger(EditIssueController.class);
    
    @Autowired
    private IssueService issueService;
    
    @Autowired
    private UserGroupService userGroupService;
    
    
    @ModelAttribute("issuePackage")
    public IssueTagEnvironment populateIssue (@PathVariable("issueId") Integer issueId) {
    
        LOG.debug("Fetching issue with ID=" + issueId);
        
        IssueTagEnvironment issueTagEnv = new IssueTagEnvironment();
        issueTagEnv.setIssue(issueService.getIssueById(issueId));
        
        return issueTagEnv;
    }
    
    @ModelAttribute("environments")
    public Collection<Environment> populateAllEnvironments () {
    
        return issueService.getAllEnvironments();
    }
    
    @ModelAttribute("tags")
    public Collection<Tag> populateAllTags () {
    
        return issueService.getAllTags();
    }
    
    @InitBinder
    public void initBinder (WebDataBinder binder) {
    
        binder.registerCustomEditor(IssueType.class, new IssueTypeEditor(issueService));
        binder.registerCustomEditor(Status.class, new IssueStatusEditor(issueService));
        binder.registerCustomEditor(Priority.class, new PriorityEditor(issueService));
        binder.registerCustomEditor(Resolution.class, new ResolutionEditor(issueService));
        binder.registerCustomEditor(Set.class, "issue.environments", new EnvironmentsEditor(Set.class, issueService));
        binder.registerCustomEditor(Set.class, "issue.tags", new TagsEditor(Set.class, issueService));
        binder.registerCustomEditor(User.class, new UserEditor(userGroupService));
        binder.registerCustomEditor(Category.class, new CategoryEditor(issueService));
    }
    
    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public ModelAndView editIssueGet (@PathVariable("issueId") Integer issueId,
                                      @ModelAttribute("issuePackage") IssueTagEnvironment issuePackage) {
    
        LOG.debug("Showing Edit Issue page (ID = {})", issueId);
        
        ModelAndView mav = new ModelAndView("/issues/EditIssue");
        
        Collection<IssueType> types = issueService.getAllIssueTypes();
        Collection<Status> statuses = issueService.getAllStatuses();
        Collection<Priority> priorities = issueService.getAllPriorities();
        Collection<Resolution> resolutions = issueService.getAllResolutions();
        Collection<User> users = userGroupService.getAllEnabledUsers();
        Collection<Category> categories = issueService.getAllCategories();
        
        mav.addObject("issue", issuePackage.getIssue());
        mav.addObject("types", types);
        mav.addObject("statuses", statuses);
        mav.addObject("priorities", priorities);
        mav.addObject("resolutions", resolutions);
        mav.addObject("users", users);
        mav.addObject("categories", categories);
        
        return mav;
    }
    
    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String editIssuePost (@PathVariable("issueId") Integer issueId,
                                 @ModelAttribute("issuePackage") IssueTagEnvironment issuePackage,
                                 @ModelAttribute("environments") List<Environment> environments,
                                 @ModelAttribute("tags") List<Tag> tags,
                                 @Valid @ModelAttribute("comment") Comment comment,
                                 BindingResult result) {
    
        LOG.debug("Updating issue (ID = {})", issueId);
        
        // TODO Optimize/Refactor
        List<String> commaList = Arrays.asList(issuePackage.getEnvironmentStr().split(","));
        
        for (String envStr : commaList) {
            if (!envStr.equals("")) {
                Environment environment = new Environment();
                environment.setName(envStr.trim());
                
                if (environments.contains(environment)) {
                    environment = issueService.getEnvironmentById(environments.get(environments.indexOf(environment))
                                                                              .getId());
                } else {
                    issueService.saveEnvironment(environment);
                }
                
                issuePackage.getIssue().addEnvironment(environment);
            }
        }
        
        commaList = Arrays.asList(issuePackage.getTagStr().split(","));
        
        for (String tagStr : commaList) {
            if (!tagStr.equals("")) {
                Tag tag = new Tag();
                tag.setName(tagStr.trim());
                
                if (tags.contains(tag)) {
                    tag = issueService.getTagById(tags.get(tags.indexOf(tag)).getId());
                } else {
                    issueService.saveTag(tag);
                }
                
                issuePackage.getIssue().addTag(tag);
            }
        }
        
        issueService.saveIssue(issuePackage.getIssue());
        
        // (comment != null) && (comment.getBody().isEmpty() == false)
        if (result.hasErrors() == false) {
            LOG.debug("Creating new comment for issue (ID = {})", issueId);
            
            User user = SecurityUtils.getCurrentUser();
            
            issueService.addCommentToIssue(issuePackage.getIssue(), user, comment);
        }
        
        return "redirect:/issues/{issueId}";
    }
}
