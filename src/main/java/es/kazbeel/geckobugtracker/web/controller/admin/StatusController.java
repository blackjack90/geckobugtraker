package es.kazbeel.geckobugtracker.web.controller.admin;


import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.kazbeel.geckobugtracker.model.Status;
import es.kazbeel.geckobugtracker.service.IssueService;


@Controller
public class StatusController {
    
    private static final Logger LOG = LoggerFactory.getLogger(StatusController.class);
    
    @Autowired
    IssueService issueService;
    
    
    @RequestMapping(value = "/admin/statuses/ViewStatuses", method = RequestMethod.GET)
    public ModelAndView viewStatuses () {
    
        LOG.debug("Showing List of Statuses");
        
        ModelAndView mav = new ModelAndView("/admin/statuses/ViewStatuses");
        mav.addObject("statuses", issueService.getAllStatuses());
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/statuses/CreateStatus", method = RequestMethod.GET)
    public ModelAndView createStatusGet () {
    
        LOG.debug("Showing Create Status page");
        
        ModelAndView mav = new ModelAndView("/admin/statuses/CreateStatus");
        mav.addObject("status", new Status());
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/statuses/CreateStatus", method = RequestMethod.POST)
    public String createStatusPost (@Valid @ModelAttribute("status") Status status,
                                    BindingResult result) {
    
        LOG.debug("Creating new Status");
        
        if (result.hasErrors() == true) {
            LOG.debug("The status is not valid: {}", result.getFieldError().getDefaultMessage());
            
            return "/admin/statuses/CreateStatus";
        }
        
        try {
            issueService.saveStatus(status);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return "/admin/statuses/CreateStatus";
        }
        
        return "redirect:/admin/statuses/ViewStatuses";
    }
    
    @RequestMapping(value = "/admin/statuses/EditStatus", params = {"id"}, method = RequestMethod.GET)
    public ModelAndView editStatusGet (@RequestParam("id") Integer statusId) {
    
        LOG.debug("Showing Edit Status page (ID = {})", statusId);
        
        ModelAndView mav = new ModelAndView("/admin/statuses/EditStatus");
        mav.addObject("status", issueService.getStatusById(statusId));
        
        return mav;
    }
    
    @RequestMapping(value = "/admin/statuses/EditStatus", params = {"id"}, method = RequestMethod.POST)
    public String editStatusPost (@RequestParam("id") Integer statusId,
                                  @Valid @ModelAttribute("status") Status status,
                                  BindingResult result) {
    
        LOG.debug("Updating Status (ID = {})", statusId);
        
        if (result.hasErrors() == true) {
            LOG.debug("The status is not valid: {}", result.getFieldError().getDefaultMessage());
            
            return "/admin/statuses/EditStatus";
        }
        
        try {
            issueService.saveStatus(status);
        } catch (DataIntegrityViolationException ex) {
            LOG.debug(ex.getMessage());
            
            return "/admin/statuses/EditStatus";
        }
        
        return "redirect:/admin/statuses/ViewStatuses";
    }

    @RequestMapping(value = "/admin/statuses/DeleteStatus", method = RequestMethod.GET)
    public String deleteStatus (@RequestParam("id") Integer statusId, RedirectAttributes redirectAttributes) {
    
        LOG.debug("Deleting Status (ID = {})", statusId);
        
        try {
            Status status = issueService.getStatusById(statusId);
            
            issueService.deleteStatus(status);
            redirectAttributes.addFlashAttribute("msgSuccess", "Status deleted successfully.");
            
            LOG.debug("Status deleted successfully");
        } catch (DataIntegrityViolationException ex) {
            redirectAttributes.addFlashAttribute("msgError",
                                                 "Status cannot be deleted because it's used by some issues.");
            LOG.debug("Status cannot be deleted because it's used by some issues");
        } catch (IllegalArgumentException ex) {
            redirectAttributes.addFlashAttribute("msgError", "Status does not exist.");
            LOG.debug("Status does not exist");
        }
        
        return "redirect:/admin/statuses/ViewStatuses";
    }
}
