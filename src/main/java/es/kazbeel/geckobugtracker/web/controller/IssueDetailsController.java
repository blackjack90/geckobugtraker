package es.kazbeel.geckobugtracker.web.controller;


import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.Collection;
import java.util.List;

import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import es.kazbeel.geckobugtracker.model.Attachment;
import es.kazbeel.geckobugtracker.model.Comment;
import es.kazbeel.geckobugtracker.model.Issue;
import es.kazbeel.geckobugtracker.model.IssueLink;
import es.kazbeel.geckobugtracker.model.IssueLinkType;
import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.model.validator.IssueLinkValidator;
import es.kazbeel.geckobugtracker.service.IssueService;
import es.kazbeel.geckobugtracker.service.UserGroupService;
import es.kazbeel.geckobugtracker.web.entityeditors.IssueEditor;
import es.kazbeel.geckobugtracker.web.entityeditors.IssueLinkTypeEditor;
import es.kazbeel.geckobugtracker.web.entityeditors.IssueLinkTypeListEditor;
import es.kazbeel.geckobugtracker.web.utils.SecurityUtils;


@Controller("issueDetailsController")
@RequestMapping(value = "/issues/{issueId}")
public class IssueDetailsController {
    
    private static final Logger LOG = LoggerFactory.getLogger(IssueDetailsController.class);
    
    @Autowired
    private IssueService issueService;
    
    @Autowired
    private UserGroupService userGroupService;
    
    
    @InitBinder
    public void initBinder (WebDataBinder binder) {
    
        binder.registerCustomEditor(List.class, "allIssueLinkTypes", new IssueLinkTypeListEditor(issueService));
        binder.registerCustomEditor(IssueLinkType.class, "linkType", new IssueLinkTypeEditor(issueService));
        binder.registerCustomEditor(Issue.class, new IssueEditor(issueService));
    }
    
    @ModelAttribute("issuePackage")
    public IssueTagEnvironment populateIssue (@PathVariable("issueId") Integer issueId) {
    
        LOG.debug("Fetching issue with ID={}", issueId);
        
        IssueTagEnvironment issueTagEnv = new IssueTagEnvironment();
        issueTagEnv.setIssue(issueService.getIssueById(issueId));
        
        return issueTagEnv;
    }
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView issueDetailsGet (@PathVariable("issueId") Integer issueId,
                                         @ModelAttribute("issuePackage") IssueTagEnvironment issueTagEnv) {
    
        LOG.debug("Showing Issue Details View (ID={})", issueId);
        
        User loggedUser = SecurityUtils.getCurrentUser();
        Issue issue = issueTagEnv.getIssue();
        
        if (issue == null) {
            LOG.debug("The issue does not exist");
            
            return new ModelAndView("redirect:/Dashboard");
        }
        
        Long numVotes = issueService.getNumVotesForIssue(issue);
        Boolean isVoter = issueService.isIssueVotedByUser(issue, loggedUser);
        Long numWatchers = issueService.getNumWatchersForIssue(issue);
        Boolean isWatcher = issueService.isIssueWatchedByUser(issue, loggedUser);
        Collection<IssueLink> outIssueLinks = issueService.getAllIssueLinksWithSource(issue);
        Collection<IssueLink> inIssueLinks = issueService.getAllIssueLinksWithDestination(issue);
        Collection<IssueLinkType> allIssueLinkTypes = issueService.getAllIssueLinkTypes();
        
        ModelAndView mav = new ModelAndView("issues/Details");
        mav.addObject("loggedUser", loggedUser);
        mav.addObject("issue", issue);
        mav.addObject("numVotes", numVotes);
        mav.addObject("isVoter", isVoter);
        mav.addObject("numWatchers", numWatchers);
        mav.addObject("isWatcher", isWatcher);
        mav.addObject("comment", new Comment());
        mav.addObject("outIssueLinks", outIssueLinks);
        mav.addObject("inIssueLinks", inIssueLinks);
        mav.addObject("allIssueLinkTypes", allIssueLinkTypes);
        mav.addObject("newIssueLink", new IssueLink());
        mav.addObject("attachments", issueService.getAllAttachmentsFromIssue(issue));
        
        return mav;
    }
    
    @RequestMapping(method = RequestMethod.POST)
    public String addComment (@ModelAttribute("issuePackage") IssueTagEnvironment issueTagEnv,
                              @Valid @ModelAttribute("comment") Comment comment,
                              BindingResult result) {
    
        LOG.debug("Adding new comment to issue");
        
        if (result.hasErrors() == true) {
            LOG.debug("Comment is invalid");
            
            return "redirect:/issues/{issueId}";
        }
        
        issueService.addCommentToIssue(issueTagEnv.getIssue(), SecurityUtils.getCurrentUser(), comment);
        
        return "redirect:/issues/{issueId}";
    }
    
    @RequestMapping(value = "/vote", method = RequestMethod.GET)
    public String voteIssue (@PathVariable("issueId") Integer issueId,
                             @RequestParam(value = "action", required = true) String action) {
    
        User user = SecurityUtils.getCurrentUser();
        Issue issue = issueService.getIssueById(issueId);
        
        if (action.equals("vote")) {
            LOG.debug("Voting issue (ID = {})", issueId);
            
            issueService.voteIssue(user, issue, true);
        } else if (action.equals("unvote")) {
            LOG.debug("Unvoting issue (ID = {})", issueId);
            
            issueService.voteIssue(user, issue, false);
        } else {
            LOG.debug("Unknown vote action on issue (ID = {})", issueId);
        }
        
        return "redirect:/issues/{issueId}";
    }
    
    @RequestMapping(value = "/watch", method = RequestMethod.GET)
    public String watchIssue (@PathVariable("issueId") Integer issueId,
                              @RequestParam(value = "action", required = true) String action) {
    
        User user = SecurityUtils.getCurrentUser();
        Issue issue = issueService.getIssueById(issueId);
        
        if (action.equals("watch")) {
            LOG.debug("Watching issue (ID = {})", issueId);
            
            issueService.watchIssue(user, issue, true);
        } else if (action.equals("unwatch")) {
            LOG.debug("Unwatching issue (ID = {})", issueId);
            
            issueService.watchIssue(user, issue, false);
        } else {
            LOG.debug("Unknown watch action on issue (ID = {})", issueId);
        }
        
        return "redirect:/issues/{issueId}";
    }
    
    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public String deleteIssue (@PathVariable("issueId") Integer issueId,
                               @ModelAttribute("issuePackage") IssueTagEnvironment issueTagEnv) {
    
        LOG.debug("Deleting issue (ID = {})", issueId);
        
        issueService.deleteIssue(issueTagEnv.getIssue());
        
        return "redirect:/";
    }
    
    @RequestMapping(value = "/CreateIssueLink", method = RequestMethod.POST)
    public String createIssueLink (@PathVariable("issueId") Integer issueId,
                                   @ModelAttribute("newIssueLink") IssueLink issueLink,
                                   BindingResult result) {
    
        IssueLinkValidator validator = new IssueLinkValidator();
        validator.validate(issueLink, result);
        
        if (result.hasErrors() == true) {
            LOG.debug("Invalid issue link for issue (ID = {})", issueId);
            
            return "redirect:/issues/{issueId}";
        }
        
        LOG.debug("Create issue link (type = {}) from source = {} to destination = {}",
                  issueLink.getLinkType().getId(),
                  issueLink.getSource().getId(),
                  issueLink.getDestination().getId());
        
        issueService.saveIssueLink(issueLink);
        
        return "redirect:/issues/{issueId}";
    }
    
    @RequestMapping(value = "/DeleteIssueLink", method = RequestMethod.POST)
    public String deleteIssueLink (@PathVariable("issueId") Integer issueId,
                                   @ModelAttribute("issueLinkId") Integer issueLinkId,
                                   ModelMap model) {
    
        IssueLink issueLink = issueService.getIssueLinkById(issueLinkId);
        
        model.clear();

        if (issueLink != null) {
            issueService.deleteIssueLink(issueLink);
        }
        
        return "redirect:/issues/{issueId}";
    }
    
    @RequestMapping(value = "/UploadAttachment", method = RequestMethod.POST)
    public String uploadAttachment (@PathVariable("issueId") Integer issueId,
                                    @ModelAttribute("issuePackage") IssueTagEnvironment issueTagEnv,
                                    @RequestParam("file") MultipartFile file,
                                    RedirectAttributes redirectAttributes) {
    
        LOG.debug("Upload file {} attached to issue (ID = {})", file.getOriginalFilename(), issueId);
        
        if (file.isEmpty() == true) {
            LOG.debug("The file is empty");
            
            redirectAttributes.addFlashAttribute("error", "Error storing the file.");
            return "redirect:/issues/{issueId}";
        }
        
        try {
            byte[] bytes = file.getBytes();
            
            // Creating the directory to store file
            String rootPath = System.getProperty("catalina.home");
            LOG.debug("Catalina Home Path: " + rootPath);
            File dir = new File(rootPath + File.separator + "tmpFiles");
            if (!dir.exists()) {
                dir.mkdirs();
            }
            
            // Create the file on server
            File serverFile = new File(dir.getAbsolutePath() + File.separator + file.getOriginalFilename());
            BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
            stream.write(bytes);
            stream.close();
            
            Attachment attachment = new Attachment();
            attachment.setIssue(issueTagEnv.getIssue());
            attachment.setAuthor(SecurityUtils.getCurrentUser());
            attachment.setFileName(file.getOriginalFilename());
            attachment.setFileSize(file.getSize());
            attachment.setFileMimeType(Files.probeContentType(serverFile.toPath()));
            
            issueService.saveAttachment(attachment);
            
            redirectAttributes.addFlashAttribute("success", "File uploaded successfully.");
            return "redirect:/issues/{issueId}";
        } catch (IOException ex) {
            redirectAttributes.addFlashAttribute("error", "Error storing the file.");
            return "redirect:/issues/{issueId}";
        }
    }
    
    @RequestMapping(value = "/DownloadAttachment/{attachmentId}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<byte[]> downloadAttachment (@PathVariable("issueId") Integer issueId,
                                                      @PathVariable("attachmentId") Integer attachmentId,
                                                      RedirectAttributes redirectAttributes) throws IOException {
    
        LOG.debug("Download attachment (ID = {}) from issue (ID = {})", attachmentId, issueId);
        
        Attachment attch = issueService.getAttachmentById(attachmentId);
        
        if (attch == null) {
            throw new FileNotFoundException("The requested file does not exist any longer");
        }
        
        HttpHeaders responseHeader = new HttpHeaders();
        if (attch.getFileMimeType() == null) {
            LOG.debug("Mimetype not found. octet-stream set.");
            responseHeader.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        } else {
            LOG.debug("Mimetype found: {}", attch.getFileMimeType());
            responseHeader.setContentType(MediaType.valueOf(attch.getFileMimeType()));
        }
        
        responseHeader.setContentDispositionFormData("attachment", attch.getFileName());
        
        InputStream downloadFile = new FileInputStream("D:\\Programas\\Tomcat 7.0" + File.separator
                                                       + "tmpFiles"
                                                       + File.separator
                                                       + attch.getFileName());
        
        return new ResponseEntity<byte[]>(IOUtils.toByteArray(downloadFile), responseHeader, HttpStatus.OK);
    }
}
