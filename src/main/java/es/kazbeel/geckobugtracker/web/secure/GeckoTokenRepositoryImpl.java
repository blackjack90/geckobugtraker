package es.kazbeel.geckobugtracker.web.secure;


import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import es.kazbeel.geckobugtracker.model.RemembermeToken;
import es.kazbeel.geckobugtracker.service.AuthService;


public class GeckoTokenRepositoryImpl implements PersistentTokenRepository {
    
    private static final Logger LOG = LoggerFactory.getLogger(GeckoTokenRepositoryImpl.class);
    
    @Autowired
    private AuthService authService;
    
    
    @Override
    public void createNewToken (PersistentRememberMeToken token) {

        LOG.debug("Creating/Updating token");
        
        RemembermeToken rmt = new RemembermeToken();
        
        rmt.setSeries(token.getSeries());
        rmt.setToken(token.getTokenValue());
        rmt.setUsername(token.getUsername());
        rmt.setLastUpdateOn(token.getDate());
        
        authService.saveToken(rmt);
    }
    
    @Override
    public void updateToken (String series, String tokenValue, Date lastUsed) {
    
        LOG.debug("Updating token");
        
        authService.saveToken(series, tokenValue, lastUsed);
    }
    
    @Override
    public PersistentRememberMeToken getTokenForSeries (String seriesId) {
    
        LOG.debug("Retrieving token by series (ID = {})", seriesId);
        
        RemembermeToken rmt = authService.getRemembermeTokenBySeries(seriesId);
        
        if (rmt == null) {
            LOG.debug("Token not found", seriesId);
            
            return null;
        }
        
        return new PersistentRememberMeToken(rmt.getUsername(), rmt.getSeries(), rmt.getToken(), rmt.getLastUpdateOn());
    }
    
    @Override
    public void removeUserTokens (String username) {
    
        LOG.debug("Removing token from user {}", username);
        
        authService.deleteTokensFromUsername(username);
    }
}
