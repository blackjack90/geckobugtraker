package es.kazbeel.geckobugtracker.web.secure;


import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;

import es.kazbeel.geckobugtracker.model.LoginInfo;
import es.kazbeel.geckobugtracker.service.AuthService;


public class LimitLogginAttemptsAuthenticationProvider extends DaoAuthenticationProvider {
    
    private static final Logger LOG = LoggerFactory.getLogger(LimitLogginAttemptsAuthenticationProvider.class);
    
    @Autowired
    private AuthService authService;
    
    
    @Autowired
    @Override
    public void setUserDetailsService (UserDetailsService userDetailsService) {
    
        super.setUserDetailsService(userDetailsService);
    }
    
    @Autowired
    @Override
    public void setPasswordEncoder (Object passwordEncoder) {
    
        super.setPasswordEncoder(passwordEncoder);
    }
    
    @Override
    public Authentication authenticate (Authentication authentication) throws AuthenticationException {
    
        try {
            LOG.debug("Authenticating user ({})", authentication.getName());
            
            Authentication auth = super.authenticate(authentication);
            
            authService.resetFailAttemptsFromLoginInfo(authentication.getName());
            
            LOG.debug("Login successful");
            
            return auth;
        } catch (LockedException ex) {
            LoginInfo loginInfo = authService.getLoginInfoByUsername(authentication.getName());
            Duration diff = new Duration(new DateTime(loginInfo.getLastUpdateOn()), new DateTime());
            
            if (diff.getStandardSeconds() > 30) {
                LOG.info("User is locked for " + diff.getStandardSeconds() + " seconds");
                loginInfo.setLocked(false);
                Authentication auth = super.authenticate(authentication);
                
                LOG.debug("Login successful");
                loginInfo.setFailAttempts(0);
                authService.saveLoginInfo(loginInfo);
                
                return auth;
            } else {
                throw ex;
            }
        }
    }
}
