package es.kazbeel.geckobugtracker.web.entityeditors;


import java.beans.PropertyEditorSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.kazbeel.geckobugtracker.model.Status;
import es.kazbeel.geckobugtracker.service.IssueService;


public class IssueStatusEditor extends PropertyEditorSupport {
    
    private static final Logger LOG = LoggerFactory.getLogger(IssueStatusEditor.class);
    
    private IssueService        issueService;


    public IssueStatusEditor (IssueService issueService) {
        
        this.issueService = issueService;
    }

    @Override
    public void setAsText (String id) {
    
        LOG.debug("Set ID=" + id + " as text");
        Status status = issueService.getStatusById(Integer.parseInt(id));
        this.setValue(status);
    }
    
    @Override
    public String getAsText () {
    
        Status status = (Status) this.getValue();
        
        if (status == null) {
            LOG.error("Previously set IssueStatus has got a null type. Empty string returned");
            
            return "";
        }
        
        LOG.debug("IssueStatus with ID=" + status.getName() + " is returned");
        
        return status.getName();
    }
}
