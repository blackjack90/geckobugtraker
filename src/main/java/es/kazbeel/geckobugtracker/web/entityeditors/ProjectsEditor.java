package es.kazbeel.geckobugtracker.web.entityeditors;


import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;

import es.kazbeel.geckobugtracker.model.Project;
import es.kazbeel.geckobugtracker.service.ProjectService;


public class ProjectsEditor extends CustomCollectionEditor {
    
    private static final Logger LOG = LoggerFactory.getLogger(ProjectsEditor.class);
    
    private ProjectService projectService;
    
    
    @SuppressWarnings("rawtypes")
    public ProjectsEditor (Class<? extends Collection> collectionType, ProjectService projectService) {
    
        super(collectionType, false);
        
        this.projectService = projectService;
    }
    
    @Override
    protected Object convertElement (Object element) {
    
        String id = (String) element;
        Project project = projectService.getProjectById(Long.parseLong(id));
        
        if (project == null) {
            LOG.error("Previously set Project has got a null type. Empty string returned");
            
            return "";
        }
        
        LOG.debug("Environment with ID=" + project.getName() + " is returned");
        
        return project;
    }
}
