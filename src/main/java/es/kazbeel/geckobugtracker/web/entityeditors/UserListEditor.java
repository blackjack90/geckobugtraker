package es.kazbeel.geckobugtracker.web.entityeditors;


import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;

import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.service.UserGroupService;


public class UserListEditor extends CustomCollectionEditor {
    
    private static final Logger LOG = LoggerFactory.getLogger(UserListEditor.class);
    
    private UserGroupService         userGroupService;
    
    
    public UserListEditor (UserGroupService userGroupService) {
    
        super(Set.class, false);
        
        this.userGroupService = userGroupService;
    }
    
    @Override
    protected Object convertElement (Object element) {
    
        User user = userGroupService.getUserById(Integer.parseInt(element.toString()));
        
        if (user == null) {
            LOG.error("Previously set User has got a null type. Empty string returned");
            
            return "";
        }
        
        LOG.debug("User with loginName=" + user.getLoginName() + " is returned");
        
        return user;
    }
}
