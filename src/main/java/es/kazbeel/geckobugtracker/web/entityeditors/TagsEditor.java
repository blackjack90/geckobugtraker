package es.kazbeel.geckobugtracker.web.entityeditors;


import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;

import es.kazbeel.geckobugtracker.model.Tag;
import es.kazbeel.geckobugtracker.service.IssueService;


public class TagsEditor extends CustomCollectionEditor {
    
    private static final Logger LOG = LoggerFactory.getLogger(TagsEditor.class);
    
    private IssueService        issueService;
    
    
    @SuppressWarnings("rawtypes")
    public TagsEditor (Class<? extends Collection> collectionType, IssueService issueService) {
    
        super(collectionType, false);
        
        this.issueService = issueService;
    }
    
    @Override
    protected Object convertElement (Object element) {
    
        String id = (String) element;
        Tag tag = issueService.getTagById(Integer.parseInt(id));
        
        if (tag == null) {
            LOG.error("Previously set Tag has got a null type. Empty string returned");
            
            return "";
        }
        
        LOG.debug("Environment with ID=" + tag.getName() + " is returned");
        
        return tag;
    }
}
