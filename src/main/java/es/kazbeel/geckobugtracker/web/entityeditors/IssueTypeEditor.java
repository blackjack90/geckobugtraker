package es.kazbeel.geckobugtracker.web.entityeditors;


import java.beans.PropertyEditorSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.kazbeel.geckobugtracker.model.IssueType;
import es.kazbeel.geckobugtracker.service.IssueService;


public class IssueTypeEditor extends PropertyEditorSupport {
    
    private static final Logger LOG = LoggerFactory.getLogger(IssueTypeEditor.class);
    
    private IssueService        issueService;
    
    
    public IssueTypeEditor (IssueService issueService) {
    
        this.issueService = issueService;
    }
    
    @Override
    public void setAsText (String id) {
    
        LOG.debug("Set IssueType (ID=" + id + ") as text");
        IssueType type = issueService.getIssueTypeById(Integer.parseInt(id));
        this.setValue(type);
    }
    
    @Override
    public String getAsText () {
    
        IssueType type = (IssueType) this.getValue();
        
        if (type == null) {
            LOG.error("Previously set IssueType has got a null type. Empty string returned.");
            
            return "";
        }
        
        LOG.debug("IssueType with ID=" + type.getName() + " is returned.");
        
        return type.getName();
    }
}
