package es.kazbeel.geckobugtracker.web.entityeditors;


import java.beans.PropertyEditorSupport;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import es.kazbeel.geckobugtracker.model.Issue;
import es.kazbeel.geckobugtracker.service.IssueService;


public class IssueEditor extends PropertyEditorSupport {
    
    private static final Logger LOG = LoggerFactory.getLogger(IssueEditor.class);
    
    private IssueService        issueService;
    
    
    public IssueEditor (IssueService issueService) {
    
        this.issueService = issueService;
    }
    
    @Override
    public void setAsText (String id) {
    
        LOG.debug("Set ID=" + id + " as text");
        Issue issue = issueService.getIssueById(Integer.parseInt(id));
        this.setValue(issue);
    }
    
    @Override
    public String getAsText () {
    
        Issue issue = (Issue) this.getValue();
        
        if (issue == null) {
            LOG.error("Previously set Issue has got a null type. Empty string returned");
            
            return "";
        }
        
        LOG.debug("Issue with ID=" + issue.getId() + " is returned");
        
        return issue.getId().toString();
    }
}
