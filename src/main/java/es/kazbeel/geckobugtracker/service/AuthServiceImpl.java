package es.kazbeel.geckobugtracker.service;


import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.kazbeel.geckobugtracker.model.LoginInfo;
import es.kazbeel.geckobugtracker.model.RemembermeToken;
import es.kazbeel.geckobugtracker.repository.LoginInfoDao;
import es.kazbeel.geckobugtracker.repository.RemembermeTokenDao;


@Service
public class AuthServiceImpl implements AuthService {
    
    private static final int MAX_FAIL_ATTEMPTS = 3;
    
    private RemembermeTokenDao remembermeTokenDao;
    
    private LoginInfoDao loginInfoDao;
    
    
    @Autowired
    public AuthServiceImpl (RemembermeTokenDao remembermeTokenDao, LoginInfoDao loginInfoDao) {
    
        this.remembermeTokenDao = remembermeTokenDao;
        this.loginInfoDao = loginInfoDao;
    }
    
    @Override
    @Transactional
    public Collection<RemembermeToken> getRemembermeTokenByUsername (String username) throws DataAccessException {
    
        return remembermeTokenDao.findByUsername(username);
    }
    
    @Override
    @Transactional
    public RemembermeToken getRemembermeTokenBySeries (String series) throws DataAccessException {
    
        return remembermeTokenDao.findBySeries(series);
    }
    
    @Override
    @Transactional
    public void saveToken (RemembermeToken remembermeToken) throws DataAccessException {
    
        remembermeTokenDao.saveOrUpdate(remembermeToken);
    }
    
    @Override
    @Transactional
    public void saveToken (String series, String token, Date createdOn) throws DataAccessException {
    
        RemembermeToken rmt = remembermeTokenDao.findBySeries(series);
        
        rmt.setToken(token);
        rmt.setLastUpdateOn(createdOn);
        
        remembermeTokenDao.saveOrUpdate(rmt);
    }
    
    @Override
    @Transactional
    public void deleteTokensFromUsername (String username) throws DataAccessException {
    
        Collection<RemembermeToken> rmtCollection = remembermeTokenDao.findByUsername(username);
        
        for (RemembermeToken rmt : rmtCollection) {
            remembermeTokenDao.delete(rmt);
        }
    }
    
    @Override
    @Transactional
    public LoginInfo getLoginInfoByUsername (String username) throws DataAccessException {
    
        return loginInfoDao.findByUsername(username);
    }
    
    @Override
    @Transactional
    public void resetFailAttemptsFromLoginInfo (String username) throws DataAccessException {
    
        loginInfoDao.resetFailAttempts(username);
    }
    
    @Override
    @Transactional
    public void increaseFailAttemptsFromLoginInfo (String username) throws DataAccessException {
    
        LoginInfo loginInfo = loginInfoDao.findByUsername(username);
        
        if (loginInfo == null) {
            return;
        }
        
        loginInfo.setFailAttempts(loginInfo.getFailAttempts() + 1);
        
        if (loginInfo.getFailAttempts() >= MAX_FAIL_ATTEMPTS) {
            loginInfo.setLocked(true);
        }
        
        loginInfoDao.saveOrUpdate(loginInfo);
    }
    
    @Override
    @Transactional
    public void saveLoginInfo (LoginInfo loginInfo) throws DataAccessException {
    
        loginInfoDao.saveOrUpdate(loginInfo);
    }
    
    @Override
    @Transactional
    public void deleteLoginInfoFromUsername (String username) {
        
        LoginInfo loginInfo = loginInfoDao.findByUsername(username);
        
        if (loginInfo != null) {
            loginInfoDao.delete(loginInfo);
        }
    }
}
