package es.kazbeel.geckobugtracker.service;


import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import es.kazbeel.geckobugtracker.model.Group;
import es.kazbeel.geckobugtracker.model.User;
import es.kazbeel.geckobugtracker.repository.GroupDao;
import es.kazbeel.geckobugtracker.repository.UserDao;
import es.kazbeel.geckobugtracker.support.Page;
import es.kazbeel.geckobugtracker.support.PageRequest;


@Service
public class UserGroupServiceImpl implements UserGroupService {
    
    private UserDao userDao;
    
    private GroupDao groupDao;
    
    
    @Autowired
    public UserGroupServiceImpl (UserDao userDao, GroupDao groupDao) {
    
        this.userDao = userDao;
        this.groupDao = groupDao;
    }
    
    @Override
    @Transactional
    public User getUserById (Integer userId) throws DataAccessException {
    
        return userDao.findById(userId);
    }
    
    @Override
    @Transactional
    public Collection<User> getAllEnabledUsers () throws DataAccessException {
    
        return userDao.findAllEnabled();
    }
    
    @Override
    @Transactional
    public void saveUser (User user) throws DataAccessException {
    
        userDao.saveOrUpdate(user);
    }
    
    @Override
    @Transactional
    public Page<User> getAllUsers (PageRequest pageRequest) throws DataAccessException {
    
        List<User> content = userDao.findAll(pageRequest.getOffset(), pageRequest.getSize());
        int totalElements = userDao.countAll().intValue();
        
        Page<User> page = new Page<User>(content);
        page.setPageNumber(pageRequest.getPage());
        page.setMaxPageSize(pageRequest.getSize());
        page.setTotalElements(totalElements);
        
        return page;
    }
    
    @Override
    @Transactional
    public void deleteUser (User user) throws DataAccessException {
        userDao.delete(user);
    }
    
    @Override
    @Transactional
    public Page<Group> getAllGroups (PageRequest pageRequest) throws DataAccessException {
    
        List<Group> content = groupDao.findAll(pageRequest.getOffset(), pageRequest.getSize());
        int totalElements = groupDao.countAll().intValue();
        
        Page<Group> page = new Page<Group>(content);
        page.setPageNumber(pageRequest.getPage());
        page.setMaxPageSize(pageRequest.getSize());
        page.setTotalElements(totalElements);
        
        return page;
    }
    
    @Override
    @Transactional
    public Collection<Group> getAllEnabledGroups () throws DataAccessException {
    
        return groupDao.findAllEnabled();
    }
    
    @Override
    @Transactional
    public Group getGroupById (Integer groupId) throws DataAccessException {
    
        return groupDao.findById(groupId);
    }
    
    @Override
    @Transactional
    public void saveGroup (Group group) throws DataAccessException {
    
        groupDao.saveOrUpdate(group);
    }
    
    @Override
    @Transactional
    public void deleteGroup (Group group) throws DataAccessException {
    
        groupDao.delete(group);
    }
}
