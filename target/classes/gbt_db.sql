CREATE DATABASE  IF NOT EXISTS `gbt` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `gbt`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: gbt
-- ------------------------------------------------------
-- Server version	5.6.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `attachments`
--

DROP TABLE IF EXISTS `attachments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attachments` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ISSUE_ID` int(11) NOT NULL,
  `AUTHOR_ID` int(11) NOT NULL,
  `FILE_NAME` varchar(255) NOT NULL,
  `FILE_SIZE` bigint(20) NOT NULL,
  `FILE_MIMETYPE` varchar(255) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK54475F90E519FD20` (`AUTHOR_ID`),
  KEY `FK54475F90409A06D4` (`ISSUE_ID`),
  CONSTRAINT `FK54475F90409A06D4` FOREIGN KEY (`ISSUE_ID`) REFERENCES `issues` (`ID`),
  CONSTRAINT `FK54475F90E519FD20` FOREIGN KEY (`AUTHOR_ID`) REFERENCES `users` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(64) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `ENABLED` bit(1) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ISSUE_ID` int(11) NOT NULL,
  `AUTHOR_ID` int(11) NOT NULL,
  `BODY` longtext NOT NULL,
  `CREATED_ON` datetime NOT NULL,
  `LAST_UPDATE_ON` datetime NOT NULL,
  `PARENT_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKABDCDF4E519FD20` (`AUTHOR_ID`),
  KEY `FKABDCDF4409A06D4` (`ISSUE_ID`),
  KEY `FKABDCDF463068869` (`PARENT_ID`),
  CONSTRAINT `FKABDCDF463068869` FOREIGN KEY (`PARENT_ID`) REFERENCES `comments` (`ID`),
  CONSTRAINT `FKABDCDF4409A06D4` FOREIGN KEY (`ISSUE_ID`) REFERENCES `issues` (`ID`),
  CONSTRAINT `FKABDCDF4E519FD20` FOREIGN KEY (`AUTHOR_ID`) REFERENCES `users` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `environments`
--

DROP TABLE IF EXISTS `environments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `environments` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) NOT NULL,
  `ENABLED` bit(1) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) NOT NULL,
  `ENABLED` bit(1) NOT NULL,
  `DESCRIPTION` longtext,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `issue_actors`
--

DROP TABLE IF EXISTS `issue_actors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issue_actors` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ACTOR_TYPE` varchar(255) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `ISSUE_ID` int(11) NOT NULL,
  `CREATED_ON` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FKFA3A436484610AE0` (`USER_ID`),
  KEY `FKFA3A4364409A06D4` (`ISSUE_ID`),
  CONSTRAINT `FKFA3A4364409A06D4` FOREIGN KEY (`ISSUE_ID`) REFERENCES `issues` (`ID`),
  CONSTRAINT `FKFA3A436484610AE0` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `issue_link_types`
--

DROP TABLE IF EXISTS `issue_link_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issue_link_types` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) NOT NULL,
  `INWARD` varchar(255) NOT NULL,
  `OUTWARD` varchar(255) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `issue_links`
--

DROP TABLE IF EXISTS `issue_links`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issue_links` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SOURCE_ID` int(11) NOT NULL,
  `DESTINATION_ID` int(11) NOT NULL,
  `LINK_TYPE_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK21763C93B1B8E852` (`SOURCE_ID`),
  KEY `FK21763C938DCF7D02` (`LINK_TYPE_ID`),
  KEY `FK21763C934D5FA37F` (`DESTINATION_ID`),
  CONSTRAINT `FK21763C934D5FA37F` FOREIGN KEY (`DESTINATION_ID`) REFERENCES `issues` (`ID`),
  CONSTRAINT `FK21763C938DCF7D02` FOREIGN KEY (`LINK_TYPE_ID`) REFERENCES `issue_link_types` (`ID`),
  CONSTRAINT `FK21763C93B1B8E852` FOREIGN KEY (`SOURCE_ID`) REFERENCES `issues` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `issue_types`
--

DROP TABLE IF EXISTS `issue_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issue_types` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(128) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `SORT_VALUE` int(11) NOT NULL,
  `ENABLED` bit(1) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `issues`
--

DROP TABLE IF EXISTS `issues`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issues` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `COMPONENT_ID` int(11) NOT NULL,
  `ISSUE_TYPE_ID` int(11) NOT NULL,
  `REPORTER_ID` int(11) NOT NULL,
  `ASSIGNEE_ID` int(11) DEFAULT NULL,
  `PRIORITY_ID` int(11) DEFAULT NULL,
  `STATUS_ID` int(11) DEFAULT NULL,
  `RESOLUTION_ID` int(11) DEFAULT NULL,
  `CATEGORY_ID` int(11) DEFAULT NULL,
  `SUMMARY` varchar(255) NOT NULL,
  `DESCRIPTION` longtext,
  `CREATED_ON` datetime NOT NULL,
  `LAST_UPDATE_ON` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK814A66BA12345678` (`COMPONENT_ID`),
  KEY `FK814A66BA7EC7720A` (`REPORTER_ID`),
  KEY `FK814A66BAFA909D40` (`PRIORITY_ID`),
  KEY `FK814A66BAF4FA7A5C` (`ASSIGNEE_ID`),
  KEY `FK814A66BA8CB66147` (`ISSUE_TYPE_ID`),
  KEY `FK814A66BA8EEEA700` (`CATEGORY_ID`),
  KEY `FK814A66BA93A876C0` (`RESOLUTION_ID`),
  KEY `FK814A66BA5D501C00` (`STATUS_ID`),
  CONSTRAINT `FK814A66BA12345678` FOREIGN KEY (`COMPONENT_ID`) REFERENCES `components` (`ID`),
  CONSTRAINT `FK814A66BA5D501C00` FOREIGN KEY (`STATUS_ID`) REFERENCES `statuses` (`ID`),
  CONSTRAINT `FK814A66BA7EC7720A` FOREIGN KEY (`REPORTER_ID`) REFERENCES `users` (`ID`),
  CONSTRAINT `FK814A66BA8CB66147` FOREIGN KEY (`ISSUE_TYPE_ID`) REFERENCES `issue_types` (`ID`),
  CONSTRAINT `FK814A66BA8EEEA700` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `categories` (`ID`),
  CONSTRAINT `FK814A66BA93A876C0` FOREIGN KEY (`RESOLUTION_ID`) REFERENCES `resolutions` (`ID`),
  CONSTRAINT `FK814A66BAF4FA7A5C` FOREIGN KEY (`ASSIGNEE_ID`) REFERENCES `users` (`ID`),
  CONSTRAINT `FK814A66BAFA909D40` FOREIGN KEY (`PRIORITY_ID`) REFERENCES `priorities` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `issues_environments_map`
--

DROP TABLE IF EXISTS `issues_environments_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issues_environments_map` (
  `ISSUE_ID` int(11) NOT NULL,
  `ENVIRONMENT_ID` int(11) NOT NULL,
  PRIMARY KEY (`ENVIRONMENT_ID`,`ISSUE_ID`),
  KEY `FK107BF82280339114` (`ENVIRONMENT_ID`),
  KEY `FK107BF822409A06D4` (`ISSUE_ID`),
  CONSTRAINT `FK107BF822409A06D4` FOREIGN KEY (`ISSUE_ID`) REFERENCES `issues` (`ID`),
  CONSTRAINT `FK107BF82280339114` FOREIGN KEY (`ENVIRONMENT_ID`) REFERENCES `environments` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `issues_tags_map`
--

DROP TABLE IF EXISTS `issues_tags_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `issues_tags_map` (
  `ISSUE_ID` int(11) NOT NULL,
  `TAG_ID` int(11) NOT NULL,
  PRIMARY KEY (`TAG_ID`,`ISSUE_ID`),
  KEY `FK1A078BFB409A06D4` (`ISSUE_ID`),
  KEY `FK1A078BFBAEFEA9B4` (`TAG_ID`),
  CONSTRAINT `FK1A078BFBAEFEA9B4` FOREIGN KEY (`TAG_ID`) REFERENCES `tags` (`ID`),
  CONSTRAINT `FK1A078BFB409A06D4` FOREIGN KEY (`ISSUE_ID`) REFERENCES `issues` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `login_info`
--

DROP TABLE IF EXISTS `login_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login_info` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(255) NOT NULL,
  `FAIL_ATTEMPTS` int(11) DEFAULT NULL,
  `LOCKED` bit(1) DEFAULT NULL,
  `LAST_UPDATE_ON` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `USERNAME` (`USERNAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `priorities`
--

DROP TABLE IF EXISTS `priorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `priorities` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(64) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `SORT_VALUE` int(11) NOT NULL,
  `ENABLED` bit(1) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `rememberme_tokens`
--

DROP TABLE IF EXISTS `rememberme_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rememberme_tokens` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(255) NOT NULL,
  `SERIES` varchar(255) NOT NULL,
  `TOKEN` varchar(255) NOT NULL,
  `LAST_UPDATE_ON` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `USERNAME` (`USERNAME`,`SERIES`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `resolutions`
--

DROP TABLE IF EXISTS `resolutions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `resolutions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `SORT_VALUE` int(11) NOT NULL,
  `ENABLED` bit(1) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sections`
--

DROP TABLE IF EXISTS `sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sections` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(4096) DEFAULT NULL,
  `SORT_VALUE` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `projects`
--

DROP TABLE IF EXISTS `projects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(4096) DEFAULT NULL,
  `SECTION_ID` int(11) NOT NULL,
  `DEV_LEADER_ID` int(11) NOT NULL,
  `QA_LEADER_ID` int(11) NOT NULL,
  KEY `FK1A078BFB409A06DAA` (`SECTION_ID`),
  KEY `FK1A078BFB409A06DAB` (`DEV_LEADER_ID`),
  KEY `FK1A078BFB409A06DAC` (`QA_LEADER_ID`),
  CONSTRAINT `FK1A078BFB409A06DAA` FOREIGN KEY (`SECTION_ID`) REFERENCES `sections` (`ID`),
  CONSTRAINT `FK1A078BFB409A06DAB` FOREIGN KEY (`DEV_LEADER_ID`) REFERENCES `users` (`ID`),
  CONSTRAINT `FK1A078BFB409A06DAC` FOREIGN KEY (`QA_LEADER_ID`) REFERENCES `users` (`ID`),
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project_versions`
--

DROP TABLE IF EXISTS `project_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(4096) DEFAULT NULL,
  `SORT_VALUE` int(11) NOT NULL,
  `RELEASED` bit(1) NOT NULL,
  `ARCHIVED` bit(1) NOT NULL,
  `RELEASED_DATE` datetime DEFAULT NULL,
  `CREATED_ON` datetime NOT NULL,
  `PROJECT_ID` int(11) NOT NULL,
  KEY `FK1A078BFB409A06DAA1` (`PROJECT_ID`),
  CONSTRAINT `FK1A078BFB409A06DAA1` FOREIGN KEY (`PROJECT_ID`) REFERENCES `projects` (`ID`),
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `components`
--

DROP TABLE IF EXISTS `components`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `components` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) NOT NULL,
  `DESCRIPTION` varchar(4096) DEFAULT NULL,
  `DEV_LEADER_ID` int(11) NOT NULL,
  `QA_LEADER_ID` int(11) NOT NULL,
  `PROJECT_ID` int(11) NOT NULL,
  `CREATED_ON` datetime NOT NULL,
  `LAST_UPDATE_ON` datetime NOT NULL,
  KEY `FK1A078BFB409A06DAEA` (`DEV_LEADER_ID`),
  KEY `FK1A078BFB409A06DAFA` (`QA_LEADER_ID`),
  KEY `FK1A078BFB409A06DAGA` (`PROJECT_ID`),
  CONSTRAINT `FK1A078BFB409A06DAEA` FOREIGN KEY (`DEV_LEADER_ID`) REFERENCES `users` (`ID`),
  CONSTRAINT `FK1A078BFB409A06DAFA` FOREIGN KEY (`QA_LEADER_ID`) REFERENCES `users` (`ID`),
  CONSTRAINT `FK1A078BFB409A06DAGA` FOREIGN KEY (`PROJECT_ID`) REFERENCES `projects` (`ID`),
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `statuses`
--

DROP TABLE IF EXISTS `statuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `statuses` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(64) NOT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `SORT_VALUE` int(11) NOT NULL,
  `ENABLED` bit(1) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) NOT NULL,
  `ENABLED` bit(1) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `NAME` (`NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `LOGIN_NAME` varchar(128) NOT NULL,
  `SHORTCUT` varchar(5) NOT NULL,
  `PASSWORD` varchar(64) NOT NULL,
  `ENABLED` bit(1) NOT NULL,
  `FIRST_NAME` varchar(64) NOT NULL,
  `LAST_NAME` varchar(128) NOT NULL,
  `EMAIL` varchar(255) NOT NULL,
  `CREATED_ON` datetime NOT NULL,
  `LAST_UPDATE_ON` datetime NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `LOGIN_NAME` (`LOGIN_NAME`),
  UNIQUE KEY `SHORTCUT` (`SHORTCUT`),
  UNIQUE KEY `EMAIL` (`EMAIL`),
  UNIQUE KEY `LOGIN_NAME_2` (`LOGIN_NAME`),
  UNIQUE KEY `EMAIL_2` (`EMAIL`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `users_groups_map`
--

DROP TABLE IF EXISTS `users_groups_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_groups_map` (
  `USER_ID` int(11) NOT NULL,
  `GROUP_ID` int(11) NOT NULL,
  PRIMARY KEY (`GROUP_ID`,`USER_ID`),
  KEY `FK78429B0884610AE0` (`USER_ID`),
  KEY `FK78429B0835065D14` (`GROUP_ID`),
  CONSTRAINT `FK78429B0835065D14` FOREIGN KEY (`GROUP_ID`) REFERENCES `groups` (`ID`),
  CONSTRAINT `FK78429B0884610AE0` FOREIGN KEY (`USER_ID`) REFERENCES `users` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

--
-- Table structure for table `projects_components_map`
--

DROP TABLE IF EXISTS `projects_components_map`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `projects_components_map` (
  `PROJECT_ID` int(11) NOT NULL,
  `COMPONENT_ID` int(11) NOT NULL,
  PRIMARY KEY (`COMPONENT_ID`,`PROJECT_ID`),
  KEY `FK78429B0884610AE0F` (`PROJECT_ID`),
  KEY `FK78429B0835065D14F` (`COMPONENT_ID`),
  CONSTRAINT `FK78429B0884610AE0F` FOREIGN KEY (`PROJECT_ID`) REFERENCES `projects` (`ID`),
  CONSTRAINT `FK78429B0835065D14F` FOREIGN KEY (`COMPONENT_ID`) REFERENCES `components` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-01-06 12:46:59
